---
title: ! 'Scraping essentials: BeautifulSoup - part 5'
tags: [scraping, beautifulsoup, amazon, mechanicalsoup, python, tutorial, amazon-scraper]
updated: 2024-01-15 21:06:00
description: ! "An improved version of the Amazon product scraper in Python"
lang: 'en'
images:
  - id: 'intro'
    filename: 'intro.png'
    alt: ! 'An Amazon search result page with annotations to explain where the different variables come from'
    caption: ! 'The most important data for each product can be gathered from the search results'
    width: '300px'
---
{% include image.html id="intro" %}
<!--excerpt_start-->Instead of using a bottom-up approach as we did in the
[previous posts]({%- include post/post_url_iso639.liquid post_url="2024-01-14-scraping-essentials-beautifulsoup-part-4" -%}{{ post_url_iso639 }} "{{ post_title_iso639 }}")<!--excerpt_end-->
we'll get all the data we need from the search query page.

<iframe width="280px" height="157px" src="https://www.youtube.com/embed/UdQDlNpJGwY?si=O4HrXEMs6pNGeYEe" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Improvements

### Time

Scraping the data from the search page involves only one HTTP request. This
reflects in the total running time of the script:

```
time python -m amzn_light "ssd"

<JSON result>

real    0m1.921s
user    0m0.524s
sys     0m0.052s
```

2 seconds, not bad compared to nearly 5 minutes for the previous version to
gather the same data. Moreover, sometimes Amazon did block the requests because
some of their thresholds were being passed. This is 150 times better!

### Code reduction

This time the dataclasses are the same but we only need one scraping function
instead of three. The value normalization functions are still needed so they
have not changed.

### No more search bar

As said
[earlier]({%- include post/post_url_iso639.liquid post_url="2024-01-14-scraping-essentials-beautifulsoup-part-4" -%}{{ post_url_iso639 }}#simpler-alternative "{{ post_title_iso639 }}"),
there is no need to use the search bar to get the results: you can just use the
URL with the query.

### Passing the query to the script

Finally, you can now pass the search term directly as an argument to the
script, for example like this:

```shell
python -m amzn_light "ssd 1tb"
```

## Code

```python
import mechanicalsoup
from dataclasses import dataclass
import decimal
import re
from urllib.parse import urlparse, urljoin
from mashumaro.mixins.json import DataClassJSONMixin
import sys


@dataclass
class Product:
    url: str
    asin_raw: str
    asin: str
    title_raw: str
    title: str
    price_raw: str
    price: decimal.Decimal
    currency_symbol_raw: str
    currency_symbol: str
    available_raw: str
    available: bool
    rating_raw: str
    rating: float
    reviews_raw: str
    reviews: int


@dataclass
class Products:
    products: list[Product]


@dataclass
class ProductSearch(DataClassJSONMixin):
    products: Products
    url: str


def normalize_price(price_raw: str) -> decimal.Decimal:
    r"""Accepts a stripped string containing numbers, ',' or '.'."""

    price_filtered: list = []
    for c in price_raw:
        if re.match(r'([0-9]|,)', c):
            if c == ',':
                c = '.'
            price_filtered.append(c)

    return decimal.Decimal(''.join(price_filtered))


def normalize_float(float_raw: str) -> float:
    r"""Accepts a stripped string containing numbers, ',' or '.'."""

    float_filtered: list = []
    for c in float_raw:
        if re.match(r'([0-9]|,)', c):
            if c == ',':
                c = '.'
            float_filtered.append(c)

    return float(''.join(float_filtered))


def normalize_int(int_raw: str) -> int:
    int_filtered: list = []

    for c in int_raw:
        if re.match(r'[0-9]', c):
            int_filtered.append(c)

    return int(''.join(int_filtered))


def get_search_results(url: str, browser_user_agent: str) -> ProductSearch:
    browser = mechanicalsoup.StatefulBrowser(
            user_agent=browser_user_agent,
    )
    browser.open(url)


    all_products: list[Product] = []
    product_divs = browser.page.find_all('div', class_='a-section a-spacing-base')
    for div in product_divs:
        link = div.find('a', class_='a-link-normal s-no-outline')
        if link:
            product_url: str = link.get('href')

            # Products don't start with https:// and /sspa/
            if not (product_url.startswith('https://') or product_url.startswith('/sspa/')):


                # Products without the price written in "big" characters seems unavailable
                # or they don't have any currently available offer: prices are very high
                # compared to similar products. We'll assume all these products are
                # unavailable.
                price_offscreen_element = div.find('span',  class_='a-price')
                if price_offscreen_element is not None:
                    # Get the rating section.
                    rating_reviews_span = div.find('div', class_='a-row a-size-small')

                    # Skip other buying options.
                    if rating_reviews_span is not None:

                        product_url: str = urljoin(url, product_url)
                        u = urlparse(product_url)
                        # Get the ASIN from the URL.
                        product_asin_raw: str = u.path.split('/')[3]
                        product_asin: str = product_asin_raw
                        product_title_raw: str = div.find('span', class_='a-size-base-plus a-color-base a-text-normal').text.strip()
                        product_title: str = product_title_raw
                        product_price_raw: str = price_offscreen_element.find('span', class_='a-offscreen').text.strip()
                        product_price = normalize_price(product_price_raw)
                        product_currency_symbol_raw: str = price_offscreen_element.find('span',  class_='a-price-symbol').text.strip()
                        product_currency_symbol: str = product_currency_symbol_raw
                        product_available_raw: str = 'yes'
                        product_available: bool = True
                        product_reviews_raw: str = rating_reviews_span.find('span', class_='a-size-base s-underline-text').text.strip()
                        product_reviews: str = normalize_int(product_reviews_raw)
                        # The rating value is inside the `aria-label` attribute so we need to use `get()` instead of `text`.
                        product_rating_raw: str = rating_reviews_span.find('span', attrs={'aria-label': re.compile('.*')}).get('aria-label').strip()
                        # The rating is in the format: "${int_part}.${dec_part} out of 5"
                        product_rating_raw_split: str = product_rating_raw.split(' ')[0]
                        product_rating: float = normalize_float(product_rating_raw_split)

                        all_products.append(Product(
                            url=product_url,
                            asin_raw=product_asin_raw,
                            asin=product_asin,
                            title_raw=product_title_raw,
                            title=product_title,
                            price_raw=product_price_raw,
                            price=product_price,
                            currency_symbol_raw=product_currency_symbol_raw,
                            currency_symbol=product_currency_symbol,
                            available_raw=product_available_raw,
                            available=product_available,
                            reviews_raw=product_reviews_raw,
                            reviews=product_reviews,
                            rating_raw=product_rating_raw,
                            rating=product_rating
                        ))

    return ProductSearch(products=Products(products=all_products), url=url)


if __name__ == '__main__':
    URL='https://www.amazon.it'
    USER_AGENT = 'Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Ubuntu/10.10 Chromium/10.0.648.0 Chrome/10.0.648.0 Safari/534.16'
    SEARCH_QUERY = sys.argv[1]

    URL = URL + '/s?k=' + SEARCH_QUERY
    s = get_search_results(URL, USER_AGENT)
    print(s.to_json())
```

## Conclusion

This posts closes this Amazon scraping tutorial. Have fun 🚀
