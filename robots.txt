---
layout: null
---

user-agent: EmailCollector
disallow: /

User-agent: GPTBot
Disallow: /

{% comment %}https://netfuture.ch/2023/07/blocking-ai-crawlers-robots-txt-chatgpt/{% endcomment -%}

User-agent: ChatGPT-User
Disallow: /

{% comment %}Used for many other (non-commercial) purposes as well{% endcomment -%}
User-agent: CCBot
Disallow: /

{% comment %}Marker for disabling Bard and Vertex AI. https://developers.google.com/search/docs/crawling-indexing/overview-google-crawlers{% endcomment -%}
User-agent: Google-Extended
Disallow: /

User-agent: GoogleOther
Disallow: /

{% comment %}Speech synthesis only?{% endcomment -%}
User-agent: FacebookBot
Disallow: /

{% comment %}Multi-purpose, commercial uses; including LLMs{% endcomment -%}
User-agent: Omgilibot
Disallow: /

{% comment %}
User-agent: *
Disallow: /*.SHA512SUM.txt$
Disallow: /*.SHA256SUM.txt$
Disallow: /*.MD5SUM.txt$
Disallow: /*.tar.gz$
Disallow: /*.sig$
{% endcomment %}

sitemap: {{ site.rooturl | append: '/sitemap.xml' }}
