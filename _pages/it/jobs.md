---
layout: page
title: Lavoro
permalink: /it/jobs/
description: ! 'I miei lavori di informatica e servizi'
lang: 'it'
---

## Recensioni su Fiverr

{% include fiverr_gigs.html %}

## Scripts e documentazione

Posso
[sviluppare script a se stanti...](https://it.fiverr.com/s/4eZ65b)
così come [documentazione dettagliata del software](https://it.fiverr.com/s/b47YA1).

## Lezioni

Do lezioni in informatica (programmazione, database, reti, Linux, ecc...)
con videochiamate su Nextcloud, Skype, Google Meet, Zoom, ecc...

## Altro

- Installazione Linux
- Installazione ROM Android
- Soluzione per problemi generici sul PC
- Recupero dati (se la memoria non presenta danni fisici, ad esempio se
  l'utente ha cancellato i file involontariamente )

## Contatti

Vedere il paragrafo [contatti]({{ '/about/' | relative_url }}#contatti).
