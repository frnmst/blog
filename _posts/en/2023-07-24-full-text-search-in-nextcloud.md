---
title: Full text search in Nextcloud
tags: [nextcloud, search, docker, docker-compose, elasticsearch, video, youtube]
related_links: ['https://youtu.be/yPZkrzgue5c']
updated: 2023-07-24 20:38:00
description: Setup full text search in Nextcloud using Elasticsearch
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/yPZkrzgue5c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

This video shows you how to set up full text search in a Nextcloud instance.

When I did the setup myself, I found it being not as easy as I thought, so I decided to share the steps and caveats.

In this video, Nextcloud and Elasticsearch are set up using docker-compose.
