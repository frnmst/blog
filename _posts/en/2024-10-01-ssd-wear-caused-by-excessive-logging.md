---
title: ! 'SSD wear caused by excessive logging?'
tags: [ssd, ssd-wear, logs, memory, server, tutorial, youtube, video]
related_links: ['https://www.youtube.com/watch?v=tHpgYyboSHM']
updated: 2024-10-01 20:27:39
description: ! "In this video we'll see what happens to SSD drives when you don't configure your loggings correctly"
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/tHpgYyboSHM?si=qY_B2WXFwtdExLsN" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<!--TOC-->

- [Summary](#summary)

<!--TOC-->

## Summary

In this video we'll see what happened with the SSDs in my server, specifically focusing on the `/var/log` and Gitea partitions that I have mirrored on two 500GB SSDs. <!--excerpt_start-->After replacing my previous SSDs due to a firmware failure, I installed two<!--excerpt_end--> Crucial MX500 drives. However, I recently received a notification regarding a concerning SMART attribute, `Percent_Lifetime_Remain`. This indicated that both drives were showing signs of wear much sooner than expected, raising my skepticism about their performance.

Upon investigating, I discovered that both drives reported the same issue within a day, suggesting that it wasn't just a single defective unit but possibly a problem with the firmware or my server setup. I decided to check the `Total_LBAs_Written` attribute to calculate the total terabytes written to the drives. The results were alarming, indicating that the drives had endured approximately 2.5 times the maximum guaranteed writes, which I attributed to extensive logging from the Apache web server and the numerous health checks performed by Gitea on the mirrored repositories.

I realized that the logging practices were likely contributing significantly to the wear on the SSDs. Even though the web server logs all requests, including successful ones, the sheer volume of traffic from bots on various subdomains was not sufficient to explain the excessive write amounts. To mitigate this, I implemented a filtering method to exclude certain HTTP response codes from being logged, which I found to be an effective solution. I also disabled Gitea's repos health checks for the time being. Additionally, I addressed verbose logging settings across other services to further reduce unnecessary writes.

Despite these challenges, I am currently not experiencing any errors with the SSDs, but I remain vigilant and plan to monitor their performance closely. I am considering replacing the drives soon. I am not an expert in this area and would appreciate any insights or suggestions from others who might have faced similar issues.
