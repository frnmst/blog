## Instructions

<!--TOC-->

- [Instructions](#instructions)
  - [Setup](#setup)
    - [Linux](#linux)
    - [Mac](#mac)
    - [Windows](#windows)
  - [Running](#running)
    - [Linux](#linux-1)
    - [Mac](#mac-1)
    - [Windows](#windows-1)
- [Other](#other)

<!--TOC-->

**All the commands you see are to be executed one by one. Don't copy the whole command block,
but execute only one command at a time!**

### Setup

See also [https://docs.python.org/3/library/venv.html#how-venvs-work](https://docs.python.org/3/library/venv.html#how-venvs-work)

#### Linux

1. open a terminal and go into the project directory
2. if you are on a new Debian-based system and never created a Python virtual environment,
   most probably you need to install this package

   ```shell
   sudo apt install python3-venv
   ```

3. create the virtual environment

   ```shell
   python3 -m venv .venv
   . .venv/bin/activate
   pip install -r requirements.txt
   deactivate
   ```

#### Mac

1. open a terminal and go into the project directory
2. if not already installed, install Python

   ```shell
   brew install python
   ```

3. now create the virtual environment

   ```shell
   python3 -m venv .venv
   source .venv/bin/activate
   pip install -r requirements.txt
   deactivate
   ```

#### Windows

1. install Python 3.11 from the **Windows store** which works out of the box. *Don't use the releases from the Python website*.
2. open the Powershell **as administrator**, then set the execution policy to `Unrestricted`.
   This needs to be done only once, not every time you create a virtual
   environment. See also
   [https://go.microsoft.com/fwlink/?LinkID=135170](https://go.microsoft.com/fwlink/?LinkID=135170)

   ```shell
   Set-ExecutionPolicy -ExecutionPolicy Unrestricted
   ```

3. close the **administrator** Powershell
4. open a normal Powershell
5. go into the project directory and proceed to create the virtual environment

   ```shell
   python3 -m venv .venv
   . .\.venv\Scripts\activate
   pip install -r requirements.txt
   deactivate
   ```

Note: for security reasons I suggest putting the execution policy back to its
original state when the script will not be needed anymore

### Running

#### Linux

1. open a terminal and go into the project directory
2. run the script

   ```shell
   . .venv/bin/activate
   python -m automate
   deactivate
   ```

#### Mac

1. open a terminal and go into the project directory
2. run the script

   ```shell
   source .venv/bin/activate
   python -m automate
   deactivate
   ```

#### Windows

1. open the Powershell and go into the project directory
2. run the script

   ```shell
   . .\.venv\Scripts\activate
   python -m automate
   deactivate
   ```

## Other

To regenerate this HTML doc run:

```shell
pandoc -f markdown -t html -o readme.html README.md
```
