---
title: ! 'Self hosted CPU A.I. part 1 - usage'
tags: [ai, chatbot, openwebui, ollama, youtube, video]
related_links: ['https://www.youtube.com/watch?v=wwZSNBuKOgc']
updated: 2024-08-20 17:03:30
description: ! "In this video we'll see how to use self-hosted CPU A.I. chatbots."
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/wwZSNBuKOgc?si=wTBlN79Nssf7bbMl" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

In this video, I introduced OpenWebUI, a web interface designed for self-hosted generative AI models. I demonstrated how the interface operates alongside a server running Ollama, which handles the AI models. By separating the web interface and the backend server, I aimed to reduce the load on the main server. I showcased various models available for use, including the Tinyllama model, and explained how to create new chats and manage model files. The interface allows users to download models from the Ollama website and customize prompts for generating responses.

I also discussed the performance of different models, noting that some are lightweight and fast, while others can be resource-intensive. I encountered issues with certain models freezing or timing out, which required refreshing the page or creating new chats. I shared my experience with a simple prompt generator I created, which successfully generated prompts and responses. Throughout the video, I monitored the server load and RAM usage, emphasizing the importance of selecting models that are compatible with CPU resources.

As I explored the model management features, I highlighted the need for sufficient storage space, as some models can consume significant disk space. I demonstrated how to delete non-functional models to free up space and ensure smoother operation. Additionally, I mentioned the possibility of generating images using other tools like Stable Diffusion and ComfyUI, although I had not tested those yet.

In conclusion, I encouraged viewers to explore the Ollama website for more models that work well on CPUs, sharing my own setup with a Core i5-9400 and 32 GB of RAM. I hinted at an upcoming video where I would provide installation instructions for OpenWebUI and Ollama using Docker, emphasizing the benefits of running them on separate servers to optimize performance.
