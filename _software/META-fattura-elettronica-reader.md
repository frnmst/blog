---
meta: true
software_name: fattura-elettronica-reader
archived: false
repositories:
  - label: 'canonical'
    url: 'https://software.franco.net.eu.org/frnmst/fattura-elettronica-reader'
  - label: 'codeberg'
    url: 'https://codeberg.org/frnmst/fattura-elettronica-reader'
  - label: 'framagit'
    url: 'https://framagit.org/frnmst/fattura-elettronica-reader'
documentation:
  - label: 'docs.franco.net.eu.org/fattura-elettronica-reader'
    url: 'https://docs.franco.net.eu.org/fattura-elettronica-reader'
  - label: 'changelog'
    url:
---
