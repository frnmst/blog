// SPDX-FileCopyrightText: 2023 Franco Masotti
//
// SPDX-License-Identifier: MIT
#include <stdio.h>
#include <string.h>

int main(void)
{
    char array[100] = "ab c def g hi";

    int i = 0, j = 0, done = 0, go_back = 0;

    char tmp;

    while (!done)
    {
        while (array[i] != '\0')
            i++;
        if (array[i] == '\0')
        {
            // Exclude the '\0' character from the count.
            go_back = 1;
            i--;
        }

        while (i > j)
        {
            // Swap.
            tmp = array[j];
            array[j] = array[i];
            array[i] = tmp;

            i--;
            j++;
        }

        if (go_back && i <= j)
            done = 1;
    }

    printf("%d %d\n", i, j);

    printf("reversed = %s\n", array);

    return 0;
}
