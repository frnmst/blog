---
meta: true
software_name: qvm
archived: true
repositories:
  - label: 'canonical'
    url: 'https://software.franco.net.eu.org/frnmst-archives/qvm'
  - label: 'codeberg'
    url: 'https://codeberg.org/frnmst-archives/qvm'
  - label: 'framagit'
    url: 'https://framagit.org/frnmst-archives/qvm'
documentation:
  - label: 'software.franco.net.eu.org/frnmst-archives/qvm#qvm'
    url: 'https://software.franco.net.eu.org/frnmst-archives/qvm#qvm'
---
