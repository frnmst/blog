---
title: ! 'Digital Euro updates and considerations'
tags: [euro, digital-euro, cbdc, cental-bank-digital-currency, news video, youtube]
related_links: ['https://www.youtube.com/watch?v=tNkKAmsAPW0']
updated: 2024-02-01 18:10:00
description: ! 'Recent updates on the digital euro CBDC and personal opinions about it'
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/tNkKAmsAPW0?si=rroESvGz4p2ugbss" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Let's have a look at some of the official documents from the European Union
concerning the Digital Euro.

There are now more information on the different services to support this CBDC such as:

- offline usage
- alias lookup service
- fraud detection mechanisms

and more. You'll discover them in the video.
