---
title: Markdown table of contents
tags: [md-toc, table-of-contents, markdown, video, youtube]
updated: 2023-08-07 17:28:00
description: md-toc is a software written in Python to automatically generate table of contents (TOC) in markdown using its CLI, API or pre-commit hook
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/guyVdPNmC0A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In this video I present you a software called md-toc, useful to create table of contents (TOC) in markdown without effort.

md-toc is written in Python and supports several markdown parsers. It has a CLI, an API and a pre-commit hook.
