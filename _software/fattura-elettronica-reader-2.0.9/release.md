---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fattura-elettronica-reader
software_name_python_module: fattura_elettronica_reader
software_version: 2.0.9
software_version_raw: 000002.000000.000009
release_timestamp: 2021-10-19 10:34:02
is_on_pypi: true
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Updated file checksum variables.
- Relaxed Python dependencies.

### Added

- Added new makefile instructions.
