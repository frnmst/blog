---
title: ! 'JSON-LD con Jekyll'
tags: [json, json-ld, jekyll, tutorial]
updated: 2023-12-27 16:53:00
description: ! "Generate JSON-LD for Jekyll blog posts to improve SEO"
lang: 'it'
permalink: '/it/post/json-ld-in-jekyll.html'
---
JSON-LD è un modo per esporre i dati strutturati del tuo sito web in un formato
leggibile dalle macchine.
È particolarmente utile per i motori di ricerca così possono generare quelle
belle anteprime con le immagini, sezioni di domande e risposte, ecc...
Funziona con gli stessi campi dei
[Microdata](https://en.wikipedia.org/wiki/Microdata_(HTML))
e dell'[Open Graph protocol](https://ogp.me/).
Ho scritto qualcosa a riguardo in
[questo precedente post]({%- include post/post_url_iso639.liquid post_url="2023-12-02-moving-data-to-jekyll-yaml-front-matter" -%}{{ post_url_iso639 }}).

{% include link_preview.html url="https://json-ld.org/" %}

Ci sono molti tipi diversi di
[dati strutturati](https://developers.google.com/search/docs/appearance/structured-data/intro-structured-data)
usati dai motori di ricerca.
[La documentazione di Google](https://developers.google.com/search/docs/appearance/structured-data/search-gallery)
spiega queste opzioni in dettaglio.

Per creare una struttura dati JSON-LD per i post di questo blog che utilizza
Jekyll è necessario includere un file HTML. Nel mio caso viene messo in
`_includes/json_ld.html`. Ho modificato il codice da
[questo post](https://mincong.io/2018/08/22/create-json-ld-structured-data-in-jekyll/)
e l'ho migliorato affinché sia più affidabile e compatibile con il mio blog.
Andando nei dettagli ho aggiunto alcuni campi come `licenza` e usato il filtro
`jsonify` per tutte le variabili. Ho anche usato lo stesso trucco per
l'immagine che ho fatto per l'[Open Graph protocol]({%- include post/post_url_iso639.liquid post_url="2023-12-02-moving-data-to-jekyll-yaml-front-matter" -%}{{ post_url_iso639 }})

```liquid
{% raw %}{% comment %}CC Attribution 4.0 International license https://mincong.io/2018/08/22/create-json-ld-structured-data-in-jekyll/{% endcomment %}
{%- if page.is_post -%}
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "BlogPosting",
  "url": {{ page.url | prepend: site.rooturl | jsonify }},
  "name": {{ page.title | jsonify }},
  "headline": {{ page.title | jsonify }},
  "keywords": {{ page.tags | join: ',' | jsonify }},
  "description": {{ page.excerpt | newline_to_br | strip_newlines | replace: '<br />', ' ' | strip | jsonify }},
  "articleBody": {{ page.content | strip_html | jsonify }},
  "datePublished": {{ page.date | jsonify }},
  "dateModified": {{ page.updated | default: page.date | jsonify }},
  "license": {{ site.website.license.url | jsonify }},
  "author": {
    "@type": "Person",
    "name": {{ site.author.name | jsonify }},
    "email": {{ site.author.email | jsonify }}
  },
  "publisher": {
    "@type": "Organization",
    "name": {{ site.title | jsonify }},
    "url": {{ site.rooturl | jsonify }},
    "logo": {
      "@type": "ImageObject",
      "width": 32,
      "height": 32,
      "url": {{ "/favicon.ico" | prepend: site.rooturl | jsonify }}
    }
  },
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": {{ page.url | prepend: site.rooturl | jsonify }}
  },
  {%- if page.images -%}
    {%- if page.seo_default_image_id -%}
        {%- assign img_data = page.images | where:"id",page.seo_default_image_id | first -%}
    {%- else -%}
        {%- comment -%}Get the first picture of a post if not specified otherwise.{%- endcomment -%}
        {%- assign img_data = page.images[0] -%}
    {%- endif -%}
  {%- include image_url.html img_data=img_data -%}
  {%- endif %}
  {%- capture favicon -%}{{ "/favicon.ico" | prepend: site.rooturl }}{%- endcapture -%}
  "image": {
    "@type": "ImageObject",
    "url": {{ url | default: favicon | jsonify }}
  }
}
</script>
{%- endif -%}{% endraw %}
```

Molto probabilmente dovrete cambiare alcune variabili poiché sono progettate
per funzionare con il mio file `_config.yml`.

Il codice JSON-LD deve essere messo nella sezione `head` del fine HTML (uso un
`_include/head.html` per questo): tutto quello che dovete fare è aggiungere
`{% raw %}{%- include json_ld.html -%}{% endraw %}`.

Al momento questo codice è eseguito solo per le pagine dei post. Infatti, se
andate su [schema.org](https://schema.org) e cercate `blog` verrete
reindirizzati allo tipo di schema chiamato `BlogPosting`. Potete estendere il
codice JSON-LD come volete a patto di seguire lo
[schema](https://schema.org/BlogPosting).

Ona volta che il codice è attivo r funzionante potete provarlo con la
[JSON-LD playground](https://json-ld.org/playground/) oppure con lo
[strumento di Google](https://search.google.com/test/rich-results). Potete
anche usare un programma a riga di comando come
[jq](https://github.com/jqlang/jq) per controllare che il JSON sia stato
scritto correttamente. Questo sistema, tuttavia, evidenzia solo errori di
sintassi e non valida il vostro JSON, quindi attenzione!

```shell
curl https://blog.franco.net.eu.org/post/simpler-yaml-io-in-python.html \
    | sed -n "/<script type=\"application\/ld+json\">/,/<\/script>/p" \
    | sed '1d' \
    | head -n-1 \
    | jq '.'
```

che risulta in

```json
{
  "@context": "https://schema.org",
  "@type": "BlogPosting",
  "url": "https://blog.franco.net.eu.org/post/simpler-yaml-io-in-python.html",
  "name": "Simpler YAML I/O in Python",
  "headline": "Simpler YAML I/O in Python",
  "keywords": "yaml,python,tutorial",
  "description": "<p>Let’s say you have a YAML file called <code class=\"language-plaintext highlighter-rouge\">toot4feed/constants.yml</code> with this content:</p>",
  "articleBody": "Let’s say you have a YAML file called toot4feed/constants.yml with this\ncontent:\n\ntemplates:\n  rootdir: 'templates'\n  template_data:\n    - type: 'atom'\n      alias: 'atom_entry_filter_date_no_content'\n      relative_path: 'entry/filter_date_no_content.txt'\n    - type: 'atom'\n      alias: 'atom_entry_base_no_content'\n      relative_path: 'entry/base_no_content.txt'\n\ncache:\n  relative_directory: 'toot4feed'\n  filename: 'data.json'\n\n\nYou can get and validate your input using this ugly and error prone method\n\nimport yaml\n\nconfig = yaml.load(open('toot4feed/constants.yml', 'r'), Loader=yaml.SafeLoader)\n\nif ('templates' in config\n   and 'cache' in config\n   and isinstance(config['templates'], dict)\n   and isinstance(config['templates']), dict)\n   and 'rootdir' in config['templates']\n   and isinstance(config['templates']['rootdir'], str)\n   and 'template_data' in config['templates']\n   and isinstance(config['templates']['template_data'], list)):\n    for t in config['templates']['template_data']:\n        if 'type' in t and isinstance(t['type'], str):\n        # All other cases are not shown here...\n            pass\n        else:\n            raise ValueError\n   # You get the idea.\n   pass\nelse:\n    raise ValueError\n\n\nYou manually need to check the existence of every field, check its data type\nand validate it.\n\nFortunately there is a cleaner way. You can neatly organize your content in\ndata classes (more about this later) and use a serialization library such as\nMashumaro to get your data.\n\n\n\n  \n  \n  \n\n  GitHub - Fatal1ty/mashumaro: Fast and well tested serializati...\n  Fast and well tested serialization library. Contribute to Fatal1ty/mashumaro development by creating an account on GitHub.\n\n\n\nLet’s install the library first, including PyYAML:\n\npip install --user mashumaro[yaml]\n\n\n\n\n  \n  \n  \n\n  mashumaro · PyPI\n  Fast and well tested serialization library\n\n\n\nThis is the first version of the program\n\nfrom dataclasses import dataclass\nfrom mashumaro.mixins.yaml import DataClassYAMLMixin\nimport pathlib\nimport platformdirs\n\n@dataclass\nclass FeedType:\n    type: str\n    alias: str\n    relative_path: str\n\n\n@dataclass\nclass Templates():\n    rootdir: str\n    template_data: list[FeedType]\n\n@dataclass\nclass Cache:\n    relative_directory: str\n    filename: str\n\n    def __post_init__(self):\n        self.absolute_directory: str = platformdirs.user_cache_dir(self.relative_directory)\n        self.pathlib_absolute_filename: pathlib.Path = pathlib.Path(self.absolute_directory, self.filename)\n\n@dataclass\nclass Constants(DataClassYAMLMixin):\n    templates: Templates\n    cache: Cache\n\n\n# Load the constants YAML file.\nconst: Constants = Constants.from_yaml(pathlib.Path('toot4feed/constants.yml').read_text())\n\n\nWhat is going on here? Data classes are special type of classes suited to\nhold data: you avoid lots of boilerplate code that you normally have to write\nwith standard classes. For example, by default, dataclasses provide the\n__init__ method, so there is no need to write it.\n\n\n\n  \n  \n  \n\n  dataclasses — Data Classes — Python 3.12.1 documentation\n  Source code: Lib/dataclasses.py This module provides a decorator and functions for automatically adding generated special met...\n\n\n\nAll you have to do is to define attributes using Python typing annotations.\n\n\n\n  \n  \n  \n\n  typing — Support for type hints — Python 3.12.1 documentation\n  Source code: Lib/typing.py This module provides runtime support for type hints. For the original specification of the typing ...\n\n\n\nWhen you design these classes you should start with the inner-most component:\nin this case we have the feed type.\n\n@dataclass\nclass FeedType:\n    type: str\n    alias: str\n    relative_path: str\n\n\nIf we extract the first element from the template_data list and remove the\nlist notation we get:\n\ntype: 'atom'\nalias: 'atom_entry_filter_date_no_content'\nrelative_path: 'entry/filter_date_no_content.txt'\n\n\nindeed, all these 3 variables are of type str and are required.\n\nNow, let’s go up a level. We get this:\n\ntemplates:\n  rootdir: 'templates'\n  template_data: []         # Empty list notation.\n\n\nrootdir is a string. Easy. template_data is a list of something; a list\nof FeedType.\n\n@dataclass\nclass Templates():\n    rootdir: str\n    template_data: list[FeedType]\n\n\nCache is a simple object composed by two strings only.\n\n@dataclass\nclass Cache:\n    relative_directory: str\n    filename: str\n\n\nThe __post_init__ function sets two other variables used for locating the\ncache file. They are placed here and not with the other variables for matters\nof context.\n\ndef __post_init__(self):\n    self.absolute_directory: str = platformdirs.user_cache_dir(self.relative_directory)\n    self.pathlib_absolute_filename: pathlib.Path = pathlib.Path(self.absolute_directory, self.filename)\n\n\nIn the root level of the YAML file we have 2 elements, templates and cache.\nYou just need to point the two variable types to the classes we previously\ndiscussed.\n\n@dataclass\nclass Constants(DataClassYAMLMixin):\n    templates: Templates\n    cache: Cache\n\n\nInheriting from the DataClassYAMLMixin class enables us to use several\nmethods from the Mashumaro library, such as from_yaml. This method populates\nthe instance of the Constants object (const) with the file content. It\naccepts a string, so we need to open the file and read it first using the\npathlib.Path.read_text method.\n\nconst: Constants = Constants.from_yaml(pathlib.Path('toot4feed/constants.yml').read_text())\n\n\nNow we can print the content of const:\n\n&gt;&gt;&gt; import pprint\n&gt;&gt;&gt; pprint.pprint(const)\nConstants(templates=Templates(rootdir='templates',\n                              template_data=[FeedType(type='atom',\n                                                      alias='atom_entry_filter_date_no_content',\n                                                      relative_path='entry/filter_date_no_content.txt'),\n                                             FeedType(type='atom',\n                                                      alias='atom_entry_base_no_content',\n                                                      relative_path='entry/base_no_content.txt')]),\n          cache=Cache(relative_directory='toot4feed', filename='data.json'))\n\n\nThe two variables in the __post_init__ method are not shown, but they are\nthere:\n\n&gt;&gt;&gt; pprint.pprint(const.cache.absolute_directory)\n'/home/vm/.cache/toot4feed'\n&gt;&gt;&gt; pprint.pprint(const.cache.pathlib_absolute_filename)\nPosixPath('/home/vm/.cache/toot4feed/data.json')\n\n\nThe reason for this is that the dataclass does not add these variables to the\n__repr__ method, as explained in the\ndocumentation\n\n\n\n  \n  \n  \n\n  3. Data model — Python 3.12.1 documentation\n  Objects, values and types: Objects are Python’s abstraction for data. All data in a Python program is represented by objects ...\n\n\n\nYou can “fix” this behaviour by adding the fields in the class.\n\n+from dataclasses import dataclass, field\n-from dataclasses import dataclass\nfrom mashumaro.mixins.yaml import DataClassYAMLMixin\nimport pathlib\nimport platformdirs\n+import pprint\n\n@dataclass\nclass FeedType:\n    type: str\n    alias: str\n    relative_path: str\n\n\n@dataclass\nclass Templates():\n    rootdir: str\n    template_data: list[FeedType]\n\n@dataclass\nclass Cache:\n    relative_directory: str\n    filename: str\n+   absolute_directory: str = field(init=False)\n+   pathlib_absolute_filename: pathlib.Path = field(init=False)\n\n\n    def __post_init__(self):\n        self.absolute_directory = platformdirs.user_cache_dir(self.relative_directory)\n        self.pathlib_absolute_filename = pathlib.Path(self.absolute_directory, self.filename)\n\n@dataclass\nclass Constants(DataClassYAMLMixin):\n    templates: Templates\n    cache: Cache\n\n\n# Load the constants YAML file.\nconst: Constants = Constants.from_yaml(pathlib.Path('toot4feed/constants.yml').read_text())\n+pprint.pprint(const)\n\n\nWhich leads to:\n\nConstants(templates=Templates(rootdir='templates',\n                              template_data=[FeedType(type='atom',\n                                                      alias='atom_entry_filter_date_no_content',\n                                                      relative_path='entry/filter_date_no_content.txt'),\n                                             FeedType(type='atom',\n                                                      alias='atom_entry_base_no_content',\n                                                      relative_path='entry/base_no_content.txt')]),\n          cache=Cache(relative_directory='toot4feed',\n                      filename='data.json',\n                      absolute_directory='/home/vm/.cache/toot4feed',\n                      pathlib_absolute_filename=PosixPath('/home/vm/.cache/toot4feed/data.json')))\n\n\nHowever, as I said before, I’m going to leave these two variables in the\n__post_init__ method only.\n\nWhat happens if we set a variable with a different type in the YAML file, for\nexample by setting a str variable to an int, like this?\n\ntemplates:\n  rootdir: 'templates'\n  template_data:\n    - type: 1\n    # [ ... ]\n\n\nIf we run the program, we get this print\n\nConstants(templates=Templates(rootdir='templates',\n                              template_data=[FeedType(type=1,\n                                                      alias='atom_entry_filter_date_no_content',\n                                                      relative_path='entry/filter_date_no_content.txt'),\n                                             FeedType(type='atom',\n                                                      alias='atom_entry_base_no_content',\n                                                      relative_path='entry/base_no_content.txt')]),\n          cache=Cache(relative_directory='toot4feed', filename='data.json'))\n\n\nOk, it does not work as expected: the first type is now 1 which is an\nint and not a str. But why? This issue explains the reason.\n\n\n\n  \n  \n  \n\n  Inconsistent checks for invalid value type for str field type...\n  If a class based on DataClassDictMixin has a field with type str it will construct instances from data that contains data of ...\n\n\n\nWhat you need to do for some data types such as str is to enable validation,\nbecause it is disabled by default for sake of performance. If we set it up\nlike this, it works:\n\nfrom dataclasses import dataclass, field\nfrom mashumaro.mixins.yaml import DataClassYAMLMixin\nimport pathlib\nimport platformdirs\nimport pprint\n+\n+def validate_str(value):\n+    if not isinstance(value, str):\n+        raise ValueError\n+    return value\n+\n+\n+class EnableStrValidation:\n+     class Config:\n+        serialization_strategy = {\n+            str: {\n+                \"deserialize\": validate_str,\n+            },\n+        }\n\n\n@dataclass\nclass FeedType(EnableStrValidation):\n+    type: str = field(metadata={\"deserialize\": validate_str})\n+    alias: str = field(metadata={\"deserialize\": validate_str})\n+    relative_path: str = field(metadata={\"deserialize\": validate_str})\n-    type: str\n-    alias: str\n-    relative_path: str\n\n@dataclass\nclass Templates(EnableStrValidation):\n+   rootdir: str = field(metadata={\"deserialize\": validate_str})\n-   rootdir: str\n    template_data: list[FeedType]\n\n@dataclass\nclass Cache(EnableStrValidation):\n+    relative_directory: str = field(metadata={\"deserialize\": validate_str})\n+    filename: str = field(metadata={\"deserialize\": validate_str})\n-    relative_directory: str\n-    filename: str\n\n    def __post_init__(self):\n        self.absolute_directory = platformdirs.user_cache_dir(self.relative_directory)\n        self.pathlib_absolute_filename = pathlib.Path(self.absolute_directory, self.filename)\n\n\n@dataclass\nclass Constants(DataClassYAMLMixin):\n    templates: Templates\n    cache: Cache\n\n\n# Load the constants YAML file.\nconst: Constants = Constants.from_yaml(pathlib.Path('toot4feed/constants.yml').read_text())\npprint.pprint(const)\n\n\nThe first raised exception is this one:\n\nTraceback (most recent call last):\n  File \"&lt;string&gt;\", line 8, in __mashumaro_from_dict__\n  File \"toot4feed/toot4feed/constants.py\", line 11, in validate_str\n    raise ValueError\nValueError\n\nDuring handling of the above exception, another exception occurred:\n\nTraceback (most recent call last):\n  File \"&lt;string&gt;\", line 11, in __mashumaro_from_dict__\n  File \"&lt;string&gt;\", line 11, in &lt;listcomp&gt;\n  File \"&lt;string&gt;\", line 10, in __mashumaro_from_dict__\nmashumaro.exceptions.InvalidFieldValue: Field \"type\" of type str in FeedType has invalid value 1\n\n\nTry setting a string in an integer field. In this case type validation works\nout of the box.\n\nOk so now we can read, validate data and load it in variables. We can also\noutput it. Using the to_yaml method is all we need\n\nprint(const.to_yaml())\n\n\ncache:\n  absolute_directory: /home/vm/.cache/toot4feed\n  filename: data.json\ntemplates:\n  rootdir: templates\n  template_data:\n  - alias: atom_entry_filter_date_no_content\n    relative_path: entry/filter_date_no_content.txt\n    type: atom\n  - alias: atom_entry_base_no_content\n    relative_path: entry/base_no_content.txt\n    type: atom\n\n\nTa-da 🚀\n\nMashumaro supports other file types as well: JSON, TOML and MessagePack.\n\n\n\n  \n  \n  \n\n  GitHub - Fatal1ty/mashumaro: Fast and well tested serializati...\n  Fast and well tested serialization library. Contribute to Fatal1ty/mashumaro development by creating an account on GitHub.\n\n\n\n",
  "datePublished": "2023-12-06 00:00:00 +0100",
  "dateModified": "2023-12-08 17:10:00 +0000",
  "license": "https://creativecommons.org/licenses/by-sa/4.0/",
  "author": {
    "@type": "Person",
    "name": "Franco Masotti",
    "email": "email"
  },
  "publisher": {
    "@type": "Organization",
    "name": "Franco Masotti software developer blog",
    "url": "https://blog.franco.net.eu.org",
    "logo": {
      "@type": "ImageObject",
      "width": 32,
      "height": 32,
      "url": "https://blog.franco.net.eu.org/favicon.ico"
    }
  },
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "https://blog.franco.net.eu.org/post/simpler-yaml-io-in-python.html"
  },
  "image": {
    "@type": "ImageObject",
    "url": "https://blog.franco.net.eu.org/favicon.ico"
  }
}
```

Qui c'é un esempio di un altro sito che usa JSON-LD. Qui abbiamo una
cosiddétta `BreadcrumbList`, e non un `BlogPosting`

```json
{
  "@context": "https://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [
    {
      "@type": "ListItem",
      "position": 1,
      "item": {
        "@id": "https://www.ansa.it/",
        "name": "Ansa"
      }
    },
    {
      "@type": "ListItem",
      "position": 2,
      "item": {
        "@id": "https://www.ansa.it/english/",
        "name": "ANSA English"
      }
    },
    {
      "@type": "ListItem",
      "position": 3,
      "item": {
        "@id": "https://www.ansa.it/english/news/science_tecnology/science_tecnology.shtml",
        "name": "Science - Tecnology"
      }
    },
    {
      "@type": "ListItem",
      "position": 4,
      "item": {
        "@id": "https://www.ansa.it/english/news/science_tecnology/2023/12/14/one-of-mysteries-of-strange-superconductor-materials-solved_c9d25fa2-41da-438e-99e2-18b329bc383e.html",
        "name": "One of mysteries of strange superconductor materials solved"
      }
    }
  ]
}
```

Ovviamente, invece che usare questi strumenti da riga di comando, si può
fare anche tutto in Python usando
[BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
e i moduli di default.
