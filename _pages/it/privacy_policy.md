---
layout: page
title: Privacy policy
permalink: /it/privacy-policy/
description: ! 'Privacy policy di blog.franco.net.eu.org'
lang: 'it'
---

In questo blog si applicano le stesse regole si [software.franco.net.eu.org](https://software.franco.net.eu.org)
. Dai un'occhiata al paragrafo
[*Privacy policy*](https://software.franco.net.eu.org/frnmst/software.franco.net.eu.org/src/branch/master/privacy_policy.md#privacy-policy).

Non viene usato alcun cookie.
