---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fattura-elettronica-reader
software_name_python_module: fattura_elettronica_reader
software_version: 3.0.4
software_version_raw: 000003.000000.000004
release_timestamp: 2023-01-09 17:24:01
is_on_pypi: true
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---

### Removed

- Removed [python-atomicwrites](https://github.com/untitaker/python-atomicwrites)
  (`atomicwrites` is the Python requirement name) library dependency because
  of its deprecation in July 2022. Atomic writes are now done through
  temporary files without external libraries, as
  [suggested](https://github.com/untitaker/python-atomicwrites/blob/4183999d9b7e81af85dee070d5311299bdf5164c/README.rst#unmaintained)
  by the author of python-atomicwrites.
- Removed [requests](https://github.com/psf/requests) library dependency and
  replaced the only use of it in fattura-elettronica-reader with the build-in
  `urllib.request` module.

### Fixed

- Pinned YAPF and flake8 versions through the pre-commit update command
  in the Makefile.
- Fixed pre-commit hooks.
- Updated assets checksum.
- Fixed gitignore file.

### Changed

- Implemented use of `setup.cfg` and `pyproject.toml`.
  `setup.py` is now a dummy file.
- Replaced Pipenv environment with `venv` and `pip` commands.
- Replaced distribution commands (`setup.py sdist`, `setup.py bdist_wheel`)
  with the [`build`](https://pypa-build.readthedocs.io/en/stable/) module.
- Pinned [fpyutils](https://blog.franco.net.eu.org/software/#fpyutils)
  dependency at least to version 3.0.1.
- Use of modern tools to install the package from the PKGBUILD file.
  See
  [Standards based (PEP 517)](https://wiki.archlinux.org/title/Python_package_guidelines#Standards_based_(PEP_517))
  vs
  [setuptools or distutils](https://wiki.archlinux.org/title/Python_package_guidelines#setuptools_or_distutils)
- Moved some pre-commit options to `setup.cfg`.
- Changed Sphinx theme.
- Updated dependencies.
