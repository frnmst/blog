---
layout: page
title: About
permalink: /about/
description: ! 'About and contacts of Franco Masotti'
lang: 'en'
---

This blog is maintained by Franco Masotti. You will find posts about computer
science, software, hardware, hacks, technology, etc...

I hope these posts turn out to be useful.

## Contacts

| Repositories |
|:-------------|
| [Canonical repositories](https://software.franco.net.eu.org/frnmst) |
| [Codeberg](https://codeberg.org/frnmst) |
| [Framagit](https://framagit.org/frnmst) |
| [GitHub](https://github.com/frnmst) (see [this post]({%- include post/post_url_iso639.liquid post_url="2021-09-02-quitting-github" -%}{{ post_url_iso639 }})) |

| Emails |
|:-------|
| [my first name].[my last name]@tutanota.com |
| [my first name].[my last name].1@protonmail.com |

| Other |
|:------|
| [Solve Computer Science YouTube channel](https://www.youtube.com/channel/UC2rr0LbIuy34JHEoCndmKiA) |
| [anonymous share via Nextcloud](https://cloud.franco.net.eu.org/s/QyqgfCPHdemjCcp) |
| Nextcloud (Open Cloud Mesh protocol) at [frnmst@cloud.franco.net.eu.org](https://cloud.franco.net.eu.org)
| [Keyoxide ID](https://keyoxide.org/396da54dc8019e4f4522cbd5a3fa3c2b4230215a) |

## Education

- Bachelor degree in computer science @ University of Ferrara: [*Integrazione di una applicazione web con strumenti per la statistica*](https://software.franco.net.eu.org/frnmst-archives/thesis)
- [ORCID iD](https://orcid.org/0000-0002-1736-3858)
- [CS198.1x: Bitcoin and Cryptocurrencies](https://courses.edx.org/certificates/2849569d6b2e422e8652b5ff93f8ea09)

## My PGP public keys

- [Public key for signing Git commits](/pubkeys/pgp_pubkey_since_2019.txt)
- [Keyoxide ID public key](/pubkeys/pgp_pubkey_frnmst_keyoxide.txt)
