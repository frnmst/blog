---
layout: course
title: ! 'Physical exercise courses'
permalink: /courses/health/physical-exercises/
description: ! 'Physical exercise courses'
enable_markdown: true
lang: 'en'
courses:
  comment: ! 'Some health tips that have been or are working for me, made by other people on YouTube.'
  course:
  - subject: ! 'Back'
    comment: ! ''
    data:
      - id: 0
        type: video
        title: ! 'Improve hunchback posture'
        comment: ! 'This worked as promised.'
        embed_html: |
          <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/LT_dFRnmdGs?si=b3g6lVoABrAZ-3NI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

        backup_url: ! 'https://assets.franco.net.eu.org/video/blog.franco.net.eu.org/Exercise%20to%20improve%20hunchback%20posture%20%20%20forward%20head%20carriage%20correction%20%5bLT_dFRnmdGs%5d.webm'
      - id: 1
        type: video
        title: ! 'Fix "Hunchback" Posture in 10 Minutes/Day'
        embed_html: |
          <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/gfSCZbb9MOY?si=jBZ2EmDa0XCK-ZIC" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

        backup_url: ! 'https://assets.franco.net.eu.org/video/blog.franco.net.eu.org/Fix%20%ef%bc%82Hunchback%ef%bc%82%20Posture%20in%2010%20Minutes%e2%a7%b8Day%20(Daily%20Exercise%20Routine)%20%5bgfSCZbb9MOY%5d.webm'
  - subject: ! 'Muscle gains'
    comment: ! ''
    data:
      - id: 0
        type: video
        title: ! 'Home workout'
        embed_html: |
          <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/511xeUmG11k?si=MuDOGCRiq-A1Ad08" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

        backup_url: ! 'https://assets.franco.net.eu.org/video/blog.franco.net.eu.org/Exercises%20With%205%20Liter%20Bottle%20Burn%20Fat%20(%20Home%20Workout)%20%5b511xeUmG11k%5d.webm'
      - id: 1
        type: video
        title: ! 'Daily Home Ab Workout'
        embed_html: |
          <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/95hX_OMuIpg?si=4axOvQ7QS_0DLpRU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

        backup_url: ! 'https://assets.franco.net.eu.org/video/blog.franco.net.eu.org/7%20Minute%20Daily%20Home%20Ab%20Workout%20(GET%206%20PACK%20ABS%20FAST)%20%5b95hX_OMuIpg%5d.webm'
      - id: 2
        type: video
        title: ! 'No pull up bar at home'
        embed_html: |
          <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/fYS-rSmedCE?si=UOdNda8HGTgx_Pdj" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

        backup_url: ! 'https://assets.franco.net.eu.org/video/blog.franco.net.eu.org/We%20tried%20to%20do%20pull%20ups%20WITHOUT%20a%20pull%20up%20bar%20(it%20works)%20%ef%bd%9c%20No%20pull%20up%20bar%20needed%20%5bfYS-rSmedCE%5d.webm'
      - id: 3
        type: video
        title: ! "Can't do pushups?"
        embed_html: |
          <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/zkU6Ok44_CI?si=6j-_6c_0rwDGksZQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

        backup_url: ! 'https://assets.franco.net.eu.org/video/blog.franco.net.eu.org/You%20CAN%20do%20pushups,%20my%20friend!%20(2022%20Version)%20%5bzkU6Ok44_CI%5d.webm'

  - subject: ! 'Running'
    comment: ! ''
    data:
      - id: 0
        type: video
        title: ! 'Breathing'
        embed_html: |
          <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/wSZ9w2T2Iqw?si=hQyx1W5gYtyO2b3O" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

        backup_url: ! 'https://assets.franco.net.eu.org/video/blog.franco.net.eu.org/DO%20THIS%20DAILY%20If%20You%20Struggle%20to%20Breathe%20While%20Running%20%5bwSZ9w2T2Iqw%5d.webm'
      - id: 1
        type: video
        title: ! 'Back pain after running (fix)'
        comment: ! 'Really simple tip that worked immediately.'
        embed_html: |
          <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/j-UIfv1sZU0?si=tPiKBv0WoaLKLS8r" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

        backup_url: ! 'https://assets.franco.net.eu.org/video/blog.franco.net.eu.org/If%20you%20run%20with%20lower%20back%20pain...%20WATCH%20THIS%20%5bj-UIfv1sZU0%5d.webm'
      - id: 2
        type: video
        title: ! 'Running/walking warmup'
        comment: ! 'Really simple, just takes 5 mins'
        embed_html: |
          <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/u-e0ZO5L0s0?si=X5oPBlkpGJ37YHIj" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

        backup_url: ! 'https://assets.franco.net.eu.org/video/blog.franco.net.eu.org/5%20MIN%20Pre-Run%20Stretching%20Routine%20%5bu-e0ZO5L0s0%5d.webm'

---
