---
meta: true
software_name: fpyutils
archived: false
repositories:
  - label: 'canonical'
    url: 'https://software.franco.net.eu.org/frnmst/fpyutils'
  - label: 'codeberg'
    url: 'https://codeberg.org/frnmst/fpyutils'
  - label: 'framagit'
    url: 'https://framagit.org/frnmst/fpyutils'
  - label: 'github'
    url: 'https://github.com/frnmst/fpyutils'
documentation:
  - label: 'docs.franco.net.eu.org/fpyutils'
    url: 'https://docs.franco.net.eu.org/fpyutils'
  - label: 'changelog'
    url:
related_links:
  - label: 'Repology python:fpyutils (package tracking)'
    url: 'https://repology.org/project/python:fpyutils/versions'
  - label: 'Repology fpyutils (package tracking)'
    url: 'https://repology.org/project/fpyutils/versions'
  - label: 'Libraries.io'
    url: 'https://libraries.io/pypi/fpyutils'
  - label: 'Liberapay'
    url: 'https://en.liberapay.com/frnmst/'
  - label: 'Buy Me a Coffee'
    url: 'https://www.buymeacoffee.com/frnmst'
---
