---
layout: page
title: ! 'Emplois'
permalink: /fr/jobs/
description: ! 'Mes métiers et services en informatique'
lang: 'fr'
---

## Critiques sur Fiverr

{% include fiverr_gigs.html %}

## Scripts et documentation

Je peux
[écrire des scripts autonomes qui interagissent avec les API, etc...](https://it.fiverr.com/s/4eZ65b)
ainsi qu'une [documentation logicielle détaillée](https://it.fiverr.com/s/b47YA1).

## Leçons

Je donne des cours d'informatique (programmation, base de données, réseaux,
Linux, etc...) via appel vidéo sur Nextcloud, Skype, Google Meet, Zoom, etc...

## Autre

- Installations Linux
- Installations Android ROM
- Solutions aux problèmes génériques du PC
- Récupération de données (si la mémoire n'est pas physiquement endommagée, par
  exemple si l'utilisateur a involontairement supprimé des fichiers)

## Contacts

Voir le paragraphe [contacts]({{- '/about/' | relative_url -}}#contacts).
