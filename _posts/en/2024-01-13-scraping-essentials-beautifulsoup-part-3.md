---
title: ! 'Scraping essentials: BeautifulSoup - part 3'
tags: [scraping, beautifulsoup, amazon, mechanicalsoup, python, tutorial, amazon-scraper]
updated: 2024-01-13 22:41:00
description: ! 'Automatically perform a search on Amazon and scrape information'
lang: 'en'
images:
  - id: 'scraping_result'
    filename: 'scraping_result.png'
    alt: ! 'A comparison between the homepage and a search result page on the right and its JSON scraped data on a terminal on the left'
    caption: ! 'Getting JSON data from Amazon using several *tricks*'
    width: '300px'
  - id: 'amazon_search_bar'
    filename: 'amazon_search_bar.png'
    alt: ! 'The search bar is highlighed and a text "This is a form" is display below it, with an arrow pointing to the search bar'
    caption: ! 'Highlighted search bar'
    width: '300px'
  - id: 'search_bar_selection'
    filename: 'search_bar_selection.png'
    alt: ! 'The search bar is selected and pointed by an arrow from the developer options. The HTML form is highlighted with a red rectangle'
    caption: ! 'Get the form using the `id` attribute'
    width: '300px'
  - id: 'form_input_by_name_attribute'
    filename: 'form_input_by_name_attribute.png'
    alt: ! 'The search bar is selected and pointed by an arrow from the developer options. The HTML input and the `name` attribute are highlighted with a red rectangle'
    caption: ! 'Get the form input using the `name` attribute'
    width: '300px'
---
{% include image.html id="scraping_result" %}

<!--excerpt_start-->After
[obtaining the product URLs]({%- include post/post_url_iso639.liquid post_url="2024-01-12-scraping-essentials-beautifulsoup-part-2" -%}{{ post_url_iso639 }} "{{ post_title_iso639 }}")
and
[scraping their data]({%- include post/post_url_iso639.liquid post_url="2024-01-11-scraping-essentials-beautifulsoup" -%}{{ post_url_iso639 }} "{{ post_title_iso639 }}")
we'll see how to automatically perform a query<!--excerpt_end-->.

## Table of contents

<!--TOC-->

- [Table of contents](#table-of-contents)
- [Setup](#setup)
- [Getting the search bar element](#getting-the-search-bar-element)
  - [Getting the form](#getting-the-form)
  - [Setting the value](#setting-the-value)
  - [Sumbmit](#sumbmit)
- [Getting all the results](#getting-all-the-results)
- [JSON](#json)
- [Simpler alternative](#simpler-alternative)
- [Conclusion](#conclusion)

<!--TOC-->

## Setup

The setup phase is done in the same way as before: select a user agent and
open the URL which is Amazon's home page this time.

```python
url ='https://www.amazon.it'
browser_user_agent = 'Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Ubuntu/10.10 Chromium/10.0.648.0 Chrome/10.0.648.0 Safari/534.16'
browser = mechanicalsoup.StatefulBrowser(
        user_agent=browser_user_agent,
)
browser.open(url)
```

## Getting the search bar element

{% include image.html id="amazon_search_bar" %}

The search bar you see in the upper section of the page is a form. There
are three steps to use it:

1. select it in a way that is useful for MechanicalSoup to fill it in and send it later
2. set a value for the form
3. submit the modified form

### Getting the form

{% include image.html id="search_bar_selection" %}

Use the
[select_form](https://mechanicalsoup.readthedocs.io/en/stable/mechanicalsoup.html?highlight=select_form#mechanicalsoup.StatefulBrowser.select_form "select_form() - MeachanicalSoup")
method of MechanicalSoup. See [this example as well](https://mechanicalsoup.readthedocs.io/en/stable/tutorial.html?highlight=select_form#first-contact-step-by-step "MechanicalSoup tutorial")

```python
search_form = browser.select_form('form[id="nav-search-bar-form"]')
```

As stated in the documentation, the argument of `select_form()` must be a CSS
selector.

After this instruction, the `browser` object knows that we have selected that
specific search bar, so we can carry on and add some data to it.

### Setting the value

{% include image.html id="form_input_by_name_attribute" %}

Setting a value for the form is the easiest thing in the world: just set the
attributes based on the `name` attribute of the HTML `input` tag.

```python
browser["field-keywords"] = 'SSD'
```

### Sumbmit

Since we selected the form earlier the only thing to do now is to *send it* 👍

```python
response = browser.submit_selected()
```

The value we're interested in is stored in `response.url`. This value will
be similar to the one you get by using the form manually. It may be this one,
for example

```
https://www.amazon.it/s?k=ssd&__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=36NTQWY8CKJSZ&sprefix=ssd%2Caps%2C227&ref=nb_sb_noss_1
```

## Getting all the results

Now we can call the code of the
[previous post]({%- include post/post_url_iso639.liquid post_url="2024-01-11-scraping-essentials-beautifulsoup" -%}{{ post_url_iso639 }} "{{ post_title_iso639 }}").
You should group this in a single function

```python
s = get_search_results(response.url, browser_user_agent)
```

## JSON

We now have all the data stored in the `ProductSearch` dataclass. We can
easily export it to JSON using the
[Mashumaro library](https://github.com/Fatal1ty/mashumaro "GitHub - Fatal1ty/mashumaro: Fast and well tested serialization library")
as I
[explained in an older post]({%- include post/post_url_iso639.liquid post_url="2023-12-06-simpler-yaml-io-in-python" -%}{{ post_url_iso639 }} "{{ post_title_iso639 }}")

```python
print(s.to_json())
```

This is the (formatted) result limited to the first three products:

```json
{
  "products": {
    "products": [
      {
        "url": "https://www.amazon.it/Crucial-BX500-CT480BX500SSD1-Interno-Pollici/dp/B07G3KGYZQ/ref=sr_1_5?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dib=eyJ2IjoiMSJ9.olQJm1lYim2GiOQfaT7nklxTkGGGaUj3p6hGGtBDpjL2OIiDaaRZXitp66NK-Y5rNqXURjrBxqs4mowQ-h7u8zx1VoqoQCVdr5G2MATol-Mmn9SD35qtk1QgPM_qqUuOJiWIiPBj67qN4T9jAT50cQ.Ivu7tY1h_V8lkdtLGcI9NJnYGw2-Y45CRBkSgpP2z_Y&dib_tag=se&keywords=ssd&qid=1705182255&sr=8-5",
        "asin_raw": "B07G3KGYZQ",
        "asin": "B07G3KGYZQ",
        "title_raw": "Crucial BX500 480GB 3D NAND SATA da 2,5 Pollici SSD Interno - Fino a 540MB/s - CT480BX500SSD1",
        "title": "Crucial BX500 480GB 3D NAND SATA da 2,5 Pollici SSD Interno - Fino a 540MB/s - CT480BX500SSD1",
        "price_raw": "40,99€",
        "price": "40.99",
        "currency_symbol_raw": "€",
        "currency_symbol": "€",
        "availabile_raw": "Nuovo & Usato (16) da38,94€38,94€  Spedizione GRATUITA.",
        "availabile": true,
        "rating_raw": "4,7",
        "rating": 4.7,
        "reviews_raw": "114.812 voti",
        "reviews": 114812
      },
      {
        "url": "https://www.amazon.it/Silicon-Power-Performance-Pollici-interno/dp/B07KR1GFY5/ref=sr_1_6?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dib=eyJ2IjoiMSJ9.olQJm1lYim2GiOQfaT7nklxTkGGGaUj3p6hGGtBDpjL2OIiDaaRZXitp66NK-Y5rNqXURjrBxqs4mowQ-h7u8zx1VoqoQCVdr5G2MATol-Mmn9SD35qtk1QgPM_qqUuOJiWIiPBj67qN4T9jAT50cQ.Ivu7tY1h_V8lkdtLGcI9NJnYGw2-Y45CRBkSgpP2z_Y&dib_tag=se&keywords=ssd&qid=1705182255&sr=8-6",
        "asin_raw": "B07KR1GFY5",
        "asin": "B07KR1GFY5",
        "title_raw": "Silicon Power SSD 512GB 3D NAND A55 SLC Cache Performance Boost 2.5 Pollici SATA III 7mm (0.28\") SSD interno",
        "title": "Silicon Power SSD 512GB 3D NAND A55 SLC Cache Performance Boost 2.5 Pollici SATA III 7mm (0.28\") SSD interno",
        "price_raw": "34,50€",
        "price": "34.50",
        "currency_symbol_raw": "€",
        "currency_symbol": "€",
        "availabile_raw": "Nuovo (3) da34,50€34,50€  & Spedizione GRATUITA",
        "availabile": true,
        "rating_raw": "4,6",
        "rating": 4.6,
        "reviews_raw": "16.113 voti",
        "reviews": 16113
      },
      {
        "url": "https://www.amazon.it/Samsung-Memorie-Tecnologia-Intelligent-Software/dp/B08PC43D78/ref=sr_1_7?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dib=eyJ2IjoiMSJ9.olQJm1lYim2GiOQfaT7nklxTkGGGaUj3p6hGGtBDpjL2OIiDaaRZXitp66NK-Y5rNqXURjrBxqs4mowQ-h7u8zx1VoqoQCVdr5G2MATol-Mmn9SD35qtk1QgPM_qqUuOJiWIiPBj67qN4T9jAT50cQ.Ivu7tY1h_V8lkdtLGcI9NJnYGw2-Y45CRBkSgpP2z_Y&dib_tag=se&keywords=ssd&qid=1705182255&sr=8-7",
        "asin_raw": "B08PC43D78",
        "asin": "B08PC43D78",
        "title_raw": "Samsung 870 EVO SSD, Fattore di forma 2,5\", Intelligent Turbo Write, software Magician 6, Nero, 500 GB",
        "title": "Samsung 870 EVO SSD, Fattore di forma 2,5\", Intelligent Turbo Write, software Magician 6, Nero, 500 GB",
        "price_raw": "56,89€",
        "price": "56.89",
        "currency_symbol_raw": "€",
        "currency_symbol": "€",
        "availabile_raw": "Nuovo & Usato (159) da56,61€56,61€  Spedizione GRATUITA.",
        "availabile": true,
        "rating_raw": "4,8",
        "rating": 4.8,
        "reviews_raw": "34.605 voti",
        "reviews": 34605
      }
    ]
  },
  "url": "https://www.amazon.it/s/ref=nb_sb_noss?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&url=search-alias%3Daps&field-keywords=ssd"
}
```

## Simpler alternative

In this specific case, instead of acting on the form, we could have used the
URL *query* component directly:

```
https://www.amazon.it/s?k=ssd
```

So, to put it in Python terms, all we need is one of these:

```python
>>> URL = 'https://www.amazon.it/s'
>>> QUERY = 'ssd'
>>> FULL_URL = ''.join([URL, '?k=', QUERY])
>>> print(FULL_URL)
'https://www.amazon.it/s?k=ssd'

>>> FULL_URL = 'https://www.amazon.it/s?k=' + QUERY
>>> print(FULL_URL)
'https://www.amazon.it/s?k=ssd'
```

## Conclusion

In this post we've seen how to query the Amazon search bar to get the result
page and all the data we need. The next post will contain the full Python
code of this Amazon scraping exercise.
