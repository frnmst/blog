---
meta: true
software_name: licheck
archived: false
repositories:
  - label: 'canonical'
    url: 'https://software.franco.net.eu.org/frnmst/licheck'
  - label: 'codeberg'
    url: 'https://codeberg.org/frnmst/licheck'
  - label: 'framagit'
    url: 'https://framagit.org/frnmst/licheck'
documentation:
  - label: 'docs.franco.net.eu.org/licheck'
    url: 'https://docs.franco.net.eu.org/licheck'
  - label: 'changelog'
    url:
---
