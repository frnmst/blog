---
title: ! 'GitHub: can no longer search code without being logged in'
tags: [github, git, code-search, discussion]
related_links: ['https://github.com/orgs/community/discussions/77046', 'https://github.com/orgs/community/discussions/77046#discussioncomment-7692613']
updated: 2023-11-28 17:45:00
description: ! 'My answer to the GitHub community discussion (can no longer search code without being logged in)'
lang: 'en'
canonical_url: 'https://github.com/orgs/community/discussions/77046#discussioncomment-7692613'
---

This is
[my answer](https://github.com/orgs/community/discussions/77046#discussioncomment-7692613)
to the
[Can no longer search code without being logged in. #77046](https://github.com/orgs/community/discussions/77046)
GitHub community discussion:

> I find this *feature* kind of annoying as well. My current solution to find something quickly, if limited to a single repo is:
>
> 1. `cd /dev/shm` (don't use up precious disk read/writes)
> 2. `git clone https://github.com/user/repo`
> 3. `cd repo`
> 4. `grep -r "pattern" .`
> 5. `cd ..`
> 6. `rm -rf repo`
>
> If you want to search for a pattern all over GitHub you can use a normal search engine, with something like: `"pattern" site:github.com`. I don't need to search through all GitHub frequently, but I don't think using an external search engine is as *powerful* as using GitHub's built-in one. Maybe some code is not indexed. Moreover, this system only works for public repos.
>
> > This is primarily to ensure we can support the load for developers on GitHub and help protect the servers from being overwhelmed by anonymous requests from bots etc.
>
> This seems a mitigation to me. What about if someone creates n GitHub bot accounts using n different emails and remains well below API limits for each bot? I think the bot *manager* would win. It would be much simpler to disable code search altogether (!) Problem solved.
>
> As mentioned in other comments there are other GIT forges (Gitlab (Framagit), Gitea, Forgejo (Codeberg), Gogs, etc...). I think some of these allow you to enable code search if you self-host. Apparently Codeberg and Framagit don't have code search enabled at this moment. These two [Codeberg](https://codeberg.org/Codeberg/Community/issues/379) [issues](https://codeberg.org/Codeberg/Contributing/issues/26) seem also interesting.
>
> ## Personal advice
>
> Not directly related to code search but more broad: my personal advice is to treat GitHub as *one of the mirrors of your projects*. GIT is by nature decentralized (Distributed Version Control System (DVCS)). If you use GitHub (or any other) as your only code forge you get a single point of failure. You can do something like this:
>
> ```shell
> git remote add github https://github.com/user/repo.git
> git remote add codeberg https://codeberg.org/user/repo.git
> git remote add framagit https://framagit.org/user/repo.git
> git remote add private https://example.com/user/repo.git
>
> git add ...
> git commit ...
>
> git push github
> git push codeberg
> git push framagit
> git push private
> ```
