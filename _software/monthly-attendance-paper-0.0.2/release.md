---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: monthly-attendance-paper
software_version: 0.0.2
software_version_raw: 000000.000000.000002
release_timestamp: 2021-06-04 21:56:38
is_on_pypi: false
has_changelog: false
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---
