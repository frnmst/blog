#include <Adafruit_SSD1306.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <DHT.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>


// Sensors and outputs.
#define MQ2_DIGITAL_PIN                   14
#define MQ2_ANALOG_PIN                    A0
#define BUZZER_PIN                        0
#define BOARD_LED                         2
#define DHT_PIN                           12
#define DHT_TYPE                          DHT11
#define WIRE                              Wire

/* 0 = 0°
   1 = 90°
   2 = 180°
   3 = 270°
*/
#define DISPLAY_ROTATION 2

// MQ_2 is connected to the 5 Volts rail.
#define MQ_2_SENSOR_VOLTS                 5

#define GAS_ALARM_VOLTS_THRESHOLD         0.75

// Raise the alarm if the raw value increases after GAS_ALARM_TENDENCY_ITERATIONS.
#define GAS_ALARM_TENDENCY_ITERATIONS     64

// Rolling readings for measuring average
#define TOTAL_SENSOR_READINGS             50

#define BUZZER_SENSOR_ERROR_ITERATIONS    10
#define BUZZER_SENSOR_ERROR_DELAY_MS      1000
#define MQ_2_WARM_UP_MS                   60000

// Every i iterations invert the display color to prevent burn-in.
#define DISPLAY_INVERT_COLOR_MODULO       32

// Number of times the display's colors are inverted on a gas alarm.
#define INVERT_COLORS_ON_ALARM_TIMES      10

// Devices.
Adafruit_SSD1306 display = Adafruit_SSD1306(128, 64, &WIRE);
DHT dht(DHT_PIN, DHT_TYPE);

// Data structures.
float gas_sensor_readings[TOTAL_SENSOR_READINGS], temperature_sensor_readings[TOTAL_SENSOR_READINGS], humidity_sensor_readings[TOTAL_SENSOR_READINGS];
float average_gas_reading, average_temperature_reading, average_humidity_reading, gas_total = 0.0, temperature_total = 0.0, humidity_total = 0.0;
static int gas_index = 0, temperature_index = 0, humidity_index = 0;
int gas_tendency = 0;
bool inverted = false;
unsigned long long int iterations = 1;

void clear_display()
{
    display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(SSD1306_WHITE);
    display.setCursor(0,0);
}

// Function to read sensor value and update index and total
void update_sensor_data(float sensor_value, float* sensor_readings, int& index, float& total, int& tendency)
{
    float prev_total = total;
    total = total - sensor_readings[index];
    sensor_readings[index] = sensor_value;
    total = total + sensor_readings[index];

    // Move to the next position in the array.
    index = (index + 1) % TOTAL_SENSOR_READINGS;

    // Specific to the gas readings only.
    if (prev_total > total)
        tendency += 1;
    else if (prev_total == total)
        // Flatten if the reading does not change.
        if (tendency < 0)
            tendency = 0;
        else
            // Adjust to -10% the alarm value.
            tendency -= (int) (GAS_ALARM_TENDENCY_ITERATIONS * 0.10);
        else if (prev_total < total)
            tendency -= 1;
}

void setup()
{
    Serial.begin(9600);

    if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
        Serial.println(F("SSD1306 allocation failed"));
        for(;;);
    }
    display.display();
    delay(1000);
    clear_display();
    display.display();
    display.setRotation(DISPLAY_ROTATION);

    dht.begin();

    pinMode(MQ2_DIGITAL_PIN, INPUT);
    pinMode(BUZZER_PIN, OUTPUT);

    Serial.println("Warming up MQ2 sensor");

    for (int i = 0; i < MQ_2_WARM_UP_MS / 1000; i++)
    {
        clear_display();
        display.println("Warming");
        display.println(i);
        display.println("of");
        display.println(MQ_2_WARM_UP_MS / 1000);
        display.display();
        delay(1000);
    }

    clear_display();
    display.display();
    display.println("sampling");

    for (int i = 0; i < TOTAL_SENSOR_READINGS; i++)
    {
        clear_display();
        display.println("sample ");
        display.println(i + 1);
        display.println("of ");
        display.println(TOTAL_SENSOR_READINGS);
        display.display();

        Serial.print("sampling ");
        Serial.print(i + 1);
        Serial.print(" of ");
        Serial.println(TOTAL_SENSOR_READINGS);

        gas_sensor_readings[i] = analogRead(MQ2_ANALOG_PIN);
        temperature_sensor_readings[i] = dht.readTemperature();
        humidity_sensor_readings[i] = dht.readHumidity();
        gas_total += gas_sensor_readings[i];
        temperature_total += temperature_sensor_readings[i];
        humidity_total += humidity_sensor_readings[i];
        delay(1000);
    }

    average_gas_reading = gas_total / TOTAL_SENSOR_READINGS;
    average_temperature_reading = temperature_total / TOTAL_SENSOR_READINGS;
    average_humidity_reading = humidity_total / TOTAL_SENSOR_READINGS;
}

void invert_display_colors()
{
    inverted = !inverted;
    if (inverted)
        // Invert colors
        display.invertDisplay(true);
    else
        display.invertDisplay(false);
}

void loop()
{
    int gas_state = digitalRead(MQ2_DIGITAL_PIN);
    float gas_concentration = (float) analogRead(MQ2_ANALOG_PIN);
    float temperature_celsius = dht.readTemperature();
    float humidity_percent = dht.readHumidity();
    char average_gas_reading_normalized_char[32];
    int dummy = 0;

    update_sensor_data(gas_concentration, gas_sensor_readings, gas_index, gas_total, gas_tendency);
    average_gas_reading = gas_total / TOTAL_SENSOR_READINGS;
    update_sensor_data(temperature_celsius, temperature_sensor_readings, temperature_index, temperature_total, dummy);
    average_temperature_reading = temperature_total / TOTAL_SENSOR_READINGS;
    update_sensor_data(humidity_percent, humidity_sensor_readings, humidity_index, humidity_total, dummy);
    average_humidity_reading = humidity_total / TOTAL_SENSOR_READINGS;

    float average_gas_reading_volts = (average_gas_reading / 1023) * MQ_2_SENSOR_VOLTS;
    sprintf(average_gas_reading_normalized_char, "%.6f", average_gas_reading_volts);

    clear_display();

    if (gas_state == HIGH)
        Serial.println("The gas is NOT present");
    else
    {
        Serial.println("The gas is present");
        display.println("GAS!");
    }

    Serial.print("MQ2 sensor avg V: ");
    Serial.println(average_gas_reading_normalized_char);
    Serial.println(average_temperature_reading);
    Serial.println(average_humidity_reading);
    Serial.println(gas_tendency);

    display.print(average_gas_reading_normalized_char);
    display.println(" V");
    display.print("Gt: ");
    display.println(gas_tendency);
    display.print(average_temperature_reading);
    display.println(" oC");
    display.print(average_humidity_reading);
    display.println(" %");

    if (iterations % DISPLAY_INVERT_COLOR_MODULO == 0)
        invert_display_colors();

    display.display();

    if (gas_state == LOW
        || average_gas_reading_volts >= GAS_ALARM_VOLTS_THRESHOLD
        || gas_tendency >= GAS_ALARM_TENDENCY_ITERATIONS
    )
    {
        // Gas present.
        for (int i = 0; i < BUZZER_SENSOR_ERROR_ITERATIONS; i++)
        {
            digitalWrite(BUZZER_PIN, HIGH);
            delay(BUZZER_SENSOR_ERROR_DELAY_MS);
            digitalWrite(BUZZER_PIN, LOW);
            delay(BUZZER_SENSOR_ERROR_DELAY_MS);
            for (int j = 0; j < INVERT_COLORS_ON_ALARM_TIMES; j++)
            {
                invert_display_colors();
                delay(100);
            }
        }
    }

    delay(1000);
    display.clearDisplay();
    display.display();
    delay(10);
    iterations += 1;
}
