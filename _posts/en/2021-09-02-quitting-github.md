---
title: Quitting GitHub
tags: [quitting, github, repository]
updated: 2022-10-08 15:33:00
description: Quitting GitHub
lang: 'en'
---

<!--excerpt_start-->
Mass censorship, [privacy violations and trackings](https://tutanota.com/blog/posts/australia-surveillance-bill/)
are surging everywhere right now<!--excerpt_end-->.

Given Microsoft's acquisiton of GitHub in 2018, the temporary censorship
of [youtube-dl](https://github.blog/2020-11-16-standing-up-for-developers-youtube-dl-is-back/),
GH's [Copilot](https://copilot.github.com/), I don't think GH
is a good place to store my repositories anymore.

GitLab (gitlab.com instance) is not a good choice either because
they block TOR users:

```shell
$ torsocks --isolate git clone https://gitlab.com/pycqa/flake8
Cloning into 'flake8'...
fatal: unable to access 'https://gitlab.com/pycqa/flake8/': The requested URL returned error: 403
```

Moreover, they have the
[annoying captcha and the anti DDOS things that never work](https://forum.gitlab.com/t/cant-open-the-signin-page-it-keeps-showing-checking-your-browser-before-accessing-gitlab-com/45857)
which prevented me from logging in with some browsers so I removed my account there.

Have a look at these posts for other reasons:

- [https://thelig.ht/abandoning-github/](https://thelig.ht/abandoning-github/)
- [https://sanctum.geek.nz/why-not-github.html](https://sanctum.geek.nz/why-not-github.html)

Anyway I will keep the account for now in case I have to contribute to some
project hosted on GH.

## Current hostings

For the moment my repositories are hosted here:

- [https://software.franco.net.eu.org/frnmst](https://software.franco.net.eu.org/frnmst)
  - personal instance
  - see [this](https://software.franco.net.eu.org/frnmst/software.franco.net.eu.org/src/branch/master/privacy_policy.md#user-content-table-of-contents)
- [https://codeberg.org/frnmst](https://codeberg.org/frnmst)
  - *Codeberg is a community-driven, non-profit software development platform operated by Codeberg e.V. and centered around Codeberg.org, a Gitea-based software forge.*
- [https://framagit.org/frnmst](https://framagit.org/frnmst)
  - *Framasoft is a not-for-profit which can only keep running thanks to your donations.*
  - they provide authentication with GitHub, GitLab.com and BitBucket
