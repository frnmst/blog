---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: the-flux-of-thought
software_version: 3.0.0
software_version_raw: 000003.000000.000000
release_timestamp: 2020-12-19 21:29:56
is_on_pypi: false
has_changelog: false
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---
