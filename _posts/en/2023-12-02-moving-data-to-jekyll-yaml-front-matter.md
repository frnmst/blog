---
title: ! 'Moving data to Jekyll YAML front matter'
tags: [jekyll, yaml, blog, front-matter, open-graph-protocol]
updated: 2023-12-08 17:10:00
description: ! "Moving some data to Jekyll's YAML front matter increases flexibility"
lang: 'en'
---

In Jekyll's front matter you usually put the post title, tags and description.
Being *YAML*, you can insert any data as long as it's valid.

{% include link_preview.html url="https://jekyllrb.com/docs/front-matter/" %}

Recently I added support for several variables on this blog such as:

- `canonical_url`: a string pointing to the URL of the original content. SEO
  purposes.
- `related_links`: a list containing URLs that are related to the post content
  and that are reported in the Atom feed.
- `images`: a list of objects: each object corresponds to an image that can
  be used inside the post.

{% include link_preview.html url="https://developers.google.com/search/docs/crawling-indexing/canonicalization" %}

The logic for these three variables is defined in various layouts and include
files.

Let's see the first example which comes from an older post:

```yaml
---
title: ! 'Simple text generation'
tags: [computer-recreations, python, textgeneration, video, youtube]
related_links: ['https://www.youtube.com/watch?v=rgMATFBacYA']
updated: 2023-11-20 18:00:00
description: ! 'A followup of the "Probability-based random text generation" video: lots of improvements'
lang: 'en'
canonical_url: 'https://www.youtube.com/watch?v=rgMATFBacYA'
---
```

See the `related_links` and `canonical_url` variables?

The canonical URL is defined in the `_includes/head.html` file. All you have to
do is to use the correct HTML tag. If a page or post misses it, then we just
use a self-reference to the page.

```jinja
# [ ... ]
{% raw %}
{%- if page.canonical_url -%}
    <link rel="canonical" href="{{- page.canonical_url -}}">
{%- else -%}
    <link rel="canonical" href="{{- page_url | prepend: site.rooturl -}}">
{%- endif -%}
{% endraw %}
# [ ... ]
```

Concerning the related links, I edited the feed.xml file like this:

```jinja
# [ ... ]

<entry>

# [ ... ]
{% raw %}
{%- for link in post.related_links -%}
    <link rel="related" href="{{ link }}"/>
{%- endfor -%}
{% endraw %}
# [ ... ]

</entry>

# [ ... ]
```

As you can see, you just need to put those 3 lines inside a feed entry.
[This element](https://validator.w3.org/feed/docs/atom.html#link) is an
[optional one](https://validator.w3.org/feed/docs/atom.html#recommendedEntryElements)
specified in the Atom feed specification.

{% include link_preview.html url="https://validator.w3.org/feed/docs/atom.html" %}

Finally we get to the images. Let's see the front matter of this other post:

```yaml
---
title: Simplest bread making
tags: [food, bread, recipe]
updated: 2023-02-15 17:08:00
description: ! 'A very simple recipe to make bread consisting in four ingredients'
lang: 'en'
images:
  - id: '0'
    filename: '0.jpg'
    alt: 'ingredients before mixing'
    caption: ! 'Ingredients before mixing'
    width: '300px'
  - id: '1'
    filename: '1.jpg'
    alt: 'spread dough'
    caption: ! 'Spread dough'
    width: '300px'
  - id: '2'
    filename: '2.jpg'
    alt: 'dough resting'
    caption: ! 'Dough resting'
    width: '300px'
  - id: '3'
    filename: '3.jpg'
    alt: 'final result'
    caption: ! 'Final result'
    width: '300px'
open_graph_protocol_default_image_id: '3'
---
```

As I said before, `images` is a list of objects, where each object describes
an image. To display the image with id `'2'` within the post you just have
to do:

```jinja
{%- raw -%}
{%- include image.html id="2" -%}
{% endraw -%}
```

The `image.html` include file contains all the logic. First of all you have
to filter by image id to get the single object:

```jinja
{%- raw -%}
{%- assign img_data = page.images | where:"id",include.id -%}
{%- assign img_data = img_data[0] -%}
{% endraw -%}
```
An `img` HTML tag is now trivial[^1]:

```html
{%- raw -%}
<img src="{{ img_data.filename }}" width="{{ img_data.width }}" alt="{{ img_data.alt }}">
{% endraw %}
```

The reason to move the image data to YAML front matter is flexibility: if I
left everything as-is, it would have been like this:

```jinja
{%- raw -%}
{% include image.html file="2.jpg" alt="dough resting" caption="Dough resting" width="300px" %}
{% endraw %}
```

The problem with the old way is that I couldn't use this kind of data to
generate the Open Graph Protocol representation of the blog posts.

{% include link_preview.html url="https://ogp.me/" %}

This protocol is used by Facebook and other websites to retrieve web
page data in a standardized way. Different elements of the webpage are *tagged*
and summarized in the `head` section of the HTML:

```html
<meta content="Franco Masotti's software developer blog" property="og:site_name">
<meta content="Simplest bread making" property="og:title">
<meta content="article" property="og:type">
<meta content="A very simple recipe to make bread consisting in four ingredients" property="og:description">
<meta content="http://blog.franco.net.eu.org/post/simplest-bread-making.html" property="og:url">
<meta content="2023-02-02T00:00:00+01:00" property="article:published_time">
<meta content="http://blog.franco.net.eu.org/about/" property="article:author">
<meta content="https://testing.franco.net.eu.org/media/2023-02-02-simplest-bread-making/3.jpg" property="og:image">
<meta content="food" property="article:tag">
<meta content="bread" property="article:tag">
<meta content="recipe" property="article:tag">
```

To generate this I created a new include file called `open_graph_protocol.html`.
I modified [this](https://gist.github.com/davidensinger/5431869) gist. See
the images section in particular:

```jinja
{%- raw -%}
<meta content="{{ site.title }}" property="og:site_name">

{%- if page.title -%}
<meta content="{{ page.title | escape }}" property="og:title">
{%- else -%}
<meta content="{{ site.title | escape}}" property="og:title">
{%- endif -%}

{%- if page.title -%}
<meta content="article" property="og:type">
{%- else -%}
<meta content="website" property="og:type">
{%- endif -%}

{%- if page.description -%}
<meta content="{{ page.description | escape }}" property="og:description">
{%- else -%}
<meta content="{{ site.description | escape }}" property="og:description">
{%- endif -%}

{%- if page.url -%}
<meta content="{{ page.url | absolute_url }}" property="og:url">
{%- endif -%}

{%- if page.date -%}
<meta content="{{ page.date | date_to_xmlschema }}" property="article:published_time">
<meta content="{{ site.url }}/about/" property="article:author">
{% endif %}

{%- if page.images -%}
{%- if page.open_graph_protocol_default_image_id -%}
    {%- assign img_data = page.images | where:"id",page.open_graph_protocol_default_image_id -%}
    {%- assign img_data = img_data[0] -%}
{%- else -%}
    {%- comment -%}Get the first picture of a post if not specified otherwise.{%- endcomment -%}
    {%- assign img_data = page.images[0] -%}
{%- endif -%}
{%- include image_url.html img_data=img_data -%}
<meta content="{{ url }}" property="og:image">
{%- else -%}
<meta content="/favicon.ico" property="og:image">
{%- endif -%}

{%- if page.tags -%}
    {%- for tag in page.tags -%}
<meta content="{{ tag | escape }}" property="article:tag">
    {%- endfor -%}
{%- endif -%}
{% endraw -%}
```

That's it 🚀

## Notes

[^1]: Ok, the truth is that it's a little more complicated because we need to compute the correct
      path and default values, but you get the idea :)
