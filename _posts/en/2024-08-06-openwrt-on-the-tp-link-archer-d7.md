---
title: ! 'OpenWrt on the TP-Link Archer D7'
tags: [openwrt, modem, router, raspberry-pi, serial, uart, jumper-wires]
updated: 2024-08-06 14:00:00
description: ! "Let's see how to flash OpenWrt to the TP-Link Archer D7"
lang: 'en'
images:
  - id: 'console'
    filename: 'console.gif'
    alt: ! 'OpenWrt console'
    caption: ! "OpenWrt has already been flashed here but you can see the bootloader"
    width: '300px'
  - id: 'd7_with_jumper_wires'
    filename: 'd7_with_jumper_wires.png'
    alt: ! 'TP-Link Archer D7 with jumper wires'
    caption: ! "Connections ready"
    width: '300px'
  - id: 'd7_openwrt_page'
    filename: 'IMG-20240725-WA0003.jpeg'
    alt: ! 'View of the LuCi interface on the TP-Link Archer D7 running OpenWrt'
    caption: ! "LuCi works out of the box"
    width: '300px'
  - id: 'j1_pins'
    filename: 'j1_pins.png'
    alt: ! 'Detailed view of the J1 pins'
    caption: ! "Pinout orientation like in this photo: `| [**TXD**] | RXD | GNC | VCC |`"
    width: '300px'
  - id: 'jumper_wires_connected_to_rpi'
    filename: 'jumper_wires_connected_to_rpi.png'
    alt: ! 'Detailed view of the jumper wires connected to the Raspberry PI'
    caption: ! "The 3 pins on the RPI are consecutive"
    width: '300px'
  - id: 'rpi_and_d7'
    filename: 'rpi_and_d7.png'
    alt: ! 'Global view of the TP-Link Archer D7 and the Raspberry PI 1 model B connected with jumper wires'
    caption: ! "Simple, right?"
    width: '300px'
---
## Introduction

Some routers are harder to flash with custom ROMs than others. The TP-Link
Archer D7 is one of them. It's an ADSL WiFI modem/router combo so it's not the
best hardware you can put OpenWrt on: infact you'll lose the modem part since
it's driver is proprietary.

{% include image.html id="d7_with_jumper_wires" %}

The [official wiki page explains](https://openwrt.org/toh/tp-link/archer_d7)
multiple procedures to flash OpenWrt on this router.

The easy method (software only) didn't work for me so I had to open up the case
and connect via serial to it, something which I never did.

{% include link_preview.html url="https://www.youtube.com/watch?v=RtqVKNfuxWM" %}

This guy did it with another TP-Link router by using a USB to serial adapter. I
remember reading that Raspberry PIs have a serial (UART) interface, and I had a
model B (version 1) lying around: I thought I'd use that instead.

## Hardware

- TP-Link Archer D7
- Raspberry PI (even version 1 is OK)
- 3 male-female jumper cables

## Opening the case

First of all you need to remove the three screws on the back: two are covered
by the info label and one is hidden below the plastic silver-colored element
(the one in the middle) which snaps off. I broke a piece of it while trying it.

After that you need to pry open the plastic grafts. These ones will not break
if you use a plastic pry tool such as the ones for mobile phones.

Now the PCB is accessible.

## Connections

As described in the wiki, there are two serial headers. J1 and J41: connect the
male jumper cable parts to the J1 headers and the female parts to the
Raspberry. This is the diagram for the J1 pins on the router. The TXD pin is
marked differently from the others on the PCB:

```
| VCC | GND | RXD | [**TXD**] |
```

| RPI pin name | RPI Pid ID | RPI Pin functionality | router J1 pin functionality |
|----------|--------|-------------------|------------------|
|          | 6      | GND               | GND              |
| GPIO 14  | 8      | TXD               | RXD              |
| GPIO 15  | 10     | RXD               | TXD              |

**Important: never connect anything to the VCC pin on the router!**

{% include image.html id="j1_pins" %}

This page explains which GPIO is which.

{% include link_preview.html url="https://pinout.xyz/pinout/uart" %}

{% include image.html id="jumper_wires_connected_to_rpi" %}

If you are unsure of the pins and voltages you can grab a tester and follow
[this other tutorial](https://openwrt.org/docs/techref/hardware/port.serial) on
the OpenWrt wiki. Fortunaltely both the Rasbperry and the router pins all work
on 3.3V, so there is no need for more hardware in between.

{% include image.html id="rpi_and_d7" %}

## Software on the Raspberry PI

Grab the latest version of Raspbian and flash it on an SD card. Proceed to
the usual setup. I suggest installing and enabling OpenSSH for convenience.

### Serial port basic setup

Run `raspi-config` as root and enable the serial port.

Before installing anything else you need to remove the `console=serial0` option
from `/boot/cmdline.txt`: this way the Raspberry PI will not be accessible via
the serial console because we need to use it to flash our router. If you don't
disable this the login serial console will interfere with the router (or
anything else you want to use with the serial port in the future).

Finally reboot to apply these two changes.

### Serial port software

[Minicom](https://en.wikipedia.org/wiki/Minicom) is a simple software you can
use to interact with the serial console. You can install it with

```shell
apt install minicom
```

Use `minicom -s` to set up the program. In this case the options you need to
use are:

- serial port: `/dev/serial0`
- 115200 baud
- 8 bit
- no parity
- 1 stop bit
- no flow control

{% include link_preview.html url="https://wiki.emacinc.com/wiki/Getting_Started_With_Minicom" %}

When you now turn on the router you should see its boot text on the screen,
and you can also interact with the console. Follow the instructions on the
OpenWrt wiki page for the default credentials.

{% include image.html id="console" %}

### TFTP server

[U-Boot](https://en.wikipedia.org/wiki/Das_U-Boot), the bootloader on the
TP-Link D7, needs a TFTP server to be able to run or flash a different image.

Install the `tftp-hpa` package, and enable and run the server on the Rapsberry

```shell
apt install tftp-hpa
systemctl start tftp-hpa && systemctl enable tftp-hpa
```

Go into its root directory at `/srv/tftp`. You have to place the initramfs and
sysupgrade images in this directory. I had problems with the latest release
(23) and the master branch, so I used version 22 of OpenWrt which you can
find
[here](https://mirror-03.infra.openwrt.org/releases/22.03.7/targets/ath79/generic/)
(CTRL+F `archer-d7-v1`).

I strongly suggest to test the initramfs image first since it's loaded in RAM
and will not affect the flash memory. When you are satisifed, proceed to flash
the sysupgrade image. All the instructions for this are in the OpenWrt wiki.

{% include image.html id="d7_openwrt_page" %}

{% include link_preview.html url="https://github.com/openwrt/openwrt/pull/2079" %}

{% include link_preview.html url="https://forum.openwrt.org/t/archer-d7-support/15790/200?page=8" %}
