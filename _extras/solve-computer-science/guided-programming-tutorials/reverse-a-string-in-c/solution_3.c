// SPDX-FileCopyrightText: 2023 Franco Masotti
//
// SPDX-License-Identifier: MIT
#include <stdio.h>

int main(void)
{
    char array[100] = "ab c def g hi";
    char reversed[100];
    int i = 0, j = 0, done = 0, go_back = 0;

    while (!done)
    {
        if (array[i] == '\0')
        {
            go_back = 1;

            // Point to the character before the '\0'.
            i--;
        }

        if (go_back)
        {
            // Same as other solutions.
            reversed[j] = array[i];
            j++;
            i--;
        }
        else
            // Iterate the string normally.
            // We need to get to the terminating character ('\0').
            i++;

        // Loop stop condition.
        if (go_back && i == -1)
            // Terminating character was reached
            // and we've copied array[0]:
            // there are no more characters to copy.
            done = 1;
    }

    reversed[j] = '\0';

    printf("%s\n", reversed);

    return 0;
}
