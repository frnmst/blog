---
title: ! 'Self hosted CPU A.I. part 2 - setup'
tags: [ai, chatbot, openwebui, ollama, ssh, ssh-tunnel, youtube, video]
related_links: ['https://www.youtube.com/watch?v=jUdkKAdT50I']
updated: 2024-08-20 16:55:35
description: ! "In this video we'll see how to setup a self-hosted CPU A.I. chatbot system."
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/jUdkKAdT50I?si=CQslMDzqylK8NsRA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

In the second part of the Open WebUI video, I provided detailed instructions for installing and setting up Open WebUI and Ollama. I began by explaining the importance of configuring SSH port forwarding using autossh, which automatically re-establishes connections if they break. This setup allows me to connect to Ollama from the main server seamlessly, even though it resides on a different server. I also outlined the straightforward process of setting up Ollama as a Docker container and the more involved setup for Open WebUI, which requires building a Docker image due to some missing front-end components.

I then walked through the steps to install autossh on the main server, emphasizing the need to generate an SSH key pair without a passphrase for compatibility with Systemd scripts. I detailed how to configure the SSH daemon on the secondary server to allow TCP forwarding and how to create a Systemd script to automate the port forwarding process. This setup ensures that the remote port for Ollama is accessible from the main server, which is crucial for Docker's functionality.

Next, I explained the configuration of Ollama using a Docker Compose file, highlighting the importance of setting the correct volume path for model storage. I demonstrated how to run the Docker container and check its logs to confirm successful operation. I also discussed the setup of Open WebUI, including cloning the repository and modifying the Docker Compose file to exclude unnecessary components since Ollama is hosted on a different server.

Finally, I covered the process of building the Docker image for Open WebUI, including necessary modifications to the Dockerfile. I mentioned the creation of a Systemd service file for managing the Docker container and the need to adjust reverse proxy settings for proper port exposure. Check out the previous video for additional context.
