---
layout: software_release
enable_markdown: true
title: release
excerpt: none
tags: [md-toc, markdown, table-of-contents, python]
software_name: md-toc
software_name_python_module: md_toc
software_version: 9.0.0
software_version_raw: 000009.000000.000000
release_timestamp: 2024-04-10 22:03:05
is_on_pypi: true
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---
See the 9.x-mypy branch for the mypy pre-commit hook:

- [canonical repository](https://software.franco.net.eu.org/frnmst/md-toc/src/branch/9.x-mypy)
- [Codeberg](https://codeberg.org/frnmst/md-toc/src/branch/9.x-mypy)
- [Framagit](https://framagit.org/frnmst/md-toc/-/tree/9.x-mypy?ref_type=heads)
- [GitHub](https://github.com/frnmst/md-toc/tree/9.x-mypy)

### Fixed

- [__✓__ f3398b6](https://software.franco.net.eu.org/frnmst/md-toc/commit/f3398b6)

  `setup.cfg` now reads `options.install_requires` from the `./requirements.txt`
  file instead from a hard-coded list.
- [__✓__ f3398b6](https://software.franco.net.eu.org/frnmst/md-toc/commit/f3398b6)

  Add `--require-virtualenv` pip option in Makefile where necessary.
- [__✓__ f154114](https://software.franco.net.eu.org/frnmst/md-toc/commit/f154114)
  [__✓__ 31e703b](https://software.franco.net.eu.org/frnmst/md-toc/commit/31e703b)
  [__✓__ f357165](https://software.franco.net.eu.org/frnmst/md-toc/commit/f357165)

  Add use of SHA512 and SHA256 hashes for the `./.requirements-freeze-hashes.txt`
  file. The Makefile now downloads packages locally, computes their checksums,
  and updates the `./requirements-freeze.txt` and `./requirements-freeze-hashes.txt`  files.
- [__✓__ f154114](https://software.franco.net.eu.org/frnmst/md-toc/commit/f154114)

  Fix `.github/FUNDING.yml`
- [__✓__ 42d3086](https://software.franco.net.eu.org/frnmst/md-toc/commit/42d3086)

  Read tox test requirements (`testenv` in setup.cfg) from requirement files
  instead of hard-coded packages.

- [__✓__ #42: *IndexError: list index out of range*](https://github.com/frnmst/md-toc/issues/42)

  This issue is related to GitHub only: it seems they fixed their backend code.
  Will close once md-toc 9.x is out.
- [__✓__ c8a6402](https://software.franco.net.eu.org/frnmst/md-toc/commit/c8a6402)

  Improve documentation for the API so it is more readable: use `autosummary`
  and `automodule`.
- [__✓__ 40f9231](https://software.franco.net.eu.org/frnmst/md-toc/commit/40f9231)

  Remove NULL bytes before passing the anchor link to the `remove_emphasis`
  cmark function. Without this change, md-toc fails with this and other similar
  inputs. More tests are needed to determine if the generated TOCs still work.

  This is the output of `hexyl a.md`:

  ```
  ┌────────┬─────────────────────────┬─────────────────────────┬────────┬────────┐
  │00000000│ 23 20 61 62 63 64 65 66 ┊ 67 68 69 6a 6b 6c 6d 6e │# abcdef┊ghijklmn│
  │00000010│ 6f 70 71 72 73 74 75 76 ┊ 77 78 79 7a 41 42 43 44 │opqrstuv┊wxyzABCD│
  │00000020│ 45 46 47 48 49 4a 4b 4c ┊ 4d 4e 4f 50 51 52 53 54 │EFGHIJKL┊MNOPQRST│
  │00000030│ 55 56 57 58 59 5a 60 00 ┊ 00 00 0a                │UVWXYZ`0┊00_     │
  └────────┴─────────────────────────┴─────────────────────────┴────────┴────────┘
  ```

  ```shell
  cat -e a.md
  # abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`^@^@^@$
  ```

  Result:

  ```shell
  python3 -m md_toc github a.md

    Traceback (most recent call last):
      File "./md-toc/md_toc/__main__.py", line 36, in main
        result = args.func(args)
                 ^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cli.py", line 79, in write_toc
        toc_struct = build_multiple_tocs(
                     ^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/api.py", line 483, in build_multiple_tocs
        return [
               ^
      File "./md-toc/md_toc/api.py", line 484, in <listcomp>
        build_toc(
      File "./md-toc/md_toc/api.py", line 325, in build_toc
        headers: list[types.Header] = get_md_header(
                                      ^^^^^^^^^^^^^^
      File "./md-toc/md_toc/api.py", line 1320, in get_md_header
        return [
               ^
      File "./md-toc/md_toc/api.py", line 1328, in <listcomp>
        build_anchor_link(
      File "./md-toc/md_toc/api.py", line 906, in build_anchor_link
        header_text_trimmed = remove_emphasis(header_text_trimmed, parser)
                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/api.py", line 836, in remove_emphasis
        ignore: list[range] = inlines_c._cmark_cmark_parse_inlines(
                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 2184, in _cmark_cmark_parse_inlines
        while not _cmark_is_eof(subj) and _cmark_parse_inline(
                                          ^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 2117, in _cmark_parse_inline
        new_inl = _cmark_handle_backticks(subj, options)
                  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 639, in _cmark_handle_backticks
        openticks: _cmarkCmarkChunk = _cmark_take_while(subj, '_cmark_isbacktick')
                                      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 497, in _cmark_take_while
        while _cmark_take_while_loop_condition(subj, '_cmark_isbacktick'):
              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 482, in _cmark_take_while_loop_condition
        c = _cmark_peek_char(subj)
            ^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 430, in _cmark_peek_char
        raise ValueError
    ValueError
  ```

- [__✓__ 40f9231](https://software.franco.net.eu.org/frnmst/md-toc/commit/40f9231)

  Add more checks in the `md_toc.cmark.inlines_c._cmark_subject_find_special_char`
  function to avoid overflows. More tests are needed to determine if the
  generated TOCs still work.

  This is the output of `hexyl a.md`:

  ```
  ┌────────┬─────────────────────────┬─────────────────────────┬────────┬────────┐
  │00000000│ 23 20 61 62 63 64 65 66 ┊ 67 68 69 6a 6b 6c 6d 6e │# abcdef┊ghijklmn│
  │00000010│ 6f 70 71 72 73 74 75 76 ┊ 77 78 79 7a 41 42 43 44 │opqrstuv┊wxyzABCD│
  │00000020│ 45 46 47 48 49 4a 4b 4c ┊ 4d 4e 4f 50 51 52 53 54 │EFGHIJKL┊MNOPQRST│
  │00000030│ 55 56 57 58 59 5a c5 8c ┊ 61 62 63 64 65 66 67 68 │UVWXYZ××┊abcdefgh│
  │00000040│ 69 6a 6b 6c 6d 6e 6f 70 ┊ 71 72 73 74 75 76 77 78 │ijklmnop┊qrstuvwx│
  │00000050│ 79 7a 41 42 43 44 45 46 ┊ 47 48 49 4a 4b 4c 4d 4e │yzABCDEF┊GHIJKLMN│
  │00000060│ 4f 50 51 52 53 54 55 56 ┊ 57 58 59 5a 0a          │OPQRSTUV┊WXYZ_   │
  └────────┴─────────────────────────┴─────────────────────────┴────────┴────────┘
  ```

  ```shell
  cat -e a.md
  # abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZM-EM-^LabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$
  ```

  Result:

  ```shell
  python3 -m md_toc github a.md

     Traceback (most recent call last):
       File "./md-toc/md_toc/__main__.py", line 36, in main
        result = args.func(args)
                 ^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cli.py", line 79, in write_toc
        toc_struct = build_multiple_tocs(
                     ^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/api.py", line 483, in build_multiple_tocs
        return [
               ^
      File "./md-toc/md_toc/api.py", line 484, in <listcomp>
        build_toc(
      File "./md-toc/md_toc/api.py", line 325, in build_toc
        headers: list[types.Header] = get_md_header(
                                      ^^^^^^^^^^^^^^
      File "./md-toc/md_toc/api.py", line 1324, in get_md_header
        return [
               ^
      File "./md-toc/md_toc/api.py", line 1332, in <listcomp>
        build_anchor_link(
      File "./md-toc/md_toc/api.py", line 910, in build_anchor_link
        header_text_trimmed = remove_emphasis(header_text_trimmed, parser)
                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/api.py", line 840, in remove_emphasis
        ignore: list[range] = inlines_c._cmark_cmark_parse_inlines(
                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 2187, in _cmark_cmark_parse_inlines
        while not _cmark_is_eof(subj) and _cmark_parse_inline(
                                          ^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 2152, in _cmark_parse_inline
        endpos = _cmark_subject_find_special_char(subj, options)
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 2090, in _cmark_subject_find_special_char
        if SPECIAL_CHARS[ord(subj.input.data[n])] == 1:
           ~~~~~~~~~~~~~^^^^^^^^^^^^^^^^^^^^^^^^^
    IndexError: list index out of range
    ```

- [__✓__ 40f9231](https://software.franco.net.eu.org/frnmst/md-toc/commit/40f9231)

  Improve benchmark script.

- [__✓__ 1a40e9b](https://software.franco.net.eu.org/frnmst/md-toc/commit/1a40e9b)

  Use Python [dataclass](https://docs.python.org/3.11/library/dataclasses.html)
  in cmark code where appropriate.

- [__✓__ 1a40e9b](https://software.franco.net.eu.org/frnmst/md-toc/commit/1a40e9b)

  Remove debug code in cmark code classes.

- [__✓__ 0904177](https://software.franco.net.eu.org/frnmst/md-toc/commit/0904177)

  Fix the anchor link punctuation filter regex for cmark-like markdown parsers.
  This reflects the existing
  [Ruby code](https://github.com/gjtorikian/html-pipeline/blob/7c7fad1f82f81ebf15dd81d59eed28d979b8e441/lib/html/pipeline/toc_filter.rb#L30).
  See also
  [#42: *IndexError: list index out of range*](https://github.com/frnmst/md-toc/issues/42).

- [__✓__ 9659e9d](https://software.franco.net.eu.org/frnmst/md-toc/commit/9659e9d)

  Simplify fuzzer script

- [__✓__ 970b5a9](https://software.franco.net.eu.org/frnmst/md-toc/commit/970b5a9)

  Fix benchmark script: improve random file generation speed

### Added

- [__✓__ f3398b6](https://software.franco.net.eu.org/frnmst/md-toc/commit/f3398b6)

  Add Flatpak manifest file.
- [__✓__ 31e703b](https://software.franco.net.eu.org/frnmst/md-toc/commit/31e703b)

  Add pip-audit pre-commit hook.
- [__✓__ 31e703b](https://software.franco.net.eu.org/frnmst/md-toc/commit/31e703b)

  Move Makefile to a separate project.
- [__✓__ 8b2f1da](https://software.franco.net.eu.org/frnmst/md-toc/commit/8b2f1da)

  Start adding examples in docstrings.
- [__✓__ 40f9231](https://software.franco.net.eu.org/frnmst/md-toc/commit/40f9231)

  Add use of fuzzer to detect bugs.

- [__✓__ 0904177](https://software.franco.net.eu.org/frnmst/md-toc/commit/0904177)

  Add the `md_toc.api.anchor_link_punctuation_filter` function and related
  unit tests. See also
  [#42: *IndexError: list index out of range*](https://github.com/frnmst/md-toc/issues/42).

### Changed

- [__✓__ c8a6402](https://software.franco.net.eu.org/frnmst/md-toc/commit/c8a6402)

  Change the module imports. To import functions or exceptions from the API
  you now need the full path. For example `md_toc.build_toc` becomes
  `md_toc.api.build_toc`. To do this:
  - the `md_toc/__init__` file needs to have the bare minimum data
  - the API section of the developer interface documentation points to elements
    via their full name
  The purpose of this is to have a cleaner project structure.
- [__✓__ ab64da0](https://software.franco.net.eu.org/frnmst/md-toc/commit/ab64da0)
  [__✓__ ba25606](https://software.franco.net.eu.org/frnmst/md-toc/commit/ba25606)
  [__✓__ 9b92f17](https://software.franco.net.eu.org/frnmst/md-toc/commit/9b92f17)

  Snake case for all variables.
- [__✓__ ab64da0](https://software.franco.net.eu.org/frnmst/md-toc/commit/ab64da0)
  [__✓__ 3dcbc6d](https://software.franco.net.eu.org/frnmst/md-toc/commit/3dcbc6d)
  [__✓__ 456343f](https://software.franco.net.eu.org/frnmst/md-toc/commit/456343f)

  Fix and cleanup `md_toc/constants.py`.
- [__✓__ 9b92f17](https://software.franco.net.eu.org/frnmst/md-toc/commit/9b92f17)

  Fix and cleanup `md_toc/types.py`:
  - `class IndentationLogElement`
    - `list marker` -> `list_marker`
    - `indentation_space` -> `indentation_spaces`
  - `class Header`
    - `type` -> `header_type`
  - `class HeaderTypeCounter`
    - `1` ... `6` -> `h1` ... `h6`
  - `class AtxHeadingStructElement`
    - `header type` -> `header_type`
    - `header text trimmed` -> `header_text_trimmed`
- [__✓__ ba25606](https://software.franco.net.eu.org/frnmst/md-toc/commit/ba25606)

  Change default maximum header level to the maximum supported for each parser.
- [__✓__ 40f9231](https://software.franco.net.eu.org/frnmst/md-toc/commit/40f9231)

  Catch `UnicodeDecodeError` exceptions in the
  `md_toc.api.build_toc` function: stop reading the file and print an HTML
  comment.

### Removed

- [__✓__ 88f66bc](https://software.franco.net.eu.org/frnmst/md-toc/commit/88f66bc)

  Remove the deprecated `md_toc.exceptions.CannotTreatUnicodeString` exception.
- [__✓__ 7bbbccc](https://software.franco.net.eu.org/frnmst/md-toc/commit/7bbbccc)

  Remove the `md_toc.api.toc_renders_as_coherent_list` and
  `md_toc.api.init_indentation_status_list` functions and replace them with
  a much simpler check.

Note: GitHub Flavored Markdown is still at version 0.29. md-toc does its best
to support both GFM 0.29 and cmark 0.30.
