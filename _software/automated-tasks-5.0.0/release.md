---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: automated-tasks
software_name_python_module: automated_tasks
software_version: 5.0.0
software_version_raw: 000005.000000.000000
release_timestamp: 2020-06-30 16:06:24
is_on_pypi: false
has_changelog: false
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---
