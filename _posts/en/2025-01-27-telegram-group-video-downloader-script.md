---
title: ! 'Telegram group video downloader script'
tags: [telegram, python, script, video-downloader, tutorial, youtube, video]
related_links: ['https://www.youtube.com/watch?v=Asqn3Ggs0pI']
updated: 2025-01-27 21:50:00
description: ! "Using Telegram's API and some Python it's possible to download videos from a group in bulk"
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/Asqn3Ggs0pI?si=OtY1Pxq-I1oSbQQU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<!--TOC-->

- [Summary](#summary)

<!--TOC-->

## Summary

<!--excerpt_start-->In this video, I introduce a script designed to download videos from a public Telegram channel<!--excerpt_end--> using the official Telegram API. No scraping is involved, which makes the process straightforward and user-friendly, even for those without programming experience. The script allows for various configurations, such as setting the Telegram group name, filtering videos by duration, and downloading in chunks based on message ID intervals. This flexibility ensures that users can tailor the download process to their specific needs.

I explain the initial setup, which requires entering a phone number and confirmation code to generate a session file. Once this file is created, subsequent runs of the script do not require re-entering this information. The script is quite verbose. This provides the users with detailed feedback during execution, although this can be adjusted in the configuration settings. The script processes messages from newest to oldest, ignoring those without videos, and utilizes asynchronous operations to enhance efficiency.

During the demonstration, I run the script in a Debian virtual machine and share my preference for using an SSH terminal over VNC due to convenience. The script includes a built-in delay before exiting to ensure all videos are downloaded completely. Additionally, I clarify the distinction between message IDs and video IDs, which is crucial for managing downloads effectively. After verifying the audio presence in the downloaded videos, I confirm that the script functions similarly on Windows 10.

Finally, I offer the script for sale, providing two versions: one with source code and one without, catering to different user needs. Users can reach out for modifications or extensions if required via Fiverr or directly from this website.
