---
layout: page
title: ! 'Python courses'
permalink: /courses/computer-science/python/
description: ! 'Python courses'
lang: 'en'
---
## Mastering Python packages: organizing files and best practices

This is a course under development made by me.

### Course intro (#00)

<iframe width="560" height="315" src="https://www.youtube.com/embed/RGeNV9EwLzM?si=EJvVl4kD0BJek-5t" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Git tips (#01)

<iframe width="560" height="315" src="https://www.youtube.com/embed/JuNVgDO6trE?si=Z4IGxuC_ONJZVzS2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Virtual environments (#02)

<iframe width="560" height="315" src="https://www.youtube.com/embed/tLUx1g2Edk4?si=r5ezC1rlVoUIn46l" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Requirements files (#03)

<iframe width="560" height="315" src="https://www.youtube.com/embed/_kLe93M6zXA?si=rnh-OlO-fx_09oja" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

In this lesson, we'll see the importance of requirement files in Python, particularly in relation to virtual environments. I explained how these text files list necessary dependencies for a project, allowing for easier management of packages. You can specify exact versions or version ranges for each dependency, which is crucial for maintaining compatibility and stability in projects.

We'll see how to create a requirements file, typically named `requirements.txt`, and how to install dependencies from it using pip. I demonstrate the process of activating a virtual environment, installing packages, and the benefits of using a requirements file to avoid manually installing each dependency. This method is especially useful when dealing with multiple dependencies, as it simplifies the installation process.

The lesson further delved into the concept of frozen requirements, where I can capture the exact versions of installed packages using pip freeze. This ensures that the environment can be replicated accurately on other systems. I emphasize the importance of using hashes for security, as they help verify that the packages being installed have not been tampered with.

Finally, we'll see the different types of requirements files, including those for development and runtime dependencies. I highlight the need to regenerate the freeze file for different systems, as dependencies can vary based on the operating system. Overall, this lesson should provide you with valuable insights into managing Python dependencies effectively.
