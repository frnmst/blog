// SPDX-FileCopyrightText: 2023 Franco Masotti
//
// SPDX-License-Identifier: MIT
#include <stdio.h>
#include <string.h>

int main(void)
{
    char array[100] = "ab c def g hi";
    char reversed[100];

    int i, j = 0;
    int length = 0;

    // Does not include the '\0' character.
    length = strlen(array);

    for (i = length - 1; i >= 0; i--, j++)
        reversed[j] = array[i];

    reversed[length] = '\0';

    printf("reversed = %s\n", reversed);

    return 0;
}
