---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpydocs
software_name_python_module: fpydocs
software_version: 2.0.0
software_version_raw: 000002.000000.000000
release_timestamp: 2021-01-20 15:43:04
is_on_pypi: false
has_changelog: false
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---
