---
title: ! 'OpenAI and LocalAI (Ollama, really) integration in Nextcloud (A.I. Assistant)'
tags: [ai, chatbot, nextcloud, ollama, tutorial, youtube, video]
related_links: ['https://www.youtube.com/watch?v=6_BPOZzvzZQ']
updated: 2025-02-28 15:53:00
description: ! "Tutorial on how to set up the Nextcloud OpenAI integration with Ollama"
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/6_BPOZzvzZQ?si=SOz7ctyDHrGy7xFB" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<!--TOC-->

- [Important notice](#important-notice)
- [Summary](#summary)

<!--TOC-->

## Important notice

This setup was tested on Nextcloud 28 and 29. Nextcloud 30 introduces some
breaking changes. See the pinned comment on YouTube.

## Summary

<!--excerpt_start-->In this video, I dive into the fascinating world of large language models (LLMs), again<!--excerpt_end-->, and their integration with Nextcloud. My previous videos on Ollama and OpenWebUI provide valuable tips for setting up these tools. Currently, I'm using Nextcloud 28, the oldest major version still supported, and I explain how to set up the Ollama instance as the AI backend for Nextcloud.

After ensuring that you have a working Ollama instance, you'll see the installation of the Nextcloud Assistant app from the store. You have to navigate to the admin settings to the artificial intelligence section were you can setup the IP address. It's important to use the local IP (not localhost) of the Ollama instance since I'm running Nextcloud inside Docker. I also mention that if Ollama is on a different server, SSH port forwarding may be necessary. This setup allows for seamless communication between the two services.

As I configure the assistant, I highlight the limitations I encountered, particularly the lack of a token stream, which I find disappointing compared to other applications. I suspect this is due to the differences between Nextcloud and Ollama, as Nextcloud's A.I. app was designed to work with LocalAI instead. I also discuss the available tasks and how they are primarily text-related due to Ollama's constraints. During testing, I experience some failures, likely due to the heavy model I selected, prompting me to consider using a lighter model for better performance.

In conclusion, I touch on the AI Smart Picker feature available in the notes app, which allows for more direct text generation. I also mention an older app that serves a similar purpose but do not recommend it. In the next video I will demonstrate how to safely expose Ollama via an HTTPS endpoint using a clever trick. Stay tuned.
