---
title: ! 'Multi-language support in Jekyll'
tags: [i18n, translation, jekyll, html, tutorial]
updated: 2023-12-30 17:25:00
description: ! "Create a system to handle blog posts and pages written in multiple languages on Jekyll"
lang: 'en'
images:
  - id: 'liquid_nvim'
    filename: 'liquid_syntax_highlighting_nvim.png'
    alt: 'Liquid syntax highlighting in Neovim'
    caption: ! 'Free (as in price and freedom) liquid syntax highlighting in Neovim'
    width: '300px'
---

{% include image.html id="liquid_nvim" %}

<!--excerpt_start-->
I recently added multi-language support on this blog. There are several Jekyll
plugins to do this<!--excerpt_end-->, but I preferred to do things manually
because I already have too many custom elements. Various steps are involved.
Let's look at the ones for manage blog posts.

First of all decide for a default language: use English (`en`) for example.

Start by organizing posts by language.

1. the default or existing language must correspond. Posts in this language
   should retain the existing permaliks for SEO purposes. If you use the schema
   `/post/${post_name}` like me, keep it as-is
2. new languages can follow the schema `/${lang}/post/${post_name}`. This
   is achieved by adding the `permalink` variable to the YAML front matter
3. move all existing posts to a new sub-directory. If our default
   language is `en` we need to move `_posts/*.md` to `_posts/en/*.md`
4. add a `lang: 'en'` variable to the YAML front matter of each English
   post. Posts in other language must use their
   [language code](https://en.wikipedia.org/wiki/IETF_language_tag#List_of_common_primary_language_subtags).
   This will make things easier when we need to filter posts by language

Concerning point 2 what I advise to do is to copy the post in the default
language **after** it's completed. Unfortunately there is no
[gettext](https://en.wikipedia.org/wiki/Gettext) or similar system involved
so you will need to translate all the markdown code and not just text strings.
Once you have the original and the translations you'll need to keep them
in sync.

Here are a couple of examples for points 2 and 4

```yaml
---
title: ! 'JSON-LD in Jekyll'
tags: [json, json-ld, jekyll, tutorial]
updated: 2023-12-16 18:00:00
description: ! "Generate JSON-LD for Jekyll blog posts to improve SEO"
lang: 'en'
---
```

Note the permalink here:

```yaml
---
title: ! 'JSON-LD con Jekyll'
tags: [json, json-ld, jekyll, tutorial]
updated: 2023-12-27 16:53:00
description: ! "Generate JSON-LD for Jekyll blog posts to improve SEO"
lang: 'it'
permalink: '/it/post/json-ld-in-jekyll.html'
---
```

The next thing to fix is the index page. In my case it contains a lists of all
the posts. What I want to do is to list all the posts in the selected
language if they exist. If they don't, then fall back to the default language.
This can be achieved through a series of liquid lanugage filters. For example,
to get all posts in the selected language you can use this instruction:

```liquid
{% raw %}{%- assign site_posts_iso639 = site.posts | where: 'lang', page.lang -%}{% endraw %}
```

To get the posts in the default language you just need to change the filter
variable:

```liquid
{% raw %}{%- assign site_posts_default_iso639 = site.posts | where: 'lang', site.languages.default -%}{% endraw %}
```

Here is where the `lang` variable is useful.

Starting from these two arrays you can then use for loops and conditions to do
what I explained above, since there are no set operations in Jekyll liquid.

To generate the list of posts you need to pass the two variables like this:

```liquid
{% raw %}{%- assign site_posts_iso639 = site.posts | where: 'lang', page.lang -%}
{%- assign site_posts_default_iso639 = site.posts | where: 'lang', site.languages.default -%}
{%- include core/get_elements_by_iso639.liquid elements_iso639=site_posts_iso639 elements_default_iso639=site_posts_default_iso639 -%}
{%- assign available_posts = available_elements -%}{% endraw %}
```

For convenience I rename the variable at the end.

The result is this file is called
`_includes/core/get_elements_by_iso639.liquid`.

```liquid
{% raw %}{%- comment -%}
Generate a list of elements giving priority to the localized versions.

If a transalted elements exists use that one, otherwise use the one in the default
language which must always exist.

input:
    elements_default_iso639: array
        A list of files in the default language
    site_element_iso639: array
        A list of files in a non-default language

return:
    available_elements: array
{%- endcomment -%}
{%- assign available_elements = '' | split: ',' -%}
{%- for i in include.elements_default_iso639 -%}
    {%- assign translated_element_exists = false -%}
    {%- for j in include.elements_iso639 -%}
        {%- comment -%}Get the last path component (stem) of the two files in exam.{%- endcomment -%}
        {%- assign j_stem = j.path | split: '/' | last -%}
        {%- assign i_stem = i.path | split: '/' | last -%}
        {%- if j_stem == i_stem -%}
        {%- comment -%}Translated file found, add it to the final list.{%- endcomment -%}
            {%- assign translated_element_exists = true -%}
            {%- assign available_elements = available_elements | push: j -%}
            {%- break -%}
        {%- endif -%}
    {%- endfor -%}
    {%- if translated_element_exists == false -%}
        {%- comment -%}Translated file not found, add the default file to the final list.{%- endcomment -%}
        {%- assign available_elements = available_elements | push: i -%}
    {%- endif -%}
{%- endfor -%}{% endraw %}
```

I use this *file-function* (see below for definition) to generate the list of
pages as well. Website pages such as about, sitemap, privacy policy, etc...
work in the same way as posts.

Unfortunately, like many other operations done this way, the complexity
O(n<sup>2</sup>). This reflects on the website building time which increased
a lot since all these changes. 💥

Anyway, we now need to add some variables in the `_config.yml` file

1. site information such as title, description, etc... For example, for the
   title:

   ```yaml
   title:
      lang:
        en:                 "Franco Masotti software developer blog"
        it:                 "Franco Masotti sviluppatore software"
   ```

2. supported languages:

   ```yaml
   # Locales.
   languages:
     default: 'en'
     supported:
       - 'en'
       - 'it'
   ```

To improve the code structure you can treat your `_includes` directory as a
source of *file-functions*. Each file may have a specific input and output.
This file called `_includes/head/hreflang.liquid`, for example, generates
links to be used by search engines to
[find the same page in other languages](https://developers.google.com/search/docs/specialty/international/localized-versions?hl=en#html):

```liquid
{% raw %}{%- include seo/iso639_alternate.liquid -%}
{%- assign iso639_alt = iso639_alt | split: ',' -%}
{%- for lang in iso639_alt -%}
    {%- assign hreflang = lang | split: '|' -%}
<link rel="alternate" hreflang="{{ hreflang[0] }}" href="{{ site.rooturl }}{{ hreflang[1] }}" />
{%- endfor -%}{% endraw %}
```

An include call to the `seo/iso639_alternate.liquid` file returns an array with
the links to the current page in different languages and its language codes.

There are many more of these files and examples such as language selectors,
tags, etc... but I covered the most important points. The rest of it is in
[the source code](https://software.franco.net.eu.org/frnmst/blog) 🚀
