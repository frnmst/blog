---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: automated-tasks
software_name_python_module: automated_tasks
software_version: 10.1.0
software_version_raw: 000010.000001.000000
release_timestamp: 2021-03-15 15:46:19
is_on_pypi: false
has_changelog: false
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---
