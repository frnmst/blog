# automate.py (c) by Franco Masotti (2024)
#
# automate.py is licensed under a
# Creative Commons Attribution-ShareAlike 4.0 International License.
#
# You should have received a copy of the license along with this
# work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

# Email.
import imaplib
import email
from email.header import decode_header
from email.utils import parsedate_to_datetime

# Strings.
import re
import urllib.parse
import pprint
import datetime
from datetime import timezone
import string

# Scraping.
import mechanicalsoup
from geopy.geocoders import Nominatim
import requests

# Outputs.
import logging
import pandas as pd
import gspread_dataframe as gd

# A.I.
from transformers import pipeline

# Utils
import time
import random
from tqdm import tqdm

logging.basicConfig(level=logging.DEBUG)
CONFIG: dict = {
    'credentials': {
        'email': {
            'server_domain': 'imap.gmail.com',
            'username': 'john.doe@example.com',

            # This is the app password when using Gmail or other services.
            'password': 'my password',
        },
    },
    'outputs': {
        'excel': {
            'enabled': True,
            'filename': 'output.xlsx',
        },
        'csv': {
            'enabled': True,
            'filename': 'dump.csv',
        },
    },
    'email': {
        'inbox_directory': 'inbox',
        'scrape_unread_mails_only': False,

        # If a full email has been processed without errors, mark it as read if set to True.
        'mark_processed_emails_as_read': False,

        # Check every n seconds for new emails and do all the necessary processing.
        'poll_timeout_seconds': 3600,

        # If enabled, only read emails between start_id and end_id.
        'limit': False,
        # ids start from 0 and go to n.
        # for example you can do:
        #
        # start_id -> end_id
        #
        # 0 -> 10
        # 11 -> 20
        # 21 -> 30
        # 31 -> 100
        # 101 -> 900
        # 901 -> 910
        #
        # or
        #
        # 0 -> 1000
        # 1001 -> 3000
        # 3001 -> 3500
        'start_id': 51,
        'end_id': 500,
    },
    'misc': {
        # If set to False, do one run then quit.
        'continuous_polling': False,

        # year-month-day, all numeric.
        'date_string_format': '%Y-%m-%d',

        'nominatim': {
            # Change this randomly if you experience problems with the country
            # retrieval (exceptions).
            'user_agent': 'my script (testing) 0.9',
            'max_attempts': 3,
        },

        # Suggested.
        'scrape_bandcamp_com_webpages': True,

        # Attempt email scraping instead of using webpage (not suggested).
        'scrape_bandcamp_com_emails': False,

        # Non-bandcamp.com emails or unexpected bandcamp.com URL (not suggested).
        'scrape_generic_email': False,

        'ai': {
            # If True, summarize (and do more) using CPU-driven A.I., otherwise
            # just truncate the descriptions (or don't do anything).
            'enabled': True,

            # Summarization.
            'summarizer_object': pipeline('summarization', model='sshleifer/distilbart-cnn-12-6'),
            # Country recognition (Named Entity Recognition (NER)).
            'ner_object': pipeline('ner', model='dbmdz/bert-large-cased-finetuned-conll03-english'),

            # If True, output will be statistically random at every run.
            'random': False,

            # Summarizer text lengths.
            'min_text_length': 35,
            # Also used for trucating non-AI-summarized string at min(len(string), max_text_length)
            'max_text_length': 200,

            # Depending on the model you can't input too much text.
            'truncate_input_text_at_index': 1024,

            # By default use country names. For adjectives, add the other two items.
            # Named Entity Recognition (NER).
            'ner_country_entities': ['B-LOC', 'I-LOC'] # , 'B-MISC', 'I-MISC'],
        },
    },
    'defaults': {
        'unknown_record_label': 'Unknown Record Label',
        'self_released_record_label': 'Self released',
        'unknown_release_name': 'Unknown Release',
        'unknown_artist': 'Unknown Artist',
        'unknown_release_date': '1900-01-01',
        'unknown_country': 'Unknown Country',
        'unknown_location': 'Unknown Location',
        'unknown_colombian': None,
        'unknown_tags': '',
        'unknown_link': 'https://example.com',
    },
    'css_selectors': {
        'bandcamp_com': {
            'release_name': 'h2.trackTitle',
            'release_html_div': 'div.tralbumData.tralbum-credits',
            'release_tags': 'a.tag',
            'country_html_span': 'span.location.secondaryText',
            'extra_info_div': 'div.tralbumData.tralbum-about',
            'fallback_record_label_html_span': 'p#band-name-location > span.title',
        },
    },
    # Email regex.
    'regex': {
        'webpage': {
            'bandcamp_com': {
                'record_label': r'(©|\b\d{4}\b|\b\d{4},|[Cc]omposed by)',
                'release_info': r'\s{2,}',
            },
        },
        'email': {
            'bandcamp_com_url_track_or_album': r'(https://\S+\.bandcamp\.com/(album|track)/[a-z0-9_-]+)',
            'record_label': (
                r'('
                + r'|'.join([
                    # bandcamp.com: replace "username" with your bandcamp username.
                    r'Greetings username,\s*(.*?)\s*(just released|just added|just announced)',

                    # Compost Records.
                    r'Copyright © (.*?), All rights reserved.',

                    # Cold Busted: no useful text.
                    # ---

                    # Sonar Kollektiv.
                    r'© \d\d\d\d (.*?) GmbH',

                    ])
                + r')'
            ),
            'release_name': (
                r'('
                + r'|'.join([
                    # bandcamp.com
                    r'(just released|just added|just announced)\s*(.*?)\s*by',

                    # Compost Records.
                    r'-{60}[\r\n]{8}\*{2} "(.*?)"\r\n-{60}[\r\n]{6}',

                    # Cold Busted: no useful text.

                    # Sonar Kollektiv.
                    r'-{60}[\r\n]{6}\*{2} (.*?)\r\n-{60}[\r\n]{4}',
                ])
                + r')'
            ),
            'artist': (
                r'('
                + r'|'.join([
                    # bandcamp.com
                    r'by\s*(.*?),\s*(check it out here|check it out at)',

                    # Compost Records.
                    r'[\r\n]{6}\*\* presents[\r\n]{4}(.*?)\r\n-{60}\r\n'

                    # Cold Busted: no useful text.

                    # Sonar Kollektiv: no useful text.
                ])
                + r')'
            ),
            'release_date': (
                r'('
                + r'|'.join([
                    # Add this so the code doesn't need to be changed.
                    r'some dummy text (.*?)',

                    # Sonar Kollektiv.
                    r'Out: (\d\d\ \/ \d\d\ / \d\d\d\d?)\r\n',
                ])
                + r')'
            ),
        },
    },
    'normalization_regex': {
        # Sonar Kollektiv.
        'sk_release_date': r'\d\d\ \/ \d\d\ / \d\d\d\d',

        # Bandcamp.
        # Most of the time it uses dates such as April 1, 2007.
        'bandcamp_release_date': r'[A-Z]{1}[a-z]+ \d+, \d+',
    }
}


def extract_regex_group(re_match_tuple: tuple[str]) -> str | None:
    r"""Dynamically get the correct regex group."""

    group: list[str] = [i for i in re_match_tuple[1:] if i is not None and i != '']
    if group == []:
        return None
    else:
        return group[0]


def normalize_release_date(date: str) -> str:
    if re.match(CONFIG['normalization_regex']['sk_release_date'], date, re.IGNORECASE):
        date_obj = datetime.datetime.strptime(date, '%d / %m / %Y')
    elif re.match(CONFIG['normalization_regex']['bandcamp_release_date'], date, re.IGNORECASE):
        date_obj = datetime.datetime.strptime(date, '%B %d, %Y')
    else:
        date_obj = datetime.datetime.now()
        logging.error('cannot parse date, setting current date as release date instead')

    return date_obj.strftime(CONFIG['misc']['date_string_format'])


def append_to_excel(df: pd.DataFrame):
    logging.debug('appending data to excel file')
    try:
        df_existing = pd.read_excel(CONFIG['outputs']['excel']['filename'])
        df_combined = pd.concat([df_existing, df], ignore_index=True)
    except FileNotFoundError:
        df_combined = df
    df_combined.to_excel(CONFIG['outputs']['excel']['filename'], index=False)

    logging.debug(f'updated sheet (append) with {df.shape[0]} entries')


def get_bandcamp_com_release_url_from_email(email_content: str) -> str:
    # Find the correct url to the release page.
    # 1. get all bandcamp.com URLs without the 'unsubscribe' substring.
    # 2. deduplicate
    # 3. urls[0] to get the full URL.
    links: list[str] = list(set([link[0]
        for link in re.findall(CONFIG['regex']['email']['bandcamp_com_url_track_or_album'], email_content)
        if 'unsubscribe' not in link]))

    l: str = ''
    if len(links) >= 1:
        l = links[0]
    return l


def prepare_nominatim_attempt(current_nominatim_attempts: int) -> bool:
    if current_nominatim_attempts < CONFIG['misc']['nominatim']['max_attempts']:
        logging.info(f'nominatim attempt {current_nominatim_attempts}, remaining {CONFIG["misc"]["nominatim"]["max_attempts"] - current_nominatim_attempts}')

        # Change user agent before attempting again.
        CONFIG['misc']['nominatim']['user_agent'] = ''.join([
            CONFIG['misc']['nominatim']['user_agent'],
            random.choice(string.ascii_letters)
        ])
        return False
    else:
        return True


country_cache: dict = {}
def get_country_name(location_string: str) -> str:
    if location_string in country_cache:
        logging.debug('country cache hit')
        return country_cache[location_string]

    logging.debug('country cache miss')

    nominatim_attempts: int = 1
    done: bool = False

    while not done:
        try:
            nominatim_attempts += 1
            geolocator = Nominatim(user_agent=CONFIG['misc']['nominatim']['user_agent'])
            location = geolocator.geocode(location_string, exactly_one=True)

            if location:
                raw = location.raw
                logging.debug('country found')
                country_name_full: str = raw['display_name'].split(',')[-1].strip()

                logging.debug('adding country to country cache')
                country_cache[location_string] = country_name_full

                done = True

                return country_name_full
            else:
                done = prepare_nominatim_attempt(nominatim_attempts)
        except Exception as exc:
            done = prepare_nominatim_attempt(nominatim_attempts)
            if done:
                logging.error(exc)
                logging.error('problem retrieving country with Nominatim, using default')

    logging.error('country not found')
    return CONFIG['defaults']['unknown_country']


def get_fallback_record_label(browser) -> str:
    record_label_span = browser.page.select_one(
            CONFIG['css_selectors']['bandcamp_com']['fallback_record_label_html_span']
    )

    if record_label_span:
        record_label = record_label_span.text.strip()
    else:
        record_label = CONFIG['defaults']['self_released_record_label']

    logging.info('setting fallback record label (probably independent artist)')
    return record_label



def scrape_bandcamp_com_webpage(url: str) -> list[dict]:
    # Check if URL comes from bandcamp.com.
    u: list[str] = urllib.parse.urlparse(url).netloc.split('.')
    second_level_domain: str = '.'.join([u[-1], u[-2]])

    data: dict = {}

    # Scrape.
    if second_level_domain == 'com.bandcamp':
        # Creation date.
        data['added'] = datetime.date.today().strftime(CONFIG['misc']['date_string_format'])

        # Link.
        data['link'] = url

        # Notes are always empty.
        data['notes'] = ''

        logging.debug(f'opening web page {url}')
        browser = mechanicalsoup.StatefulBrowser()

        done: bool = False
        attempts: int = 15
        i: int = 1
        while not done:
            try:
                browser.open(url)
                #print(browser.page)
                done = True
            except requests.exceptions.RequestException as exc:
                if i <= attempts:
                    logging.error('waiting 10 seconds before retrying to connect: %s', exc)
                    logging.info(f'attempt {i} of {attempts}')
                    i += 1
                    time.sleep(10)
                else:
                    logging.error('giving up, probably connection problem: %s', exc)
                    return [{}]


        # Release name (Album), Artist
        release_details_div =  browser.page.find('div', id='name-section')
        data['release_name'] = CONFIG['defaults']['unknown_release_name'] # str
        data['artist'] = CONFIG['defaults']['unknown_artist'] # str
        if release_details_div:
            data['release_name'] = (
                release_details_div
                .select_one(CONFIG['css_selectors']['bandcamp_com']['release_name'])
                .text
                .strip()
            )
            logging.debug('release name found')

            artist_h3 = release_details_div.find('h3')
            if artist_h3:
                data['artist'] = artist_h3.select_one('a').text.strip()
                logging.debug('artist name found')
            else:
                logging.error('artist name not found, using default')
        else:
            logging.error('release details not found, using defaults')


        release_div = browser.page.select_one(CONFIG['css_selectors']['bandcamp_com']['release_html_div'])
        data['release_date'] = CONFIG['defaults']['unknown_release_date'] # str
        data['record_label'] = CONFIG['defaults']['unknown_record_label'] # str
        if release_div:
            release_info: list[str] =  re.sub(
                CONFIG['regex']['webpage']['bandcamp_com']['release_info'],
                '\n',
                release_div
                .text
                .strip()
            ).split('\n')

            # Release date.
            data['release_date'] = normalize_release_date(
                    release_info[0].removeprefix('released ').removeprefix('releases ')
            )
            logging.debug('release date found')

            # Record Label.
            # Note: this is wrong for independent artists:
            #       no record label or other info is retrieved.
            if len(release_info) >= 2:
                info: str = release_info[1].strip()

                record_label_cleaned: str = re.sub(
                    CONFIG['regex']['webpage']['bandcamp_com']['record_label'],
                    '',
                    info).strip()

                if info == record_label_cleaned:
                    # Record label not found from the bottom of the page.
                    data['record_label'] = get_fallback_record_label(browser)
                else:
                    data['record_label'] = record_label_cleaned
                    logging.debug('record label found')
                logging.info('record label scraped from web page might be wrong')

            else:
                # Record label not found from the bottom of the page.
                data['record_label'] = get_fallback_record_label(browser)
        else:
            logging.error('release info not found, using defaults')

        # Tags.
        tags_a = browser.page.select(CONFIG['css_selectors']['bandcamp_com']['release_tags'])
        data['tags'] = CONFIG['defaults']['unknown_tags'] # str
        if tags_a:
            data['tags'] = ','.join([t.text.strip().lower() for t in tags_a])
            logging.debug('tags found')
        else:
            logging.error('tags not found, using defaults')

        # Main country (location).
        country_span = browser.page.select_one(CONFIG['css_selectors']['bandcamp_com']['country_html_span'])
        data['location'] = CONFIG['defaults']['unknown_location'] # str
        data['country'] = CONFIG['defaults']['unknown_country'] # str
        data['colombian'] = CONFIG['defaults']['unknown_colombian'] # bool
        if country_span:
            location: str = country_span.text.strip()
            data['location'] = location
            logging.debug('location found')

            data['country']: str = get_country_name(location)

            data['colombian'] = False
            if data['country'] == 'Colombia':
                data['colombian'] = True
            logging.debug(f'is colombian: {data["colombian"]}, will attempt to use description if existing')
        else:
            logging.error(f'country information not found, using defaults')

        # Extra information.
        extra_info_div = browser.page.select_one(CONFIG['css_selectors']['bandcamp_com']['extra_info_div'])
        data['extra_info_summary'] = '' # str
        data['extra_info_countries'] = '' # str
        if extra_info_div:
            extra_info_text: str = extra_info_div.text.strip()

            # Truncate because the model does not accept arbitratry lengths.
            extra_info: str = extra_info_text[:CONFIG['misc']['ai']['truncate_input_text_at_index']]

            # Use the extra description to find the `[cC]olombia` substring.
            if re.search('[cC]olombia', extra_info_text):
                data['colombian'] = True
                logging.debug(f'is colombian: True, used description text instead')
            else:
                logging.debug(f'is colombian: False, description text does not contain pattern')

            # Get a summary or truncate.
            if CONFIG['misc']['ai']['enabled']:
                # Get an A.I. summary.
                logging.debug('computing description summary using A.I.')
                try:
                    data['extra_info_summary'] = CONFIG['misc']['ai']['summarizer_object'](
                            extra_info,
                            max_length=CONFIG['misc']['ai']['max_text_length'],
                            min_length=CONFIG['misc']['ai']['min_text_length'],
                            do_sample=CONFIG['misc']['ai']['random'],
                    )[0]['summary_text'].strip()
                    logging.debug('summary generated')
                except IndexError as exc:
                    logging.error('cannot compute A.I. summary, setting to empty: %s', exc)

                # Get A.I. country names (and adjectives).
                logging.debug('computing extra info countries using A.I.')
                named_entities = CONFIG['misc']['ai']['ner_object'](extra_info)
                countries: list[str] = []
                for entity in named_entities:
                    if entity['entity'] in CONFIG['misc']['ai']['ner_country_entities']:
                        countries.append(entity['word'].lower())
                data['extra_info_countries'] = ','.join(countries)
                logging.debug('extra info countries generated')

            else:
                # Don't get a summary, just truncate at specific lengths.
                min_length: int = min(len(extra_info), CONFIG['misc']['ai']['max_text_length'])
                data['extra_info_summary'] = extra_info[:min_length]
        else:
            logging.error('extra info not found, using defaults')

        # Is track (not album).
        data['type'] = 'track' # str
        if '/album/' in url:
            data['type'] = 'album'
        logging.debug(f'entry is of type "{data["type"]}"')

    else:
        logging.error('unsupported website, returning empty data')
        data = {}

    return [data]

def scrape_email(content: str) -> list[dict]:
    data: dict = {}

    # Creation date.
    data['added'] = datetime.date.today().strftime(CONFIG['misc']['date_string_format'])

    # Link.
    # Only bandcamp.com is supported at the moment.
    data['link'] = get_bandcamp_com_release_url_from_email(content)

    record_label_matches = re.findall(CONFIG['regex']['email']['record_label'], content, re.IGNORECASE)
    release_name_matches = re.findall(CONFIG['regex']['email']['release_name'], content, re.IGNORECASE)
    artist_matches = re.findall(CONFIG['regex']['email']['artist'], content, re.IGNORECASE)
    release_date_matches = re.findall(CONFIG['regex']['email']['release_date'], content, re.IGNORECASE)

    # Get the longest matches list.
    # Exclude the record label.
    max_len: int = max(
        len(release_name_matches),
        len(artist_matches),
        len(release_date_matches),
    )

    data['country'] = CONFIG['defaults']['unknown_country']
    data['location'] = CONFIG['defaults']['unknown_location'] # str
    data['colombian'] = CONFIG['defaults']['unknown_colombian'] # bool
    data['extra_info_summary'] = f'More info at {data["link"]}' # str
    data['extra_info_countries'] = '' # str
    data['type'] = 'track' # str

    if max_len == 0 and link != CONFIG['defaults']['unknown_link']:
        # Create a new entry with at least the link
        max_len = 1

    result: list[dict] = []
    for i in range(0, max_len):
        if i < len(record_label_matches):
            # Every release in the same email should belong to the same
            # record company.
            data['record_label'] = extract_regex_group(record_label_matches[0]) # str
        else:
            if len(record_label_matches) == 0:
                data['record_label'] = CONFIG['defaults']['unknown_record_label'] # str

        data['release_name'] = CONFIG['defaults']['unknown_release_name'] # str
        if i < len(release_name_matches):
            data['release_name'] = extract_regex_group(release_name_matches[i])

        data['artist'] = CONFIG['defaults']['unknown_artist'] # str
        if i < len(artist_matches):
            data['artist'] = extract_regex_group(artist_matches[i])

        data['release_date'] = CONFIG['defaults']['unknown_release_date'] # str
        if i < len(release_date_matches):
            data['release_date'] = normalize_release_date(extract_regex_group(release_date_matches[i]))

        result.append(data)

    return result


def extract_release_info(email_content) -> list[dict]:
    url: str = get_bandcamp_com_release_url_from_email(email_content)
    if CONFIG['misc']['scrape_bandcamp_com_webpages'] and url:
        return scrape_bandcamp_com_webpage(url)
    elif CONFIG['misc']['scrape_bandcamp_com_emails'] and url:
        return scrape_email(email_content)
    elif CONFIG['misc']['scrape_generic_email']:
        return scrape_email(email_content)
    else:
        logging.error('cannot process email, skipping')
        return [{}]


def decode_subject(subject):
    decoded_fragments = []
    for s, enc in decode_header(subject):
        if isinstance(s, bytes):
            decoded_fragments.append(s.decode(enc or 'utf-8', errors='ignore'))
        else:
            decoded_fragments.append(s)
    return ''.join(decoded_fragments)


def process_emails(mail_obj, messages) -> tuple[list[dict], bool, bool]:
    release_info: list[dict] = []
    error: bool = False
    kb_interrupt: bool = False

    email_ids = messages[0].split()

    logging.debug(f'total emails: {len(email_ids)}')

    # Sort emails by date
    email_list: list = []
    for i, email_id in enumerate(email_ids):
        logging.debug(f'sorting email {i}')
        status, msg_data = mail.fetch(email_id, '(RFC822)')

        # Extract the plaintext body only.
        raw_email = msg_data[0][1]

        msg = email.message_from_bytes(raw_email)
        email_list.append((parsedate_to_datetime(msg['Date']), msg))

    # Convert all datetimes to a common timezone.
    common_timezone = timezone.utc
    email_list = [(date.replace(tzinfo=common_timezone), msg) for date, msg in email_list]

    # Sort the email list by date.
    sorted_email_list = sorted(email_list, key=lambda x: x[0])

    # Remove date info from email list.
    sorted_email_list = [mail for _, mail in sorted_email_list]

    try:
        for i, msg in enumerate(sorted_email_list):
            if CONFIG['email']['limit']:
                if i < CONFIG['email']['start_id'] or i > CONFIG['email']['end_id']:
                    logging.debug(f'skipping email {i}')
                    continue
                # reverse condition: i >= CONFIG['email']['start_id'] and i <= CONFIG['email']['end_id']

            logging.debug(f'processing email {i}')

            subject = decode_subject(msg['subject'])
            from_ = msg.get('From')
            logging.info(f'email from: {from_} with subject: {subject}')

            plaintext_body: str = ''

            if msg.is_multipart():
                for part in msg.walk():
                    content_type = part.get_content_type()
                    if content_type == 'text/plain':
                        plaintext_body = part.get_payload(decode=True).decode('utf-8', errors='ignore')
                        break
            else:
                plaintext_body = msg.get_payload(decode=True).decode()

            if plaintext_body:
                # Extract release info from email and webpage.
                scraped_content: list[dict] = extract_release_info(plaintext_body)

                # Ignore empty content.
                if scraped_content != [{}]:
                    for sc in scraped_content:
                        # Convert dictionaries to tuples for comparison.
                        sc_tuple = tuple(sorted(sc.items()))

                        # Check if the tuple is already present in the release_info list.
                        if any(sc_tuple == tuple(sorted(d.items())) for d in release_info):
                            logging.info('skipping duplicate entry')
                        else:
                            release_info.append(sc)

            if CONFIG['email']['mark_processed_emails_as_read']:
                mail_obj.store(email_id, '+FLAGS', '\\Seen')

    except Exception as exc:
        logging.error('saving status because an exception occurred: %s', exc)
        error = True
    except KeyboardInterrupt as exc:
        logging.error('saving status because user interrupted the program: %s', exc)
        kb_interrupt = True

    logging.debug(f'total emails processed: {i + 1}')

    return release_info, error, kb_interrupt


if __name__ == '__main__':
    # 2024, Franco Masotti
    # https://blog.franco.net.eu.org
    i: int = 1
    error: bool = False
    kb_interrupt: bool = False
    done: bool = False

    while not done:
        logging.debug(f'iteration {i}')
        release_info: list[dict] = []

        # Connect to the email server.
        mail = imaplib.IMAP4_SSL(CONFIG['credentials']['email']['server_domain'])
        mail.login(CONFIG['credentials']['email']['username'], CONFIG['credentials']['email']['password'])
        mail.select(CONFIG['email']['inbox_directory'])

        # Search for the emails.
        search_string: str = 'ALL'
        if CONFIG['email']['scrape_unread_mails_only']:
            search_string = 'UNSEEN'
        status, messages = mail.search(None, search_string)
        if status == 'OK':
            logging.debug('email status: OK')

            release_info, error, kb_interrupt = process_emails(mail, messages)

            mail.logout()
            logging.debug('finished processing emails')

            df: pd.DataFrame = pd.DataFrame(release_info)

            if CONFIG['outputs']['csv']['enabled']:
                df.to_csv(CONFIG['outputs']['csv']['filename'], sep='\t', index=True)
            if CONFIG['outputs']['excel']['enabled']:
                append_to_excel(df)
        else:
            logging.error(f'problem with emails, status {status}')
            error = True

        if error or kb_interrupt or not CONFIG['misc']['continuous_polling']:
            done = True
            logging.critical('quitting')
        else:
            logging.debug(f'sleeping for {CONFIG["email"]["poll_timeout_seconds"]}')

            # Print the progress bar.
            for _ in tqdm(range(CONFIG['email']['poll_timeout_seconds'])):
                time.sleep(1)

        i += 1
