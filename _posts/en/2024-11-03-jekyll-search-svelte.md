---
title: ! 'Jekyll search with Svelte'
tags: [jekyll, svelte, javascript, search, json, programming, tutorial, youtube, video]
related_links: ['https://www.youtube.com/watch?v=Cbd49Srroj4']
updated: 2024-11-04 13:00:00
description: ! "A simple search page for static Jekyll blogs implemented with Svelte"
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/Cbd49Srroj4?si=p010q3iudN65vsh2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<!--TOC-->

- [Summary](#summary)

<!--TOC-->

## Summary

<!--excerpt_start-->Creating a search page involves several steps. I started by creating a new endpoint<!--excerpt_end--> in Jekyll to serve JSON data from my posts. I simplified the original field naming from an already existing JSON-LD template file and removed unnecessary structures. For each blog post object, there are only six fields - four of which are useful for doing the search matching.

To create a new Svelte project, I installed Vite, a frontend build tool. I ran the command to create a vite project and followed the prompts to configure it. This created a new directory with a Svelte web app template. The interesting files are in the `src` directory, where I found an example counter component. I then went on to create a simple search component that connects to my Jekyll JSON post page.

When you type something into the search bar, you get immediate results - a feature called reactivity. To integrate this web app with your Jekyll blog, you need to configure it to move the compiled files into the `_assets` directory. This is done by adding some variables to the `vite.config.js` file.

Once I ran `npm build`, everything worked smoothly. I was able to see my search page in my Jekyll blog. The reactivity feature allowed me to interact with the post data in real-time, making it easy to display the filtered posts. Using a for loop, I retrieved the title, URL, and picture of each post, similar to how you would use Liquid Templating Language in Jekyll. Overall, this setup provided an interactive and reactive way to search my blog's content.
