---
title: ! 'ShellGPT: Ollama interaction via terminal'
tags: [shellgpt, chatgpt, ai, ollama, tutorial, youtube, video]
related_links: ['https://www.youtube.com/watch?v=P7aC1wNcYKs']
updated: 2025-01-28 00:00:00
description: ! "ShellGPT: use ChatGPT or your local AIs via terminal"
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/P7aC1wNcYKs?si=DOrv0d-821MZl7ID" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<!--TOC-->

- [Summary](#summary)

<!--TOC-->

## Summary

<!--excerpt_start-->In this video, I introduce ShellGPT, an AI program that functions as a terminal command line version of<!--excerpt_end--> ChatGPT. Unlike the default OpenAI API, which requires a subscription, ShellGPT can also utilize Ollama, enhancing privacy, especially when self-hosted. in previous videos I explain how to set up Ollama: check them out.

There is a cautionary note about its optimization for local models, which could affect its performance. The setup process is straightforward, involving configuration edits to ensure the program uses the correct API URL and token, if applicable. Chat history can be saved for future reference.

As I explored the features, I encountered some challenges with the shell integration. The default key binding for interacting with ShellGPT was not ideal, as it conflicts with the terminal's clear command (CTRL+L). Additionally, pressing the tab key for autocompletion inadvertently triggered ShellGPT, which is frustrating. Sometimes, using ChatGPT models results in outputs that included explanations rather than direct shell commands. This can hinder efficiency.

Finally, there are different modes available in ShellGPT, such as REPL, one-off, code, and shell command modes. While I could create new roles for LLM interactions, the need for adjustments when changing models is a potential drawback. Overall, ShellGPT appears to be a powerful tool, but I need to be mindful of its limitations and configuration requirements to fully leverage its capabilities in my terminal workflows.
