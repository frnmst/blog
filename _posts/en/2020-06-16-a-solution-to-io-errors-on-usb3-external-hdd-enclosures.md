---
title: A solution to I/O errors on some external USB 3 HDD enclosures
tags: [tutorial, hdd, usb-3, error, i-o, uas, usb]
updated: 2022-10-08 15:33:00
description: How to solve I/O errors on some external USB 3 HDD enclosures
lang: 'en'
---

<!--excerpt_start-->
I bought several [Inatek fe 3001](https://www.inateck.com/inateck-fe3001-aluminum-3-5-usb-3-0-superspeed-hdd-external-enclosure.html)
USB HDD enclosures for my backups<!--excerpt_end-->.

This model is reported like this by `$ lsusb`:

    ID 174c:55aa ASMedia Technology Inc. Name: ASM1051E SATA 6Gb/s bridge, ASM1053E SATA 6Gb/s bridge, ASM1153 SATA 3Gb/s bridge, ASM1153E SATA 6Gb/s bridge

Suddenly I got `read error from input device (I/O error)` from `$ dmesg` and then the filesystem unmounted automatically.

I tried:

- remounting the filesystem
- swapping cables
- swapping enclosures
- formatting the hard drives with a different filesystem,

Nohing worked.

Apparently, to get the thing working, you must disable [UAS (USB Attached SCSI)](https://en.wikipedia.org/wiki/USB_Attached_SCSI) for this particular chipset.

## Solution

If you use Syslinux as the bootloader and certain kernels:

1. add the following kernel parameter to the `APPEND` line in `/boot/syslinux/syslinux.cfg`:

       usb-storage.quirks=174c:55aa:u

2. reboot
3. run `$ lsusb -t` and check that the `Driver` entry now reads `usb-storage` instead of `uas`

This works for example with Parabola GNU/Linux-libre.

Some websites say to blacklist the module within `/etc/modprobe.d`. That depends on your kernel.

## References

- [Stack Exchange](https://unix.stackexchange.com/questions/239782/connection-problem-with-usb3-external-storage-on-linux-uas-driver-problem)
- [Ubuntu Forums](https://ubuntuforums.org/showthread.php?t=2307662)
- [Stack Exchange](https://unix.stackexchange.com/questions/441668/debian-usb3-hdd-uas-i-o-errors/441772#441772)
- [Reddit](https://www.reddit.com/r/archlinux/comments/4rf3ey/uasp_usb_30_to_sata_iii_ssd_enclosures_that_work/)
- [linux-sunxi.org](https://linux-sunxi.org/USB/UAS#UASP_capable_chipsets_in_disk_enclosures)
- [Blacklist](https://github.com/geraldvillorente/blacklist-uas/blob/master/bin)
- [Arch Wiki](https://wiki.archlinux.org/index.php/Kernel_parameters)
- [Raspberry PI forum](https://www.raspberrypi.org/forums/viewtopic.php?t=237829)
- [Launchpad](https://bugs.launchpad.net/ubuntu/+source/linux/+bug/868233)
- [bugzilla.kernel.org](https://bugzilla.kernel.org/show_bug.cgi?id=79511)
- [Gentoo Forum](https://forums.gentoo.org/viewtopic-t-1040382-start-0.html)
