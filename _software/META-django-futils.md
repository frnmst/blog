---
meta: true
software_name: django-futils
archived: false
repositories:
  - label: 'canonical'
    url: 'https://software.franco.net.eu.org/frnmst/django-futils'
  - label: 'codeberg'
    url: 'https://codeberg.org/frnmst/django-futils'
  - label: 'framagit'
    url: 'https://framagit.org/frnmst/django-futils'
documentation:
  - label: 'docs.franco.net.eu.org/django-futils'
    url: 'https://docs.franco.net.eu.org/django-futils'
  - label: 'changelog'
    url:
---
