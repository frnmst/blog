#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2023 Franco Masotti
#
# SPDX-License-Identifier: MIT

import random
import sys

if __name__ == '__main__':
    balls: int = int(sys.argv[1])
    height: int = int(sys.argv[2])

    # List representing the destination columns (bins) of the balls.
    # Just like in the picture, the number of columns is
    # equal to the number of peg levels + 1.
    c: list = [0 for x in range(0, height + 1)]

    i: int = 0
    while i < balls:
        j: int = 0
        right: int = 0
        while j < height:
            x: float = random.random()

            # Peg.
            # 50% probability to go right.
            if x > 0.5:
                right += 1

            j += 1

        # Add a new ball the destination bins.
        c[right] += 1

        i += 1

    print(c)
