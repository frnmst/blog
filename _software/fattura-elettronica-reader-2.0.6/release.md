---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fattura-elettronica-reader
software_name_python_module: fattura_elettronica_reader
software_version: 2.0.6
software_version_raw: 000002.000000.000006
release_timestamp: 2021-07-07 15:16:54
is_on_pypi: false
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Updated file checksum variable.
- Updated readme.
