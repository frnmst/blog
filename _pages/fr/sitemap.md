---
layout: page
title: ! 'Plan du site'
permalink: /fr/sitemap/
description: ! 'Plan du site de blog.franco.net.eu.org'
lang: 'fr'
---
## Pages

{% include sitemap/sitemap_pages.html %}

## Articles

Les articles sont triés par ordre de date décroissante.

{% include sitemap/sitemap_posts.html %}
