---
layout: software_release
enable_markdown: true
title: release
excerpt: none
tags: [md-toc, markdown, table-of-contents, python]
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.2.3
software_version_raw: 000008.000002.000003
release_timestamp: 2024-02-15 17:20:40
is_on_pypi: true
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---
### Added

- [__✓__ fbf9359](https://software.franco.net.eu.org/frnmst/md-toc/commit/fbf9359)

  Add `./SECURITY.md` file.
- [__✓__ fbf9359](https://software.franco.net.eu.org/frnmst/md-toc/commit/fbf9359)

  Add [pyupgrade](https://github.com/asottile/pyupgrade) to pre-commit.
- [__✓__ 696d334](https://software.franco.net.eu.org/frnmst/md-toc/commit/696d334)

  Add more project metadata and funding info.

- [__✓__ 9edbafc](https://software.franco.net.eu.org/frnmst/md-toc/commit/9edbafc)

  Cleanup pre-commit hooks: remove unused ones and add some from the default
  pre-commit repository.

- [__✓__ 7c6fb22](https://software.franco.net.eu.org/frnmst/md-toc/commit/7c6fb22)

  Add more classifiers in `./setup.cfg`.

### Changed

- [__✓__ 2bf06bb](https://software.franco.net.eu.org/frnmst/md-toc/commit/2bf06bb)

  Remove the `md_toc.generic._utf8_array_to_string` function. Replace
  this with a single Python line in the
  `md_toc.cmark.buffer_c._cmark_cmark_strbuf_put`
  function:

  ```python
  dt = bytearray(data).decode('UTF-8')
  ```

### Fixed

- [__✓__ 2bf06bb](https://software.franco.net.eu.org/frnmst/md-toc/commit/2bf06bb)

  Replace variable correctly in the
  `md_toc.cmark.buffer_c._cmark_cmark_strbuf_put` function.
- [__✓__ fbf9359](https://software.franco.net.eu.org/frnmst/md-toc/commit/fbf9359)

  Fix `/.pre-commit-config.yaml` file indentations.
- [__✓__ fbf9359](https://software.franco.net.eu.org/frnmst/md-toc/commit/fbf9359)

  Cleanup *Pre-commit hook* documentation page.

- [__✓__ 9edbafc](https://software.franco.net.eu.org/frnmst/md-toc/commit/9edbafc)

  Fix some cmark code translated from C: `memmove` functions should now be
  implemented correctly.

- [__✓__ 7c6fb22](https://software.franco.net.eu.org/frnmst/md-toc/commit/7c6fb22)

  Move [Bandit](https://github.com/PyCQA/bandit) configuration from
  `/.pre-commit-config.yaml` to `./pyproject.toml`.

- [__✓__ 7c6fb22](https://software.franco.net.eu.org/frnmst/md-toc/commit/7c6fb22)

  Disable upper pinning Python version, as suggested by
  [this](https://iscinumpy.dev/post/bound-version-constraints/#pinning-the-python-version-is-special)
  document.

### Removed

- [__✓__ 9edbafc](https://software.franco.net.eu.org/frnmst/md-toc/commit/9edbafc)

  Remove an `import` case for Python < 3.8: recent versions of md-toc support
  Python >= 3.8.

Note: GitHub Flavored Markdown is still at version 0.29. md-toc does its best
to support both GFM 0.29 and cmark 0.30.
