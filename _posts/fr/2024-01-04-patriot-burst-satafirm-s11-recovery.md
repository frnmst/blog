---
title: ! "Réparer un Patriot Burst avec l'erreur SATAFIRM S11"
tags: [satafirm-s11, ssd, contrôleur-ssd, erreur-micrologiciel, firmware-endommagé]
updated: 2024-01-05 21:00:00
description: ! "J'ai pu réparer un Patriot Burst avec l'erreur SATAFIRM S11 (firmware endommagé) au détriment de ses données"
lang: 'fr'
permalink: '/fr/post/patriot-burst-satafirm-s11-recovery.html'
images:
  - id: 'post_firmware_flash'
    filename: 'post_firmware_flash.png'
    alt: 'Résultat après flashage du firmware sur un Patriot Burst'
    caption: ! 'Reflash du firmware sur un SSD Patriot Burst'
    width: '300px'
  - id: 'bios_disk_mode'
    filename: 'bios_disk_mode.jpg'
    alt: ! "Écran d'ordinateur affichant les options de disque dans le BIOS"
    caption: ! "N'oubliez pas de définir le bon mode!"
    width: '300px'
  - id: 'translated_readme'
    filename: 'translated_readme.png'
    alt: ! "Section lisez-moi traduite du russe vers l'anglais"
    caption: ! 'Une partie du fichier readme traduit'
    width: '300px'
---
{% include image.html id="post_firmware_flash" %}

## Table des matières

<!--TOC-->

- [Table des matières](#table-des-matières)
- [Introduction](#introduction)
- [Ce qui s'est passé](#ce-qui-sest-passé)
- [Étapes pour réparer le SSD dans un état utilisable](#étapes-pour-réparer-le-ssd-dans-un-état-utilisable)
  - [Outils](#outils)
  - [Mode disque](#mode-disque)
  - [Obtenir le bon fichier de micrologiciel](#obtenir-le-bon-fichier-de-micrologiciel)
  - [Créer le flasher](#créer-le-flasher)
  - [Exécution](#exécution)
- [Conclusion](#conclusion)

<!--TOC-->

## Introduction

<!--excerpt_start-->J'ai pu réparer un SSD
[Patriot Burst](https://www.patriotmemory.com/products/burst-sata-iii-retired-2-5-ssd-box-package)
avec 240 GB dont le firmware a été endommagé.<!--excerpt_end--> **Si vous
essayez ceci, vous perdrez toutes les données mais au moins vous pourrez à
nouveau utiliser le disque.** Malheureusement, vous aurez également les données
S.M.A.R.T. effacées, comme neuves:

```
SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAG     VALUE WORST THRESH TYPE      UPDATED  WHEN_FAILED RAW_VALUE
1 Raw_Read_Error_Rate       0x000b   100   100   050    Pre-fail  Always       -       0
9 Power_On_Hours            0x0012   100   100   000    Old_age   Always       -       0
12 Power_Cycle_Count        0x0012   100   100   000    Old_age   Always       -       1
168 SATA_Phy_Error_Count    0x0012   100   100   000    Old_age   Always       -       0
170 Bad_Blk_Ct_Erl/Lat      0x0003   100   100   010    Pre-fail  Always       -       0/301
173 MaxAvgErase_Ct          0x0012   100   100   000    Old_age   Always       -       1
192 Unsafe_Shutdown_Count   0x0012   100   100   000    Old_age   Always       -       0
194 Temperature_Celsius     0x0023   067   067   000    Pre-fail  Always       -       33 (Min/Max 33/33)
218 CRC_Error_Count         0x000b   100   100   050    Pre-fail  Always       -       0
231 SSD_Life_Left           0x0013   100   100   000    Pre-fail  Always       -       100
241 Lifetime_Writes_GiB     0x0012   100   100   000    Old_age   Always       -       0
```

En lisant divers articles en ligne, la cause de cette corruption du
micrologiciel ne m'est pas claire: est-ce un problème d'usure ou la qualité du
contrôleur (PS3111-S11 ou PS3111-S11T) est-elle faible? Y a-t-il eu un problème
électrique? Aucune idée.

{% include link_preview.html url="https://forum.hddguru.com/viewtopic.php?f=10&t=39161&mobile=desktop" %}

J'ai trouvé
[cette feuille](https://docs.google.com/spreadsheets/d/1B27_j9NDPU3cNlj2HKcrfpJKHkOf-Oi1DbuuQva2gT4/edit#gid=0)
de calcul qui répertorie de nombreux SSD différents. Vous pouvez trier la liste
par contrôleur afin de voir lesquels pourraient être affectés par ce problème.

## Ce qui s'est passé

De toute façon, ce qui m'est arrivé, c'est que le disque dur a soudainement
disparu des périphériques système alors que l'ordinateur fonctionnait
normalement. Ceci et d'autres erreurs associées apparaissaient continuellement
sur `dmesg`

```
BTRFS warning (device sdb2): lost page write due to IO error on /dev/sde2
```

Le disque faisait partie d'un array Btrfs RAID 1 contenant des dépôt Git
servis par Gitea. J'avais toujours les données sur l'autre disque jumeau et les
sauvegardes, donc je n'étais pas inquiet.

Ce que j'ai fait ensuite, c'est d'éteindre le serveur et d'essayer de vérifier
si le disque fonctionnait: à l'époque, je ne savais pas ce qui s'était passé.
Je pensais que le câble d'alimentation ou le câble SATA s'était détaché. J'ai
remarqué que le disque était reconnu comme SATAFIRM S11 et non comme Patriot
Burst par `smartctl`

```
Device Model:     SATAFIRM   S11
```

Ce que j'ai fait maintenant, c'est *réparer* le système de fichiers en
utilisant certains des outils fournis par Btrfs. Le seul qui a fonctionné est
`btrfs scrub`. Toutes les erreurs étaient désormais corrigées. En fait, lorsque
j'ai essayé de rsync avant le nettoyage, j'ai eu des erreurs d'E/S, mais pas
après.

Pour éviter davantage de problèmes, j'ai ensuite créé une nouvelle array Btrfs
RAID 1 entre le deuxième disque et un disque de rechange. Cela a laissé le SSD
*cassé* hors du serveur.

## Étapes pour réparer le SSD dans un état utilisable

### Outils

Récupérer le SSD n'a pas été si difficile une fois que vous avez obtenu les
[trois outils](https://drive.google.com/drive/folders/1xuHNNRUeO-ls19I2bQ96SfpodolndMVL):

- le programme d'identification du micrologiciel
- le flasher du micrologiciel
- les fichiers binaires (micrologiciels)

Vous pouvez également en télécharger deux [ici](http://vlo.name:3000/ssdtool/).
Accédez à la section *utilitaire Phison* et récupérez
l'[identifiant flash Phison (pour S5/S8/S9/S10/S11)](http://vlo.name:3000/ssdtool/)
et le
[Phison S11 firmware flasher v2](http://vlo.name:3000/tmph/s11-flasher.rar).
Les fichiers du firmware sont marqués avec
[Phison PS3111 Firmware [SBFM]](https://www.usbdev.ru/?wpfb_dl=6583).
L'explication est sur [cette page](https://www.usbdev.ru/files/phison/ps3111fw/).

Tous ces fichiers sont RAR, vous pouvez donc utiliser l'utilitaire `unrar` sur
GNU/Linux pour les extraire.

### Mode disque

Malheureusement, vous avez besoin de Windows (soupir) pour effectuer la
récupération. Windows 10 a bien fonctionné, à condition de connecter le disque
via SATA et de définir le mode disque sur AHCI. Au début, en fait, cela ne
fonctionnait pas sur mon ordinateur portable. Je l'ai branché en USB : non
reconnu. J'ai utilisé le slot SATA interne : non reconnu. Le mode RAID a été
défini dans le BIOS.

{% include image.html id="bios_disk_mode" %}

Si vous ouvrez l'un des readme, qui sont pour la plupart en russe, vous lirez
ceci (traduit)

> Le disque doit être connecté à un contrôleur SATA (la connexion via Firewire n'est pas prise en charge) ; il prend en charge les pilotes IDE et AHCI standard de Microsoft et Intel RST. Le fonctionnement avec d'autres pilotes n'a pas été testé. Cela ne fonctionnera probablement pas avec les pilotes de contrôleur de la section "Contrôleurs SCSI et RAID" (sauf les versions RST à partir de 11.7).
>
> Il est également possible de travailler via certains ponts USB-Sata, par exemple asmedia asm105x/115x, jmicron jm[s]20329/539/578, initio inic1608 et quelques autres.

Cela signifie qu'il est préférable de brancher le SSD via SATA et de définir le
mode AHCI dans le BIOS.

{% include image.html id="translated_readme" %}

### Obtenir le bon fichier de micrologiciel

Vous devez identifier le micrologiciel spécifique dont vous avez besoin via le
programme `phison_flash_id.exe`. La valeur de la ligne `S11fw` identifie le
micrologiciel spécifique à utiliser. J'ai obtenu `SBFM61.3, 2019Jul26`.
Obtenez le fichier correct dans le répertoire `Firmware PS3111` (les fichiers
binaires mentionnés précédemment). Il y avait plus d'un fichier pour le même
micrologiciel dans mon cas, j'ai donc obtenu le plus récent. La date du fichier
est au format `jjmmaaaa`, juste après le caractère de soulignement. Ce schéma
est utilisé:

```
${FIRMWARE_ID}_$(date +%d)$(date +%m)$(date +%Y).BIN
```

Dans mon cas j'ai choisi `SBFM61.3_11032021.BIN`.

Vous devez ensuite renommer `SBFM61.3_11032021.BIN` en `fw.BIN` et le copier
dans le répertoire du flasher de micrologiciel.

### Créer le flasher

Vous pouvez maintenant créer le programme flasher. Exécutez le script
`s11-flasher2-toshiba.cmd` script for the Patriot Burst SSD. You'll see a new
executable called `fw.exe` in the directory.

### Exécution

Après l'avoir exécuté, vous devez cliquer sur le bouton `Upgrade Firmware` et
attendre quelques instants. Ensuite, c'est fait.

## Conclusion

Il existe de nombreuses vidéos sur YouTube qui expliquent toutes ces étapes. De
plus, lorsque vous *unrar* les outils, vous y trouverez leurs fichiers readme.

{% include link_preview.html url="https://www.youtube.com/watch?v=OoDZlJjTc_I" %}
{% include link_preview.html url="https://www.youtube.com/watch?v=BQ-QRkO9VOI" %}

Leçon apprise: évitez ce type de SSD et vérifiez si le contrôleur issu des
spécifications est fiable. Vais-je utiliser ce SSD en production? Je ne pense
pas: ce sera une pièce de rechange.

🚀
