#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2023 Franco Masotti
#
# SPDX-License-Identifier: MIT

import random
import sys
import time

if __name__ == '__main__':
    iterations: int = int(sys.argv[1])
    w: float = float(sys.argv[2])
    i: int = 0
    times: list() = list()

    assert w <= 1.0

    while i < iterations:
        print('iteration: ' + str(i + 1))
        hit_wall: int = 0

        # Initial value.
        zombie: float = 100.0

        done: bool = False

        start = time.time()
        while not done:
            if zombie > w and time.time() - start >= 1.0:
                # Previous zombie hit the wall.
                zombie = random.random()
                hit_wall += 1
                start = time.time()

            if zombie <= w:
                done = True

        times.append(hit_wall)

        i += 1

    print('times = ' + str(times))
    print('avg secs = ' + str(sum(times) / iterations))
