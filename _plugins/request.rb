# See
# https://blog.bryanroessler.com/2020-03-18-insert-code-blocks-from-repository/
require 'open-uri'
require 'addressable/uri'

module Jekyll
    class Request < Liquid::Tag

        def initialize(tag_name, url, tokens)
            super
            @url = url.strip
        end

        def render(context)
            url = context[@url]
            @filename = File.basename(url)
            encoded_url = Addressable::URI.encode(url)
            @file = URI.parse(encoded_url).read
            @file
        end

    end
end

Liquid::Template.register_tag('request', Jekyll::Request)
