geopy
gspread
gspread-dataframe
# Scraping.
mechanicalsoup
openpyxl

# Data.
pandas
torch

# Utils.
tqdm

# A.I.
transformers
