---
title: ! 'ESP8266 flammable gas alarm - part 2'
tags: [circuit, esp8266, mq-2, gas detector, gas alarm, tutorial, youtube, video]
related_links: ['https://www.youtube.com/watch?v=CBjyS3Jq54Q']
updated: 2024-09-07 14:06:30
description: ! "Second part of the gas detector-alarm ESP8266 video"
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/CBjyS3Jq54Q?si=yqUueU7Vfl5dVJy9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


<!--TOC-->

- [Summary](#summary)
- [Arduino sketch](#arduino-sketch)

<!--TOC-->

## Summary

<!--excerpt_start-->In this project update, I recap the development of a simple flammable gas detector<!--excerpt_end--> equipped with an alarm. I’ve transitioned the circuit to a mini breadboard, which is a practical choice for making the project more permanent. To support the components, I repurposed a broken portable doorbell, making necessary adjustments to fit the pieces and allow for cable passage. The display and DHT11 sensor are securely attached with super glue, while the MQ-2 sensor is affixed using 3M tape for safety.

Moving on to the software side, I’ve made improvements to the main loop by replacing blocking delay operations with a more efficient time difference method for updating and displaying sensor data. The gas alarm loop has also been simplified. Additionally, I introduced an HTML dashboard and a JSON endpoint, allowing the ESP to serve a static HTML page that refreshes every five seconds to display sensor readings. The JSON endpoint provides structured data that can be accessed by other machines.

While the web server setup is functional, it operates over plain HTTP, which poses security risks. I discuss the challenges of implementing HTTPS on the board, noting that self-signed certificates would not be trusted by browsers. If there were a *Let's Encrypt* library for Arduino, it could simplify the process. Alternatively, I propose using the board as an HTTPS client to send JSON data to a more secure server.

Looking ahead, I mention the potential for logging data over time, which could be visualized in graphs for better analysis. This idea will be explored in a future video.

## Arduino sketch

{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/solve-computer-science/arduino/esp8266-flammable-gas-alarm-part-2/gas_sensor_dashboard.ino" text="schetch.ino" name="schetch.ino" is_text=true code_lang="cpp" %}
