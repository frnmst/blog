---
title: Apache from Arch to Debian
tags: [tutorial, apache, arch, debian]
updated: 2022-10-08 15:33:00
description: Migration Apache's configuration from Arch GNU/Linux-based to Debian GNU/Linux-based distos
lang: 'en'
---

<!--excerpt_start-->
Here are some simple steps to migrate Apache's configuration
from an Arch GNU/Linux-based distro to a Debian GNU/Linux-based distro<!--excerpt_end-->.

1. `systemctl stop apache2`
2. `cd /etc/apache2`
3. backup the `apache2.conf` file
4. copy the original file `httpd.conf` from Arch and rename it to `apache2.conf`
5. go through all the modules and enable them with this command:

        a2enmod ${module_name}

6. remove all the `LoadModule` directives
7. add this at the end of `apache2.conf`:

   ```
   # Include generic snippets of statements
   IncludeOptional conf-enabled/*.conf

   # Include the virtual host configurations:
   IncludeOptional sites-enabled/*.conf

   Include /etc/apache2/mods-enabled/*.load
   Include /etc/apache2/mods-enabled/*.conf
   ```

8. copy the extra configurations you think are useful from Arch, located in `/etc/httpd/conf/extra`, into `/etc/apache2/conf-available` and enable them
   with `a2enconf ${conf_name%%.conf}`
10. chage some variables in `apache2.conf`:

        ServerRoot "/etc/apache2"
        User www-data
        Group www-data

11. run `systemctl restart apache2`
