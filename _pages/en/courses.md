---
layout: page
title: Courses
permalink: /courses/
description: ! 'Courses'
lang: 'en'
---
{%- assign courses = site.courses | sort: 'title' -%}
{%- for c in courses -%}
- [{{ c.title }}]({{ c.permalink }})
{% endfor %}

## Notes

Video backups have been made with this tool and options

```shell
yt-dlp ${URL} \
    --write-auto-sub \
    --sub-langs "en,it" \
    --prefer-free-formats \
    --prefer-ffmpeg \
    --fixup detect_or_warn \
    --no-call-home \
    --no-overwrite \
    --merge-output-format webm \
    --embed-subs \
    --add-metadata
```
