---
layout: software_release
enable_markdown: true
title: release
excerpt: none
tags: [md-toc, markdown, table-of-contents, python]
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.2.2
software_version_raw: 000008.000002.000002
release_timestamp: 2023-11-09 12:21:37
is_on_pypi: true
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Add unit test support for multiple Python versions using
  [tox](https://tox.wiki) and [asdf](https://asdf-vm.com/)
- Bump fpyutils to 4.0.1. See `Fixed` section below.

### Fixed

- This unit test for md-toc version 8.2.1 failed when the new version of
  [fpyutils](https://blog.franco.net.eu.org/software/#fpyutils) was used in the
  `/requirements.txt` file:

  ```diff
  - fpyutils>=3.0.1,<4
  + fpyutils==4.0.0
  ```

  ```
  $ python -m unittest discover

  .ssss......s......sFs.......
  ======================================================================
  FAIL: test_write_string_on_file_between_markers (md_toc.tests.tests.TestApi.test_write_string_on_file_between_markers)
  Test that the TOC is written correctly on the file.
  ----------------------------------------------------------------------
  Traceback (most recent call last):
    File "/dev/shm/md-toc/md_toc/tests/tests.py", line 529, in test_write_string_on_file_between_markers
      self.assertEqual(
  AssertionError: 'hello\n' != 'hello\n<!--TOC-->\n\nThis is a static line\n\n<!--TOC-->\n'
    hello
  + <!--TOC-->
  +
  + This is a static line
  +
  + <!--TOC-->


    ----------------------------------------------------------------------
  Ran 28 tests in 0.090s

  FAILED (failures=1, skipped=7)
  ```

  This new bug was caused by
  [the new changes in fpyuils 4.0.0](https://blog.franco.net.eu.org/software/CHANGELOG-fpyutils.html#400---2023-11-06)
- Support older Python versions >= 3.8, using `__future__` imports. See also
  fpyutils 4.0.1

Note: GitHub Flavored Markdown is still at version 0.29. md-toc does its best
to support both GFM 0.29 and cmark 0.30.
