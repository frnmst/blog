---
layout: page
title: ! 'Riguardo a ...'
permalink: /it/about/
description: ! 'Riguardo a e contatti di Franco Masotti'
lang: 'it'
---

Questo blog è mantenuto da Franco Masotti. Troverete posts riguardo alle
scienze informatiche, software, hardware, trucchi, tecnologia, ecc...

Spero che questi articoli si rivelino utili.

## Contatti

| Repositories |
|:-------------|
| [Repositories ufficiali](https://software.franco.net.eu.org/frnmst) |
| [Codeberg](https://codeberg.org/frnmst) |
| [Framagit](https://framagit.org/frnmst) |
| [GitHub](https://github.com/frnmst) (vedi anche [questo post]({%- include post/post_url_iso639.liquid post_url="2021-09-02-quitting-github" -%}{{ post_url_iso639 }})) |

| Emails |
|:-------|
| [mio nome].[mio cognome]@tutanota.com |
| [mio nome].[mio cognome].1@protonmail.com |

| Altro |
|:------|
| [Solve Computer Science YouTube channel](https://www.youtube.com/channel/UC2rr0LbIuy34JHEoCndmKiA) |
| [Condivisione anonima via Nextcloud](https://cloud.franco.net.eu.org/s/QyqgfCPHdemjCcp) |
| Nextcloud (Open Cloud Mesh protocol) a [frnmst@cloud.franco.net.eu.org](https://cloud.franco.net.eu.org)
| [ID Keyoxide](https://keyoxide.org/396da54dc8019e4f4522cbd5a3fa3c2b4230215a) |

## Studi

- Laurea in Informatica @ University of Ferrara: [*Integrazione di una applicazione web con strumenti per la statistica*](https://software.franco.net.eu.org/frnmst-archives/thesis)
- [ORCID iD](https://orcid.org/0000-0002-1736-3858)
- [CS198.1x: Bitcoin and Cryptocurrencies](https://courses.edx.org/certificates/2849569d6b2e422e8652b5ff93f8ea09)

## Le mie chiavi PGP pubbliche

- [Chiave pubblica per la firma dei commit Git](/pubkeys/pgp_pubkey_since_2019.txt)
- [Chiave pubblica ID Keyoxide ID](/pubkeys/pgp_pubkey_frnmst_keyoxide.txt)
