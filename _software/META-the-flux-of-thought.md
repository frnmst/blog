---
meta: true
software_name: the-flux-of-thought
archived: true
repositories:
  - label: 'canonical'
    url: 'https://software.franco.net.eu.org/frnmst-archives/the-flux-of-thought'
  - label: 'codeberg'
    url: 'https://codeberg.org/frnmst-archives/the-flux-of-thought'
  - label: 'framagit'
    url: 'https://framagit.org/frnmst-archives/the-flux-of-thought'
documentation:
  - label: 'software.franco.net.eu.org/frnmst-archives/the-flux-of-thought#the-flux-of-thought'
    url: 'https://software.franco.net.eu.org/frnmst-archives/the-flux-of-thought#the-flux-of-thought'
---
