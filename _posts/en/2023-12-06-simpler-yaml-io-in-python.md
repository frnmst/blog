---
title: ! 'Simpler YAML I/O in Python'
tags: [yaml, python, tutorial, video, youtube]
updated: 2024-02-01 18:07:00
related_links: ['https://www.youtube.com/watch?v=fFhJUU8eLbs']
description: ! "Reading, validating and loading from and to YAML files in Python"
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/fFhJUU8eLbs?si=0k-A0x9BqZTbJn9L" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Let's say you have a YAML file called `toot4feed/constants.yml` with this
content:

```yaml
templates:
  rootdir: 'templates'
  template_data:
    - type: 'atom'
      alias: 'atom_entry_filter_date_no_content'
      relative_path: 'entry/filter_date_no_content.txt'
    - type: 'atom'
      alias: 'atom_entry_base_no_content'
      relative_path: 'entry/base_no_content.txt'

cache:
  relative_directory: 'toot4feed'
  filename: 'data.json'
```

You can get and validate your input using this ugly and error prone method

```python
import yaml

config = yaml.load(open('toot4feed/constants.yml', 'r'), Loader=yaml.SafeLoader)

if ('templates' in config
   and 'cache' in config
   and isinstance(config['templates'], dict)
   and isinstance(config['templates']), dict)
   and 'rootdir' in config['templates']
   and isinstance(config['templates']['rootdir'], str)
   and 'template_data' in config['templates']
   and isinstance(config['templates']['template_data'], list)):
    for t in config['templates']['template_data']:
        if 'type' in t and isinstance(t['type'], str):
        # All other cases are not shown here...
            pass
        else:
            raise ValueError
   # You get the idea.
   pass
else:
    raise ValueError
```

You manually need to check the existence of every field, check its data type
and validate it.

Fortunately there is a cleaner way. You can neatly organize your content in
data classes (more about this later) and use a serialization library such as
Mashumaro to get your data.

{% include link_preview.html url="https://github.com/Fatal1ty/mashumaro" %}

Let's install the library first, including PyYAML:

```shell
pip install --user mashumaro[yaml]
```

{% include link_preview.html url="https://pypi.org/project/mashumaro/" %}

This is the first version of the program

```python
from dataclasses import dataclass
from mashumaro.mixins.yaml import DataClassYAMLMixin
import pathlib
import platformdirs

@dataclass
class FeedType:
    type: str
    alias: str
    relative_path: str


@dataclass
class Templates():
    rootdir: str
    template_data: list[FeedType]

@dataclass
class Cache:
    relative_directory: str
    filename: str

    def __post_init__(self):
        self.absolute_directory: str = platformdirs.user_cache_dir(self.relative_directory)
        self.pathlib_absolute_filename: pathlib.Path = pathlib.Path(self.absolute_directory, self.filename)

@dataclass
class Constants(DataClassYAMLMixin):
    templates: Templates
    cache: Cache


# Load the constants YAML file.
const: Constants = Constants.from_yaml(pathlib.Path('toot4feed/constants.yml').read_text())
```

What is going on here? Data classes are special type of classes suited to
hold data: you avoid lots of boilerplate code that you normally have to write
with standard classes. For example, by default, dataclasses provide the
`__init__` method, so there is no need to write it.

{% include link_preview.html url="https://docs.python.org/3/library/dataclasses.html" %}

All you have to do is to define attributes using Python typing annotations.

{% include link_preview.html url="https://docs.python.org/3/library/typing.html?highlight=annotations" %}

When you design these classes you should start with the inner-most component:
in this case we have the feed type.

```python
@dataclass
class FeedType:
    type: str
    alias: str
    relative_path: str
```

If we extract the first element from the `template_data` list and remove the
list notation we get:

```yaml
type: 'atom'
alias: 'atom_entry_filter_date_no_content'
relative_path: 'entry/filter_date_no_content.txt'
```

indeed, all these 3 variables are of type `str` and are required.

Now, let's go up a level. We get this:

```yaml
templates:
  rootdir: 'templates'
  template_data: []         # Empty list notation.
```

`rootdir` is a string. Easy. `template_data` is a list of something; a list
of `FeedType`.

```python
@dataclass
class Templates():
    rootdir: str
    template_data: list[FeedType]
```

`Cache` is a simple object composed by two strings only.

```python
@dataclass
class Cache:
    relative_directory: str
    filename: str
```

The `__post_init__` function sets two other variables used for locating the
cache file. They are placed here and not with the other variables for matters
of context.

```python
def __post_init__(self):
    self.absolute_directory: str = platformdirs.user_cache_dir(self.relative_directory)
    self.pathlib_absolute_filename: pathlib.Path = pathlib.Path(self.absolute_directory, self.filename)
```

In the root level of the YAML file we have 2 elements, `templates` and `cache`.
You just need to point the two variable types to the classes we previously
discussed.

```python
@dataclass
class Constants(DataClassYAMLMixin):
    templates: Templates
    cache: Cache
```

Inheriting from the `DataClassYAMLMixin` class enables us to use several
methods from the Mashumaro library, such as `from_yaml`. This method populates
the instance of the `Constants` object (`const`) with the file content. It
accepts a string, so we need to open the file and read it first using the
`pathlib.Path.read_text` method.

```python
const: Constants = Constants.from_yaml(pathlib.Path('toot4feed/constants.yml').read_text())
```

Now we can print the content of `const`:

```python
>>> import pprint
>>> pprint.pprint(const)
Constants(templates=Templates(rootdir='templates',
                              template_data=[FeedType(type='atom',
                                                      alias='atom_entry_filter_date_no_content',
                                                      relative_path='entry/filter_date_no_content.txt'),
                                             FeedType(type='atom',
                                                      alias='atom_entry_base_no_content',
                                                      relative_path='entry/base_no_content.txt')]),
          cache=Cache(relative_directory='toot4feed', filename='data.json'))
```

The two variables in the `__post_init__` method are not shown, but they are
there:

```python
>>> pprint.pprint(const.cache.absolute_directory)
'/home/vm/.cache/toot4feed'
>>> pprint.pprint(const.cache.pathlib_absolute_filename)
PosixPath('/home/vm/.cache/toot4feed/data.json')
```

The reason for this is that the dataclass does not add these variables to the
`__repr__` method, as explained in the
[documentation](https://docs.python.org/3/library/dataclasses.html#dataclasses.dataclass)

{% include link_preview.html url="https://docs.python.org/3/reference/datamodel.html#object.__repr__" %}

You can "fix" this behaviour by adding the fields in the class.

```diff
+from dataclasses import dataclass, field
-from dataclasses import dataclass
from mashumaro.mixins.yaml import DataClassYAMLMixin
import pathlib
import platformdirs
+import pprint

@dataclass
class FeedType:
    type: str
    alias: str
    relative_path: str


@dataclass
class Templates():
    rootdir: str
    template_data: list[FeedType]

@dataclass
class Cache:
    relative_directory: str
    filename: str
+   absolute_directory: str = field(init=False)
+   pathlib_absolute_filename: pathlib.Path = field(init=False)


    def __post_init__(self):
        self.absolute_directory = platformdirs.user_cache_dir(self.relative_directory)
        self.pathlib_absolute_filename = pathlib.Path(self.absolute_directory, self.filename)

@dataclass
class Constants(DataClassYAMLMixin):
    templates: Templates
    cache: Cache


# Load the constants YAML file.
const: Constants = Constants.from_yaml(pathlib.Path('toot4feed/constants.yml').read_text())
+pprint.pprint(const)
```

Which leads to:

```python
Constants(templates=Templates(rootdir='templates',
                              template_data=[FeedType(type='atom',
                                                      alias='atom_entry_filter_date_no_content',
                                                      relative_path='entry/filter_date_no_content.txt'),
                                             FeedType(type='atom',
                                                      alias='atom_entry_base_no_content',
                                                      relative_path='entry/base_no_content.txt')]),
          cache=Cache(relative_directory='toot4feed',
                      filename='data.json',
                      absolute_directory='/home/vm/.cache/toot4feed',
                      pathlib_absolute_filename=PosixPath('/home/vm/.cache/toot4feed/data.json')))
```

However, as I said before, I'm going to leave these two variables in the
`__post_init__` method only.

What happens if we set a variable with a different type in the YAML file, for
example by setting a `str` variable to an `int`, like this?

```yaml
templates:
  rootdir: 'templates'
  template_data:
    - type: 1
    # [ ... ]
```

If we run the program, we get this print

```
Constants(templates=Templates(rootdir='templates',
                              template_data=[FeedType(type=1,
                                                      alias='atom_entry_filter_date_no_content',
                                                      relative_path='entry/filter_date_no_content.txt'),
                                             FeedType(type='atom',
                                                      alias='atom_entry_base_no_content',
                                                      relative_path='entry/base_no_content.txt')]),
          cache=Cache(relative_directory='toot4feed', filename='data.json'))
```

Ok, it does not work as expected: the first `type` is now `1` which is an
`int` and not a `str`. But why? This issue explains the reason.

{% include link_preview.html url="https://github.com/Fatal1ty/mashumaro/issues/42" %}

What you need to do for some data types such as `str` is to enable validation,
because it is disabled by default for sake of performance. If we set it up
like this, it works:

```diff
from dataclasses import dataclass, field
from mashumaro.mixins.yaml import DataClassYAMLMixin
import pathlib
import platformdirs
import pprint
+
+def validate_str(value):
+    if not isinstance(value, str):
+        raise ValueError
+    return value
+
+
+class EnableStrValidation:
+     class Config:
+        serialization_strategy = {
+            str: {
+                "deserialize": validate_str,
+            },
+        }


@dataclass
class FeedType(EnableStrValidation):
+    type: str = field(metadata={"deserialize": validate_str})
+    alias: str = field(metadata={"deserialize": validate_str})
+    relative_path: str = field(metadata={"deserialize": validate_str})
-    type: str
-    alias: str
-    relative_path: str

@dataclass
class Templates(EnableStrValidation):
+   rootdir: str = field(metadata={"deserialize": validate_str})
-   rootdir: str
    template_data: list[FeedType]

@dataclass
class Cache(EnableStrValidation):
+    relative_directory: str = field(metadata={"deserialize": validate_str})
+    filename: str = field(metadata={"deserialize": validate_str})
-    relative_directory: str
-    filename: str

    def __post_init__(self):
        self.absolute_directory = platformdirs.user_cache_dir(self.relative_directory)
        self.pathlib_absolute_filename = pathlib.Path(self.absolute_directory, self.filename)


@dataclass
class Constants(DataClassYAMLMixin):
    templates: Templates
    cache: Cache


# Load the constants YAML file.
const: Constants = Constants.from_yaml(pathlib.Path('toot4feed/constants.yml').read_text())
pprint.pprint(const)
```

The first raised exception is this one:


```
Traceback (most recent call last):
  File "<string>", line 8, in __mashumaro_from_dict__
  File "toot4feed/toot4feed/constants.py", line 11, in validate_str
    raise ValueError
ValueError

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "<string>", line 11, in __mashumaro_from_dict__
  File "<string>", line 11, in <listcomp>
  File "<string>", line 10, in __mashumaro_from_dict__
mashumaro.exceptions.InvalidFieldValue: Field "type" of type str in FeedType has invalid value 1
```

Try setting a string in an integer field. In this case type validation works
out of the box.

Ok so now we can read, validate data and load it in variables. We can also
output it. Using the `to_yaml` method is all we need

```python
print(const.to_yaml())
```

```yaml
cache:
  absolute_directory: /home/vm/.cache/toot4feed
  filename: data.json
templates:
  rootdir: templates
  template_data:
  - alias: atom_entry_filter_date_no_content
    relative_path: entry/filter_date_no_content.txt
    type: atom
  - alias: atom_entry_base_no_content
    relative_path: entry/base_no_content.txt
    type: atom
```

Ta-da 🚀

Mashumaro supports other file types as well: JSON, TOML and MessagePack.

{% include link_preview.html url="https://github.com/Fatal1ty/mashumaro#supported-serialization-formats" %}
