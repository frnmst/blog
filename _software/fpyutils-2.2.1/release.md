---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpyutils
software_name_python_module: fpyutils
software_version: 2.2.1
software_version_raw: 000002.000002.000001
release_timestamp: 2022-10-27 13:58:53
is_on_pypi: true
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- The `int` variable `max_occurrencies` in the `fpyutils.filelines.get_line_matches`
  is now compared to an `int` instead of a `float`.
  If `max_occurrencies` is set to 0 it is now overwritten with `sys.maxsize`
  (2\*\*63-1 == 9223372036854775807 on a 64-bit machine) instead of `float(inf)`.
  This means that 2\*\*63-1 are the maximum occurrencies possible.

### Fixed

- Fixed function and variable annotations thanks to mypy git hook.
