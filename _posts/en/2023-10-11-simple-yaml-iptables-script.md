---
title: ! 'Simple YAML iptables script'
tags: [firewall, iptables, python, python3, video, youtube]
updated: 2023-10-11 17:00:00
description: ! 'Manage TCP and UDP ports using iptables rules with a YAML file and an IP whitelist system'
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/_C70kKWQO_I?si=gNeSTV_oo_RtsOCn" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

A script that acts as a frontend for iptables rules. You can easily manage
ports using IP address whitelists, for example by geographical location.

All the configuration is done with a YAML file.
