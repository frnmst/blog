---
title: ! 'Restore a Patriot Burst with the SATAFIRM S11 error'
tags: [satafirm-s11, ssd, ssd-controller, firmware-error, damaged-firmware]
updated: 2024-11-18 20:45:00
description: ! 'I was able to restore a Patriot Burst with the SATAFIRM S11 error (damaged firmware) at the cost of its data'
lang: 'en'
images:
  - id: 'post_firmware_flash'
    filename: 'post_firmware_flash.png'
    alt: 'Result after flashing the firmware on a Patriot Burst'
    caption: ! 'Re-flashed the firmware on a Partiot Burst SSD'
    width: '300px'
  - id: 'bios_disk_mode'
    filename: 'bios_disk_mode.jpg'
    alt: 'Computer screen showing BIOS disk options'
    caption: ! 'Remember to set the right mode!'
    width: '300px'
  - id: 'translated_readme'
    filename: 'translated_readme.png'
    alt: 'Translated readme section from Russian to English'
    caption: ! 'Part of the translated readme'
    width: '300px'
---
{% include image.html id="post_firmware_flash" %}

## Table of contents

<!--TOC-->

- [Table of contents](#table-of-contents)
- [Introduction](#introduction)
- [What happened](#what-happened)
- [Steps to recover the SSD to a usable state](#steps-to-recover-the-ssd-to-a-usable-state)
  - [Tools](#tools)
  - [Disk mode](#disk-mode)
  - [Getting the right firmware file](#getting-the-right-firmware-file)
  - [Building the flasher](#building-the-flasher)
  - [Running](#running)
- [Conclusion](#conclusion)

<!--TOC-->

**Important update: even though this procedure worked, after a write operation
which can be considered a stress test, the drive experienced the same problem.
For this reason it was disposed of.**

## Introduction

<!--excerpt_start-->I was able to restore a
[Patriot Burst](https://www.patriotmemory.com/products/burst-sata-iii-retired-2-5-ssd-box-package)
240 GB SSD which had its firmware damaged.<!--excerpt_end--> **If you try this
you'll lose all the data but at least you can use the disk again.**
Unfortunately you'll also have the S.M.A.R.T. data cleared, as good as new:

```
SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAG     VALUE WORST THRESH TYPE      UPDATED  WHEN_FAILED RAW_VALUE
1 Raw_Read_Error_Rate       0x000b   100   100   050    Pre-fail  Always       -       0
9 Power_On_Hours            0x0012   100   100   000    Old_age   Always       -       0
12 Power_Cycle_Count        0x0012   100   100   000    Old_age   Always       -       1
168 SATA_Phy_Error_Count    0x0012   100   100   000    Old_age   Always       -       0
170 Bad_Blk_Ct_Erl/Lat      0x0003   100   100   010    Pre-fail  Always       -       0/301
173 MaxAvgErase_Ct          0x0012   100   100   000    Old_age   Always       -       1
192 Unsafe_Shutdown_Count   0x0012   100   100   000    Old_age   Always       -       0
194 Temperature_Celsius     0x0023   067   067   000    Pre-fail  Always       -       33 (Min/Max 33/33)
218 CRC_Error_Count         0x000b   100   100   050    Pre-fail  Always       -       0
231 SSD_Life_Left           0x0013   100   100   000    Pre-fail  Always       -       100
241 Lifetime_Writes_GiB     0x0012   100   100   000    Old_age   Always       -       0
```

Reading various posts online the cause of this firmware corruption is unclear
to me: is it a wear issue or is quality of the controller (PS3111-S11 or
PS3111-S11T) low? Was there an electrical problem? No idea.

{% include link_preview.html url="https://forum.hddguru.com/viewtopic.php?f=10&t=39161&mobile=desktop" %}

I found
[this spreadsheet](https://docs.google.com/spreadsheets/d/1B27_j9NDPU3cNlj2HKcrfpJKHkOf-Oi1DbuuQva2gT4/edit#gid=0)
that lists lots of different SSDs. You can sort the list by controller so
you can see which ones might be affected by this problem.

## What happened

Anyway, what happened to me is that the drive suddenly vanished from the system
devices while the computer was up and running normally. This and other related
errors popped up continuously on `dmesg`:

```
BTRFS warning (device sdb2): lost page write due to IO error on /dev/sde2
```

The disk was part of a Btrfs RAID 1 array containing Git repositories served
by Gitea. I still had the data on the other twin disk and backups, so I wasn't
worried.

What I did next was to turn off the server and try to check if the disk
was working: at the time I didn't know what happened. I thought that either
the power or the SATA cable went loose. The disk was now visible but I was
getting errors. I noticed the disk was recognized as SATAFIRM S11 and not as
Patriot Burst by `smartctl`

```
Device Model:     SATAFIRM   S11
```

What I did now was to *fix* the filesystem using some of the tools provided
by Btrfs. The only one that worked is `btrfs scrub`. All the errors were now
fixed. In-fact, when I tried to rsync before the scrub I got some I/O errors,
but not after it.

To avoid more problems I then created a new clean Btrfs RAID 1 array between
the second disk and a spare one. This left the *broken* SSD out of the server.

## Steps to recover the SSD to a usable state

### Tools

Recovering the SSD was not that hard once you get the
[three tools](https://drive.google.com/drive/folders/1xuHNNRUeO-ls19I2bQ96SfpodolndMVL):

- the flash identifier program
- the firmware flasher
- the binary files (firmwares)

You can also download two of them [here](http://vlo.name:3000/ssdtool/). Go to
the *Phison utility* section and get the
[Phison flash id (for S5/S8/S9/S10/S11)](http://vlo.name:3000/tmph/phison_flash_id.rar)
and
[Phison S11 firmware flasher v2](http://vlo.name:3000/tmph/s11-flasher.rar)
The firmware files are marked with
[Phison PS3111 Firmware [SBFM]](https://www.usbdev.ru/?wpfb_dl=6583).
The explanation is on
[this page](https://www.usbdev.ru/files/phison/ps3111fw/).

All of these are RAR files so you can use the `unrar` utility on GNU/Linux
to extract them.

### Disk mode

Unfortunately you need Windows (sigh) to do the recovery. Windows 10 worked
fine, as long as you plug the disk via SATA and set the disk mode to AHCI.
At first, in-fact, it didn't work on my laptop. I plugged it in via USB: not
recognized. I used the internal SATA slot: not recognized. I had RAID mode set
in the BIOS.

{% include image.html id="bios_disk_mode" %}

If you open one of the readmes, which are mostly is Russian,
you'll read this (translated):

> The drive must be connected to a SATA controller (connection via firewire is not supported); it supports standard IDE and AHCI drivers from Microsoft and Intel RST. Operation with other drivers has not been tested. It probably won't work with controller drivers from the "SCSI and RAID controllers" section (except RST versions starting from 11.7).
>
> It is also possible to work through some usb-sata bridges, for example asmedia asm105x/115x, jmicron jm[s]20329/539/578, initio inic1608 and some others.

This means that it's better to plug the SSD via SATA and set AHCI mode in the
BIOS.

{% include image.html id="translated_readme" %}

### Getting the right firmware file

You need to identify the specific firmware you need through the
`phison_flash_id.exe` program. The value of the `S11fw` line identifies the
specific firmware to use. I got `SBFM61.3, 2019Jul26`. Get the correct file
from the `Firmware PS3111` directory (the binary files mentioned before).
There was more that one file for the same firmware in my case, so I got the
most recent one. The date of the file is in the format `ddmmyyyy`, right after
the underscore character. This schema is used:

```
${FIRMWARE_ID}_$(date +%d)$(date +%m)$(date +%Y).BIN
```

In my case I chose `SBFM61.3_11032021.BIN`.

You then need to rename `SBFM61.3_11032021.BIN` to `fw.BIN` and copy it in the
firmware flasher directory.

### Building the flasher

Now you can build the flasher program. Execute the `s11-flasher2-toshiba.cmd`
script for the Patriot Burst SSD. You'll see a new executable called
`fw.exe` in the directory.

### Running

After you run it you must click on `Upgrade Firmware` button and wait a few
moments. Then it's done.

## Conclusion

There are numerous videos on YouTube that explain all these steps. Also, when
you *unrar* the tools you'll find their readmes there.

{% include link_preview.html url="https://www.youtube.com/watch?v=OoDZlJjTc_I" %}
{% include link_preview.html url="https://www.youtube.com/watch?v=BQ-QRkO9VOI" %}

Lesson learned: avoid these kinds of SSDs and check if the controller from
the specs is reliable. Am I going to use this SSD for production? I don't think
so: it will be a spare.

🚀
