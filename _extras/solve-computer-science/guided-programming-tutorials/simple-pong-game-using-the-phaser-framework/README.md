Serve this directory using the python3 HTTP server for example,
like this

```shell
python3 -m http.server
```

Connect to [http://127.0.0.1/pong.html](http://127.0.0.1/pong.html)
