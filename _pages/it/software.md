---
layout: page
title: Software
permalink: /it/software/
description: ! 'Fonte ufficiale del software pubblicato da Franco Masotti'
lang: 'it'
---
{%- include software/software_toc.liquid %}
<div markdown="1">
{% include software/it/software_introduction.liquid %}
{% include software/it/software_authenticity.liquid %}
</div>
{% include software/software_content.liquid %}
