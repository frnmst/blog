// SPDX-FileCopyrightText: 2023 Franco Masotti
//
// SPDX-License-Identifier: MIT
#include <stdio.h>
#include <string.h>

int main(void)
{
    char array[100] = "ab c def g hi";
    char reversed[100];

    int i, j = 0, length = 0;

    // Counts the terminating character
    // but starts from 0.
    // For this reason the result is the
    // same as strlen.
    while (array[length] != '\0')
        length++;
    printf("length = %d", length);
    printf("strlen = %zu", strlen(array));

    for (i = length - 1; i >= 0; i--, j++)
        reversed[j] = array[i];

    reversed[length] = '\0';

    printf("reversed = %s\n", reversed);

    return 0;
}
