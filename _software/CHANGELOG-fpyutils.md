---
layout: changelog
enable_markdown: true
software_name: fpyutils
title: fpyutils changelog
excerpt: none
lang: 'en'
---

## Plan for version 5.0.0

### Changed

- Remove the `fpyutils.notify` module.
  Use [apprise](https://github.com/caronc/apprise) for your applications instead.

## Plan for version 4.0.2

### Fixed

- [__✓__ 0005010](https://software.franco.net.eu.org/frnmst/fpyutils/commit/0005010)

  Improve documentation for the API so it is more readable: use `autosummary`
  and `automodule`.
- [__✓__ 4e991264](https://software.franco.net.eu.org/frnmst/fpyutils/commit/4e991264)

  Add unicode error detection in `fpyutils.filelines.get_line_matches`. Update
  unit tests accordingly.
- [__✓__ 31e703b](https://software.franco.net.eu.org/frnmst/fpyutils/commit/31e703b)

  Move Makefile to a separate project.
- [__✓__ 31e703b](https://software.franco.net.eu.org/frnmst/fpyutils/commit/31e703b)
  [__✓__ f87a388](https://software.franco.net.eu.org/frnmst/fpyutils/commit/f87a388)

  Add checksums to `./requirements-freeze.txt` file.
- [__✓__ f87a388](https://software.franco.net.eu.org/frnmst/fpyutils/commit/f87a388)

  Improve test coverage
- [__✓__ f87a388](https://software.franco.net.eu.org/frnmst/fpyutils/commit/f87a388)

  Cleanup code based on test coverage output

### Added

- [__✓__ f87a388](https://software.franco.net.eu.org/frnmst/fpyutils/commit/f87a388)
  [__✓__ f78de2d](https://software.franco.net.eu.org/frnmst/fpyutils/commit/f78de2d)

  Add GitHub unit test action

- [__✓__ 4e991264](https://software.franco.net.eu.org/frnmst/fpyutils/commit/4e991264)

  Add unicode error detection in `fpyutils.filelines.get_line_matches`. Update
  unit tests accordingly.
- [__✓__ 4e991264](https://software.franco.net.eu.org/frnmst/fpyutils/commit/4e991264)

  Add pip-audit pre-commit hook.
