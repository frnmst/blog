---
meta: true
software_name: automated-tasks
archived: true
repositories:
  - label: 'canonical'
    url: 'https://software.franco.net.eu.org/frnmst-archives/automated-tasks'
  - label: 'codeberg'
    url: 'https://codeberg.org/frnmst-archives/automated-tasks'
  - label: 'framagit'
    url: 'https://framagit.org/frnmst-archives/automated-tasks'
documentation:
  - label: 'docs.franco.net.eu.org/automated-tasks'
    url: 'https://docs.franco.net.eu.org/automated-tasks'
  - label: 'changelog'
    url:
---
