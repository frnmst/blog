---
title: ! 'Scraping essentials: BeautifulSoup - part 4'
tags: [scraping, beautifulsoup, amazon, mechanicalsoup, python, tutorial, amazon-scraper]
updated: 2024-01-15 18:00:00
description: ! "Let's see the full code of the Amazon product scraper in Python"
lang: 'en'
---
<!--excerpt_start-->Here is the full code of the Amazon scraper using
MechanicalSoup<!--excerpt_end-->. Parts
[1]({%- include post/post_url_iso639.liquid post_url="2024-01-11-scraping-essentials-beautifulsoup" -%}{{ post_url_iso639 }} "{{ post_title_iso639 }}"),
[2]({%- include post/post_url_iso639.liquid post_url="2024-01-12-scraping-essentials-beautifulsoup-part-2" -%}{{ post_url_iso639 }} "{{ post_title_iso639 }}")
and
[3]({%- include post/post_url_iso639.liquid post_url="2024-01-13-scraping-essentials-beautifulsoup-part-3" -%}{{ post_url_iso639 }} "{{ post_title_iso639 }}")
explain the following code.

## Dependencies

Before running the script you have to install the
[MechanicalSoup](https://pypi.org/project/MechanicalSoup/ "MechanicalSoup - PyPI")
and
[Mashumaro](https://pypi.org/project/mashumaro/ "mashumaro - PyPI") packages
from [PyPI](https://pypi.org/ "PyPI · The Python Package Index"). Use a
virtual environment.

## Code

Here is the full code

```python
from dataclasses import dataclass
import decimal
import re
from urllib.parse import urlparse, urljoin
import time
from mashumaro.mixins.json import DataClassJSONMixin
import sys

@dataclass
class Product:
    url: str
    asin_raw: str
    asin: str
    title_raw: str
    title: str
    price_raw: str
    price: decimal.Decimal
    currency_symbol_raw: str
    currency_symbol: str
    availabile_raw: str
    availabile: bool
    rating_raw: str
    rating: float
    reviews_raw: str
    reviews: int


@dataclass
class Products:
    products: list[Product]


@dataclass
class ProductSearch(DataClassJSONMixin):
    products: Products
    url: str


def normalize_price(price_raw: str) -> decimal.Decimal:
    r"""Accepts a stripped string containing numbers, ',' or '.'."""

    price_filtered: list = []
    for c in price_raw:
        if re.match(r'([0-9]|,)', c):
            if c == ',':
                c = '.'
            price_filtered.append(c)

    return decimal.Decimal(''.join(price_filtered))


def normalize_float(float_raw: str) -> float:
    r"""Accepts a stripped string containing numbers, ',' or '.'."""

    float_filtered: list = []
    for c in float_raw:
        if re.match(r'([0-9]|,)', c):
            if c == ',':
                c = '.'
            float_filtered.append(c)

    return float(''.join(float_filtered))


def normalize_int(int_raw: str) -> int:
    int_filtered: list = []

    for c in int_raw:
        if re.match(r'[0-9]', c):
            int_filtered.append(c)

    return int(''.join(int_filtered))


def get_product(url: str, browser_user_agent: str) -> Product:
    browser = mechanicalsoup.StatefulBrowser(
            user_agent=browser_user_agent,
    )
    browser.open(url)

    try:
        table_headers = browser.page.select('th.a-color-secondary.a-size-base.prodDetSectionEntry')
        asin_raw: str = '<undefined>'
        for th in table_headers:
            if th.text.strip() == 'ASIN':
                # ASIN value is in the column (td) of the ASIN table header (th).
                asin_raw = th.find_next_sibling().text.strip()
        asin: str = asin_raw

        product_title_raw: str = browser.page.find('span', attrs={"id":'productTitle'}).text.strip()
        product_title: str = product_title_raw

        product_price_raw: str = browser.page.find('span', class_='a-offscreen').text.strip()
        product_price: decimal.Decimal = normalize_price(product_price_raw)

        product_currency_symbol_raw: str = browser.page.find('span', class_='a-price-symbol').text.strip()
        product_currency_symbol: str = product_currency_symbol_raw

        product_availabile_raw: str = browser.page.find('div', class_='olp-link-widget')
        if product_availabile_raw is not None:
            product_availabile_raw = product_availabile_raw.text.strip()
        product_availabile: bool = bool(product_availabile_raw)

        product_reviews_raw: str = browser.page.find('span', attrs={'id': 'acrCustomerReviewText'}).text.strip()
        product_reviews: int = normalize_int(product_reviews_raw)

        # Recursive find. Find a div then a span inside it exactly with the 2 classes: `a-size-base` and `a-color-base`
        product_rating_raw: str = browser.page.find('div', attrs={'id': 'averageCustomerReviews'}).find('span', class_='a-size-base a-color-base').text.strip()
        product_rating: float = normalize_float(product_rating_raw)
    except AttributeError:
        asin_raw = None
        asin = None
        product_title_raw = None
        product_title = None
        product_price_raw = None
        product_price = None
        product_currency_symbol_raw = None
        product_currency_symbol = None
        product_availabile_raw = None
        product_availabile = None
        product_reviews_raw = None
        product_reviews = None
        product_rating_raw = None
        product_rating = None

    return Product(
            url=url,
            asin_raw=asin_raw,
            asin=asin,
            title_raw=product_title_raw,
            title=product_title,
            price_raw=product_price_raw,
            price=product_price,
            currency_symbol_raw=product_currency_symbol_raw,
            currency_symbol=product_currency_symbol,
            availabile_raw=product_availabile_raw,
            availabile=product_availabile,
            reviews_raw=product_reviews_raw,
            reviews=product_reviews,
            rating_raw=product_rating_raw,
            rating=product_rating,
        )

def get_search_results(url: str, browser_user_agent: str) -> ProductSearch:
    browser = mechanicalsoup.StatefulBrowser(
            user_agent=browser_user_agent,
    )
    browser.open(url)

    all_products: list[Product] = []
    image_divs = browser.page.select('div.s-product-image-container.puis-image-overlay-grey')
    for divs in image_divs:
        link = divs.find('a', class_='a-link-normal s-no-outline')
        if link:
            product_url: str = link.get('href')
            # Products don't start with https:// and /sspa/
            if not (product_url.startswith('https://') or product_url.startswith('/sspa/')):
                full_url: str = urljoin(url, product_url)
                all_products.append(get_product(full_url, browser_user_agent))
                time.sleep(5)

    return ProductSearch(products=Products(products=all_products), url=url)


def preform_search(url: str, browser_user_agent: str, search_query: str) -> ProductSearch:
    browser = mechanicalsoup.StatefulBrowser(
            user_agent=browser_user_agent,
    )
    browser.open(url)

    done: bool = False
    while not done:
        try:
            search_form = browser.select_form('form[id="nav-search-bar-form"]')
            done = True
        except mechanicalsoup.utils.LinkNotFoundError as e:
            print(e, file=sys.stderr)
            print('retrying ...', file=sys.stderr)
            time.sleep(60)
    browser["field-keywords"] = search_query
    response = browser.submit_selected()
    return get_search_results(response.url, browser_user_agent)


if __name__ == '__main__':
    URL='https://www.amazon.it'
    USER_AGENT = 'Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Ubuntu/10.10 Chromium/10.0.648.0 Chrome/10.0.648.0 Safari/534.16'
    SEARCH_QUERY = 'ssd'

    s = preform_search(URL, USER_AGENT, SEARCH_QUERY)
    print(s.to_json())
```

## Conclusion

In the next post we'll see how to improve this script by using data from the
search results page 🚀
