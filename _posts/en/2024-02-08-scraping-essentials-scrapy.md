---
title: ! 'Scraping GitHub profiles with Scrapy'
tags: [scraping, scrapy, github, email, tutorial, python, scrape]
updated: 2024-02-08 13:30:00
description: ! 'Scrape profile data from GitHub, including the email, using Python Scrapy'
lang: 'en'
images:
  - id: 'profile_data_base'
    filename: 'profile_data_base.png'
    alt: ! 'A cut section of a screen capture showing some of Linus Torvalds GitHub profile information'
    caption: ! "Linus's profile information"
    width: '300px'
  - id: 'hibp'
    filename: 'hibp.png'
    alt: ! 'Have I Been Pwned (HIBP) website warning of Geekedin'
    caption: ! 'Email pwned!!!'
    width: '300px'
  - id: 'commit_page'
    filename: 'commit_page.png'
    alt: ! 'A Git patch file page from a GitHub webpage'
    caption: ! 'Git patch file, HTML version'
    width: '300px'
  - id: 'commit_page_patch_file'
    filename: 'commit_page_patch_file.png'
    alt: ! 'A Git patch file cut screenshot from a web browser showing the `From` field bordered in red'
    caption: ! 'Plaintext Git patch file from a web browser'
    width: '300px'
  - id: 'git_patch_file'
    filename: 'git_patch_file.png'
    alt: ! 'A screen capture of a Git patch file from the Linux repository showing the `From` field, which includes name and email'
    caption: ! 'Plaintext Git patch file in nvim'
    width: '300px'
  - id: 'github_profile_developer_options'
    filename: 'github_profile_developer_options.png'
    alt: ! 'Browser screenshot of the developer options showing the `uid` field'
    caption: ! 'The `uid` field is midden in one of the `meta` tags'
    width: '300px'
  - id: 'github_profile_details_developer_options'
    filename: 'github_profile_details_developer_options.png'
    alt: ! 'Browser screenshot of the developer options showing the parent element of several profile fields'
    caption: ! "The parent `div` HTML element with several fields we're going to use inside it"
    width: '300px'
  - id: 'github_profile_developer_options_social'
    filename: 'github_profile_developer_options_social.png'
    alt: ! 'Browser screenshot of one of the "social" list elements'
    caption: ! 'All social list elements are in an `ul` with the `itemprop="social"` attribute'
    width: '300px'
  - id: 'get_first_repo'
    filename: 'get_first_repo.png'
    alt: ! 'Browser screenshot of the developer options showing the link to the first repository'
    caption: ! 'Select the first repository of the list'
    width: '300px'
  - id: 'latest_commit_button'
    filename: 'latest_commit_button.png'
    alt: ! 'Browser screenshot of the developer options showing the latest commit of the repository'
    caption: ! 'Get the link to the latest commit'
    width: '300px'
---
## Table of contents

<!--TOC-->

- [Table of contents](#table-of-contents)
- [Introduction](#introduction)
- [User profile](#user-profile)
  - [User fields](#user-fields)
- [Getting the email](#getting-the-email)
  - [History](#history)
  - [Email must be set up in Git](#email-must-be-set-up-in-git)
  - [First method](#first-method)
  - [Second method](#second-method)
- [Scrapy vs BeautifulSoup](#scrapy-vs-beautifulsoup)
- [Setting up the parser](#setting-up-the-parser)
- [GitHub profile page analysis](#github-profile-page-analysis)
  - [User id](#user-id)
  - [Other variables](#other-variables)
  - [The social variable](#the-social-variable)
- [Getting the list of repositories](#getting-the-list-of-repositories)
  - [Calling the `parse_first_repo` method](#calling-the-parse_first_repo-method)
    - [Why use a dict of dict in the `yield`?](#why-use-a-dict-of-dict-in-the-yield)
  - [The `parse_first_repo` method](#the-parse_first_repo-method)
  - [The `parse_commits` method](#the-parse_commits-method)
    - [Passing data from `parse_first_repo` to `parse_commits`](#passing-data-from-parse_first_repo-to-parse_commits)
    - [Getting the latest commit](#getting-the-latest-commit)
- [Pipelines](#pipelines)
  - [Cleaning the data](#cleaning-the-data)
  - [Getting the email (finally)](#getting-the-email-finally)
  - [Cleanup](#cleanup)
  - [Enabling the pipeline](#enabling-the-pipeline)
- [Testing](#testing)
- [Bulk scraping](#bulk-scraping)
  - [GitHub REST API](#github-rest-api)
  - [Fixing the spider code](#fixing-the-spider-code)
  - [Avoid being blocked](#avoid-being-blocked)
- [Conclusion](#conclusion)

<!--TOC-->

## Introduction

<!--excerpt_start-->[GitHub](https://github.com "GitHub") offers an extensive
REST API which lets you gather lots of information about its users and their
interactions. However<!--excerpt_end-->, just like any other website, it is
possible to manually scrape data to obtain some "hidden" data. Today we'll see
how to scrape data using Scrapy.

{% include link_preview.html url="https://scrapy.org/" %}

If you want to learn more about scraping you can read a
[previous post]({%- include post/post_url_iso639.liquid post_url="2024-01-15-scraping-essentials-beautifulsoup-part-5" -%}{{ post_url_iso639 }} "{{ post_title_iso639 }}")
where I talk about using BeautifulSoup to get product information.

## User profile

First of all we'll see the GitHub profile of
[Linus Torvalds](https://github.com/torvalds "torvalds (Linus Torvalds) · GitHub").
Some of the data we need is on the left column of the page, right below the
avatar.

{% include image.html id="profile_data_base" %}

### User fields

Linus's profile only contains a few fields. In-fact, the only required fields
are the uid and login name (username). Our scraper, however, may handle
more information. Some of the possible fields you can scrape are these
ones:

| Field name | Description | Required |
|------------|-------------|----------|
| uid        | the user id, unique to each profile and invisible in the rendered HTML page | ✓ |
| login name | the user name, a.k.a. login name in GitHub terms | ✓ |
| name       | the displayed name | ✘ |
| bio        | some generic text | ✘ |
| organization | the company the user claims is working for | ✘ |
| local time   | the user's time compared to the crawler's | ✘ |
| website url  | a generic URL | ✘ |
| social       | a list of URLs. Can be any kind of URL, not necessarly a social network | ✘ |
| email        | the email of the user. **Note: emails are not directly accessible in the profile page!** | ✘ |

## Getting the email

Since the email is not usually part of the profile data we need other ways
to get it.

### History

A few years ago personal emails were commonly displayed on GitHub profile
pages. There was also a *data leak* from a company that had scraped the data,
as
[reported by this post](https://www.troyhunt.com/8-million-github-profiles-were-leaked-from-geekedins-mongodb-heres-how-to-see-yours/ "Troy Hunt: 8 million GitHub profiles were leaked from GeekedIn's MongoDB - here's how to see yours").
One of my emails was there so that's why I remember this. Anyway, today you
don't see many GitHub profiles with the emails visible.

{% include image.html id="hibp" %}

### Email must be set up in Git

Besides getting the email from the profile if available, there are other ways
as well. If you look it up, the most popular method is to get it from the
commits. If you known basic Git, to make it work you need to set up an email
and a username. The email is the field used to associate local commits to
GitHub (or any other Git forge) platform(s).

### First method

Commits are stored in repositories which are stored in GitHub. A possible
way to get the email is to clone the repository and then use the Git tools
to get the data. The problem with this is disk space and time used for the
cloning.

Anyway, once you have the repository there is no direct way to associate the
email in the commits to the GitHub profile owner except using the name field.
Since Linux is a BIG repository, you can limit the Git clone to the latest
10 commits and hope that the email of the user you are looking for is in
that commit interval.

```shell
git clone --depth=10 https://github.com/torvalds/linux.git
git log --pretty=format:"%an,%ae" | sort -u | grep -v '^gpg: '
```

This command extracts the username and emails from the repositories' commits,
giving you this result:

```
Andi Shyti,andi.shyti@kernel.org
Andrey Albershteyn,aalbersh@redhat.com
Carlos Llamas,cmllamas@google.com
Christian A. Ehrhardt,lk@c--e.de
Darrick J. Wong,djwong@kernel.org
Dmitry Baryshkov,dmitry.baryshkov@linaro.org
Ekansh Gupta,quic_ekangupt@quicinc.com
Greg Kroah-Hartman,gregkh@linuxfoundation.org
Hugo Villeneuve,hvilleneuve@dimonoff.com
Ira Weiny,ira.weiny@intel.com
JackBB Wu,wojackbb@gmail.com
Kemeng Shi,shikemeng@huaweicloud.com
Linus Torvalds,torvalds@linux-foundation.org
merged tag 'char-misc-6.8-rc3'
merged tag 'cxl-fixes-6.8-rc2'
merged tag 'dmaengine-fix-6.8'
merged tag 'for-linus-6.8-rc3'
merged tag 'i2c-for-6.8-rc3'
merged tag 'i2c-host-fixes-6.8-rc3'
merged tag 'mips-fixes_6.8_1'
merged tag 'tty-6.8-rc3'
merged tag 'usb-6.8-rc3'
merged tag 'usb-serial-6.8-rc3'
merged tag 'v6.8-rc3-smb-client-fixes'
merged tag 'xfs-6.8-fixes-2'
Paulo Alcantara,pc@manguebit.com
Puliang Lu,puliang.lu@fibocom.com
Quanquan Cao,caoqq@fujitsu.com
Shyam Prasad N,sprasad@microsoft.com
tag locking_urgent_for_v6.8_rc2 names a non-parent e626cb02ee8399fd42c415e542d031d185783903
tag phy-fixes-6.8 names a non-parent 7104ba0f1958adb250319e68a15eff89ec4fd36d
Vinod Koul,vkoul@kernel.org
Will Deacon,will@kernel.org
Wolfram Sang,wsa+renesas@sang-engineering.com
Xi Ruoyao,xry111@xry111.site
Zhang Yi,yi.zhang@huawei.com
```

Now try setting up a search function to find `[lL]inus [tT]orvalds` or
something. The name field in the GitHub profile might be totally arbitrary,
just like the name configured for Git and they can be different one another.
This method is thus useless in repositories where multiple authors contributed
to the code.

### Second method

{% include image.html id="commit_page" %}

If you open the commit page of any repository on GitHub the corresponding
URL structure is this one.

```
https://github.com/${login}/${repo}/commit/${commit_checksum}
```

For example you can look at this page
[this page](https://github.com/torvalds/linux/commit/615d300648869c774bd1fe54b4627bb0c20faed4 "Merge tag 'trace-v6.8-rc1' of git://git.kernel.org/pub/scm/linux/kern… · torvalds/linux@615d300 · GitHub").

GitHub provides a different view of the commit page. If you
[append `.patch`](https://github.com/torvalds/linux/commit/615d300648869c774bd1fe54b4627bb0c20faed4.patch)
to the previous URL you'll get a plaintext file, not an HTML page, containing
the diff patch file.

{% include image.html id="commit_page_patch_file" %}

Every commit in the form of a patch file must have some basic data associated
to it: commit checksum, user, email, date, subject, etc...
This is because one can apply the patch later to his local copy of the Git
repository.

If you have a Git repository with at least one commit you can try generated
a patch file yourself:

```shell
git format-patch -1 HEAD
```

{% include image.html id="git_patch_file" %}

Ok, so we can see an email on the second line. However that might not always be
Linus's mail because of merge commits for example. Nonetheless, this should be
a good enough starting point for most cases.

Keep this method in mind for later as we're going to integrate it in the Scrapy
code.

## Scrapy vs BeautifulSoup

Scrapy works differently from BeautifulSoup: in-fact, Scrapy provides a way
to organize your code in different steps. We're going to use these two:

1. set up the spider: here you do the normal scraping and just that (no extra
   processing)
2. write the pipelines to clean your results: pipelines can be used for
   cleaning, adding data, saving to a database, etc...

First of all you need to set up a new
[Scrapy project](https://docs.scrapy.org/en/latest/intro/tutorial.html#creating-a-project "Scrapy Tutorial - Creating a project")

## Setting up the parser

Let's create a spider. You can use this command:

```shell
scrapy genspider basic
```

If you open the `github_public_data/spiders/basic.py` file you'll find some
of the following content already generated.

```python
import scrapy

class BasicSpider(scrapy.Spider):
    name = "basic"
    allowed_domains = ["github.com"]
    start_urls = ["https://github.com/torvalds"]

    # Ignore security settings: this is just a POC.
    custom_settings = {
        'HTTPERROR_ALLOW_ALL': True
    }

    def parse(self, response):
        pass
```

The `parse` method is going to return the data we scraped to other components
of the Scrapy software.

## GitHub profile page analysis

Now let's head to the profile page and study the HTML code.

Getting results is probabily easier than in BeautifulSoup. The page is saved
in the `response` variable of the `parse` method, and, by using its `css`
method one can easily filter the data using CSS tags, as explained below.

### User id

To get the user id (`uid`) one can do this, for example:

```python
# [ ... ]
def parse(self, response):
    uid = response.css('meta[name="octolytics-dimension-user_id"]::attr(content)').get()
    # [ ... ]
```

{% include image.html id="github_profile_developer_options" %}

The user id variable is buried in the HTML code but you can filter various
`meta` tags and get the value of the `content` attribute.

When using the `reponse.css.get()` method, a string is returned.

### Other variables

All the other user variables are visible on the page so you can easily select
them with the mouse.

Some variables are saved in an HTML list (`ul`). You can get cleaner code by selecting
a parent element first and then searching for your data below it in the HTML
tree. In this case the parent element variable is called `details`.

```python
# All details are defined in an ul below a div.
details = response.css('div.js-profile-editable-area')
# [ ... ]
location = details.css('li[itemProp="homeLocation"]').css('span.p-label::text').get()
local_time = details.css('li[itemProp="localTime"]').css('span.p-label').css('profile-timezone::text').get()
```

{% include image.html id="github_profile_details_developer_options" %}

### The social variable

Social links URLs are still stored in the same list as some of the other
elements. The difference here is that there might be more than one social
URL in the profile, so instead of `get()` you must use `getall()`.

```python
social = details.css('li[itemProp="social"]').css('a::attr(href)').getall()
```

{% include image.html id="github_profile_developer_options_social" %}

## Getting the list of repositories

As you know, the list of repositories of a user is present in a separate view.
Usually they are sorted by activity, with the most recent one on the top,
being the first element of a list.

### Calling the `parse_first_repo` method

Before extracting this new data we need to manage the one we've just extracted.
Save all your data in a new object:

```python
data = {
    'uid': uid,
    'login': login,
    'name': name,
    'bio': bio,
    'org': org,
    'location': location,
    'local_time': local_time,
    'url': url,
    'social': social,
}
```

Since Scrapy works asyncronously you need to use `yield` statements instead of
`return`, so before closing the original `parse` function we need to use this
statement:

```python
login = response.css('span.p-nickname.vcard-username.d-block::text').get().strip()
# [ ... ]
repositories_tab = 'https://github.com/' + login.strip() + '?tab=repositories'
yield response.follow(repositories_tab, callback=self.parse_first_repo, cb_kwargs={'args': data})
```

The first field is the repositories page URL that will be opened by
`parse_first_repo` and saved in its `response` variable.

The second argument is the name of the new parser you need to call to extract
the commits data.

#### Why use a dict of dict in the `yield`?

To have a single *return* object, you need to pass the previous data inside
a dictionary:

```python
..., cb_kwargs={'args': data})
```

The reason for this is that if you passed `data` directly, you'd need to
list each element which is part of the `data` dictionary in the
`parse_first_repo` method prototype. So, instead of having some thing clean
like this:

```python
def parse_first_repo(self, response, args):
```

you'd have to do:

```python
def parse_first_repo(self, response, uid, login, name, bio, org ... etc ...):
```

This is a nightmare if you need to change your code.

### The `parse_first_repo` method

Now you must create a new callback method (a new parser) that gets the list
of commits by author from the first repository. Remember that our final
objective is to get the email since we already have the rest of the data.

```python
def parse_first_repo(self, response, args):
    repo_url: str = 'https://github.com' + response.css('ul[data-filterable-for="your-repos-filter"]').css('a::attr(href)').get()
    commits_by_author_url: str = repo_url + '/commits?author=' + args['login']
```

By using `get()` we retrieve the first `a href` of the list automatically.

{% include image.html id="get_first_repo" %}

Easy enough.

### The `parse_commits` method

#### Passing data from `parse_first_repo` to `parse_commits`

Now we need another parser that finds the commit patch file URL. In a similar
manner as the original `parser` method we need to pass the data to
`parse_commits`:

```python
data = {
    'commits_by_author_url': commits_by_author_url,
}
data = {
   **args,
   **data
}

yield response.follow(commits_by_author_url, callback=self.parse_commits, cb_kwargs={'args': data})
```

The data coming from the first method (`args`) needs to be merged to the one
just computed (`data`). This data structure is then passed to the new
`parse_commits` method using the `cb_kwargs` trick again.

#### Getting the latest commit

By using the CSS tags we can get the button of the latest commit of the
specified author. Once we have the button we just need to extract the
content of the `href` attribute.

{% include image.html id="latest_commit_button" %}

```python
def parse_commits(self, response, args):
    span = response.css('span[aria-label="View commit details"]')
    latest_commit_as_patch = span.css('a::attr(href)').get()
    if latest_commit_as_patch is None:
        latest_commit_as_patch_url = None
    else:
        latest_commit_as_patch_url = 'https://github.com' + latest_commit_as_patch + '.patch'
```

Finally we need to return the data with `yield`.

```python
data = {
    'latest_commit_as_patch_url': latest_commit_as_patch_url,
}

yield {
    **args,
    **data
}
```

Now we have all the data we need, including the URL of the text file containing
the email which will be extracted separately using the pipelines.

## Pipelines

In Scrapy, pipelines are used to "clean" the data you get from the spiders.
In this project pipelines are defined in the `github_public_data/piplines.py`
file and are normal Python classes.

### Cleaning the data

In this example we only need to create the `GetGithubEmailPipeline` for the
moment. As the name suggests, the `process_item` method is called for every
result of the spider.

```python
from itemadapter import ItemAdapter
import requests
import re

class GetGithubEmailPipeline:
    def process_item(self, item, spider):
        pass
```

Now we can cleanup some of the data:

```python
def process_item(self, item, spider):
    adapter = ItemAdapter(item)
    if adapter.get('name'):
        adapter['name'] = adapter['name'].strip()
    if adapter.get('bio'):
        adapter['bio'] = adapter['bio'].strip()
    if adapter.get('org'):
        adapter['org'] = adapter['org'].strip()
    if adapter.get('location'):
        adapter['location'] = adapter['location'].strip()
    if adapter.get('local_time'):
        adapter['local_time'] = adapter['local_time'].strip()
    if adapter.get('url'):
        adapter['url'] = adapter['url'].strip()
    if adapter.get('social'):
        for i in range(0, len(adapter['social'])):
            adapter['social'][i] = adapter['social'][i].strip()

    # [ ... ]
```

These instructions just remove spaces from the limits of the strings. Since
all these fields are optional we must check if they exist using the
`adapter.get` method.

### Getting the email (finally)

To get the email, we need to do an HTTP request against the commit patch page.
Once we have the content we need to search the `From:` string. Regular
expressions come in handy here.

We can then extract the email address and the email name as they have been
configured in Git by the committer.

```python
    # [ ... ]

    if adapter.get('latest_commit_as_patch_url'):
        page = requests.get(adapter['latest_commit_as_patch_url'])
        if page:
            mail = re.search('From: .*<.*@.*>', page.content.decode('UTF-8'))
            components = mail.group(0).split(' ')

            # Structure of the line is:
            #   From: $first_name $last_name $third_name $etc_name <$email>
            # The email is always the last component.
            adapter['email_address'] = components[-1]
            # Skip the `From:` and go from the second to last space-separated element.
            adapter['email_name'] = ' '.join(components[1:-1])
```

### Cleanup

Finally, we need to delete the superfluous variables and return the object.


```python
    del adapter['latest_commit_as_patch_url']
    del adapter['commits_by_author_url']

    return item
```

### Enabling the pipeline

Pipelines need to be enabled in the `github_public_data/settings.py` file.

```python
# [ ... ]

ITEM_PIPELINES = {
    "github_public_data.pipelines.GetGithubEmailPipeline": 600,
}

# [ ... ]
```

## Testing

```shell
scrapy crawl basic -O out.json; cat out.json | jq '.'
```

```json
[
  {
    "uid": "1024025",
    "login": "torvalds",
    "name": "Linus Torvalds",
    "bio": "",
    "org": null,
    "location": "Portland, OR",
    "local_time": null,
    "url": null,
    "social": [],
    "email_address": "<dsterba@suse.com>",
    "email_name": "David Sterba"
  }
]
```

As you see, the email is wrong for the reasons I explained earlier.

If I use my GitHub profile I get this instead:

```json
[
  {
    "uid": "8964425",
    "login": "frnmst",
    "name": "",
    "bio": "",
    "org": null,
    "location": null,
    "local_time": null,
    "url": "https://blog.franco.net.eu.org",
    "social": [
      "https://software.franco.net.eu.org",
      "https://codeberg.org/frnmst",
      "https://www.youtube.com/channel/UC2rr0LbIuy34JHEoCndmKiA",
      "https://keyoxide.org/396da54dc8019e4f4522cbd5a3fa3c2b4230215a"
    ]
  }
]
```

No email is available because I have set contributions to private.

## Bulk scraping

### GitHub REST API

Instead of pre-loading the URL list maunally, one can use
the
[GitHub REST API](https://docs.github.com/en/rest?apiVersion=2022-11-28 "GitHub REST API documentation - GitHub Docs")

```shell
curl https://api.github.com/users?since=0
```

which results in this truncated output (first 2 users):

```
  {
    "login": "mojombo",
    "id": 1,
    "node_id": "MDQ6VXNlcjE=",
    "avatar_url": "https://avatars.githubusercontent.com/u/1?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mojombo",
    "html_url": "https://github.com/mojombo",
    "followers_url": "https://api.github.com/users/mojombo/followers",
    "following_url": "https://api.github.com/users/mojombo/following{/other_user}",
    "gists_url": "https://api.github.com/users/mojombo/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mojombo/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mojombo/subscriptions",
    "organizations_url": "https://api.github.com/users/mojombo/orgs",
    "repos_url": "https://api.github.com/users/mojombo/repos",
    "events_url": "https://api.github.com/users/mojombo/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mojombo/received_events",
    "type": "User",
    "site_admin": false
  },
  {
    "login": "defunkt",
    "id": 2,
    "node_id": "MDQ6VXNlcjI=",
    "avatar_url": "https://avatars.githubusercontent.com/u/2?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/defunkt",
    "html_url": "https://github.com/defunkt",
    "followers_url": "https://api.github.com/users/defunkt/followers",
    "following_url": "https://api.github.com/users/defunkt/following{/other_user}",
    "gists_url": "https://api.github.com/users/defunkt/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/defunkt/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/defunkt/subscriptions",
    "organizations_url": "https://api.github.com/users/defunkt/orgs",
    "repos_url": "https://api.github.com/users/defunkt/repos",
    "events_url": "https://api.github.com/users/defunkt/events{/privacy}",
    "received_events_url": "https://api.github.com/users/defunkt/received_events",
    "type": "User",
    "site_admin": false
  },
```

For example, starting from user id 0 you will get 47 users. By using
unauthenticated requests this way you can get up to `47 * 60 = 2820` user
logins per hour as explained in the
[documentation](https://docs.github.com/en/rest/using-the-rest-api/rate-limits-for-the-rest-api?apiVersion=2022-11-28#primary-rate-limit-for-unauthenticated-users "Primary rate limit for unauthenticated users - GitHub Docs")

### Fixing the spider code

Once you have the user logins you can change the code in the spider file, like
this:

```python
class BasicSpider(scrapy.Spider):

    # user_logins = requests.get('https://api.github.com/users?since=0')
    # <user_login JSON to Python dict>

    # [ ... ]

    start_urls = list()
    for i in user_logins:
        start_urls.append('https://github.com/' + i['login'])

    # [ ... ]
```

### Avoid being blocked

You should, of course, limit the number of requests per seconds to avoid being
blocked. You can set these in your `github_public_data/settings.py` file:

```python
CONCURRENT_REQUESTS = 1
DOWNLOAD_DELAY = 3
```

If you still face problems try ignoring the robot.txt rules and disabling
cookies:

```python
ROBOTSTXT_OBEY = False
COOKIES_ENABLED = False
```

## Conclusion

In this post I explained how to scrape some basic data for a GitHub profile
as well as the email through indirect means. In the next post we'll see how
to save this data on a MongoDB no-SQL database.

🚀
