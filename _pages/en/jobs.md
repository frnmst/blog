---
layout: page
title: Jobs
permalink: /jobs/
description: ! 'My computer science jobs and gigs'
lang: 'en'
---

## Fiverr reviews

{% include fiverr_gigs.html %}

## Scripts and documentation

I can
[write standalone scripts that interact with APIs, etc...](https://it.fiverr.com/s/4eZ65b)
as well as [detailed software documentation](https://it.fiverr.com/s/b47YA1).

## Lessions

I give lessions in computer science (programming, database, networks, Linux, etc...)
via video call on Nextcloud, Skype, Google Meet, Zoom, etc...

## Other

- Linux installations
- Android ROM installations
- Solutions for generic PC problems
- Data recovery (if the memory is not physically damaged, for example if the
  user deleted files involuntarily

## Contacts

See the [contacts]({{- '/about/' | relative_url -}}#contacts) section.
