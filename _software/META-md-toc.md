---
meta: true
software_name: md-toc
archived: false
repositories:
  - label: 'canonical'
    url: 'https://software.franco.net.eu.org/frnmst/md-toc'
  - label: 'codeberg'
    url: 'https://codeberg.org/frnmst/md-toc'
  - label: 'framagit'
    url: 'https://framagit.org/frnmst/md-toc'
  - label: 'github'
    url: 'https://github.com/frnmst/md-toc'
documentation:
  - label: 'docs.franco.net.eu.org/md-toc'
    url: 'https://docs.franco.net.eu.org/md-toc'
  - label: 'changelog'
    url:
related_links:
  - label: 'Repology python:md-toc (package tracking)'
    url: 'https://repology.org/project/python:md-toc/versions'
  - label: 'Repology md-toc (package tracking)'
    url: 'https://repology.org/project/md-toc/versions'
  - label: 'Free Software Directory'
    url: 'https://directory.fsf.org/wiki/Md-toc'
  - label: 'Tidelift'
    url: 'https://tidelift.com/subscription/pkg/pypi-md-toc'
  - label: 'Libraries.io'
    url: 'https://libraries.io/pypi/md-toc'
  - label: 'Liberapay'
    url: 'https://en.liberapay.com/frnmst/'
  - label: 'Buy Me a Coffee'
    url: 'https://www.buymeacoffee.com/frnmst'
---
