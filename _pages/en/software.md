---
layout: page
title: Software
permalink: /software/
description: ! 'Official source for software published by Franco Masotti'
lang: 'en'
---
{%- include software/software_toc.liquid %}
<div markdown="1">
{% include software/en/software_introduction.liquid %}
{% include software/en/software_authenticity.liquid %}
</div>
{% include software/software_content.liquid %}
