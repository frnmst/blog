---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpydocs
software_name_python_module: fpydocs
software_version: 4.2.0
software_version_raw: 000004.000002.000000
release_timestamp: 2021-12-05 15:18:59
is_on_pypi: false
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Use of verbatim blueprints instead of copy-paste.
- Updated email.

### Fixed

- Use of atomic git operations to push changes to build AUR package.

### Added

- Added a blueprint file containing the post-receive hook configuration
  instead of having it embedded in the script.
