---
title: ! 'Simple PONG game using the Phaser framework'
tags: [phaser-3, pong, game, javascript, video, youtube]
updated: 2024-01-19 21:33:00
description: ! 'This video shows a PONG game written in Javascript using the Phaser 3 framework'
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/1FsPx4jJBM0?si=ArIlHLnMUJGqCPgO" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Creating a simple PONG game using the Phaser 3 framework in JavaScript.

## Table of contents

<!--TOC-->

- [Table of contents](#table-of-contents)
- [Demo](#demo)
- [Instructions](#instructions)
- [Files](#files)
  - [Main file](#main-file)
  - [Assets](#assets)
    - [Images](#images)
    - [Audio](#audio)

<!--TOC-->

## Demo

[Try the game](/extras/solve-computer-science/guided-programming-tutorials/simple-pong-game-using-the-phaser-framework/pong.html "Try the PONG game")

## Instructions

{%- assign url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/solve-computer-science/guided-programming-tutorials/simple-pong-game-using-the-phaser-framework/README.md" %}
{% request url %}

## Files

### Main file

{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/solve-computer-science/guided-programming-tutorials/simple-pong-game-using-the-phaser-framework/pong.html" text="pong.html" name="pong.html" is_text=true code_lang="html" %}

### Assets

#### Images

{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/media/branch/master/_extras/solve-computer-science/guided-programming-tutorials/simple-pong-game-using-the-phaser-framework/paddle.xcf" text="paddle.xcf" name="paddle.xcf" is_text=false %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/media/branch/master/_extras/solve-computer-science/guided-programming-tutorials/simple-pong-game-using-the-phaser-framework/paddle.png" text="paddle.png" name="paddle.png" is_text=false %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/media/branch/master/_extras/solve-computer-science/guided-programming-tutorials/simple-pong-game-using-the-phaser-framework/ball.xcf" text="ball.xcf" name="ball.xcf" is_text=false %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/media/branch/master/_extras/solve-computer-science/guided-programming-tutorials/simple-pong-game-using-the-phaser-framework/ball.png" text="ball.png" name="ball.png" is_text=false %}

#### Audio

{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/media/branch/master/_extras/solve-computer-science/guided-programming-tutorials/simple-pong-game-using-the-phaser-framework/beep.mp3" text="beep.mp3" name="beep.mp3" is_text=false %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/media/branch/master/_extras/solve-computer-science/guided-programming-tutorials/simple-pong-game-using-the-phaser-framework/beep.aup" text="beep.aup" name="beep.aup" is_text=false %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/media/branch/master/_extras/solve-computer-science/guided-programming-tutorials/simple-pong-game-using-the-phaser-framework/beep_data/eff/d02/eff022cc.au" text="beep_data/eff/d02/eff022cc.au" name="eff022cc.au" is_text=false %}
