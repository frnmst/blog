---
title: ! 'Homebox: your personal inventory system'
tags: [homebox, self-hosted, youtube, video]
related_links: ['https://www.youtube.com/watch?v=b-W0m-lwGGM']
updated: 2024-02-29 17:24:30
description: ! "Homebox is an open source inventory server. In this video I'll show you some features and how to install it"
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/b-W0m-lwGGM?si=6xJbHC3KoILWpGY8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In this video we'll see Homebox, a personal FLOSS (Free Libre and Open Source)
inventory system.

Installation is done with docker-compose. Apache is used as reverse proxy.
