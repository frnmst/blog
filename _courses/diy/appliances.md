---
layout: course
title: ! 'Appliances'
permalink: /courses/diy/appliances/
description: ! 'Appliances courses'
enable_markdown: true
lang: 'en'
courses:
  comment: ! 'Tips to fix appliances, etc...'
  course:
  - subject: ! 'Coffee makers'
    comment: ! ''
    data:
      - id: 0
        type: video
        title: ! "De'Longhi EN97 cleanup"
        comment: ! 'Very useful when you see that no coffee is coming out the machine. This trick worked very well.'
        embed_html: |
          <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/qqyt43vitFM?si=9sfDjgyXpSJp8N6X" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

        backup_url: ! 'https://assets.franco.net.eu.org/video/blog.franco.net.eu.org/%e2%9a%a1%20Come%20SMONTARE,%20pulire%20e%20decalcificare%20una%20macchina%20da%20caff%c3%a8%20Nespresso%20%5bNUOVA%20VERSIONE%5d%20%5bqqyt43vitFM%5d.webm'
---
