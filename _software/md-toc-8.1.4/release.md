---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.1.4
software_version_raw: 000008.000001.000004
release_timestamp: 2022-06-15 14:17:41
is_on_pypi: true
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Readme is now in markdown instead of rst.

### Fixed

- Improved readme according to [issue #36](https://github.com/frnmst/md-toc/issues/36): better description of md-toc features
  compared to similar solutions.
- Improved TOC marker detection and TOC substitution in place: replace an existing TOC only if
  - the TOC is a list
  - or there is no content between the TOC markers

Note: GitHub Flavored Markdown is still at version 0.29. md-toc does its best to
      support both GFM 0.29 and cmark 0.30.
