## Site Info
title:
  lang:
    en:                 'Solve Computer Science'
    it:                 'Solve Computer Science'
    fr:                 'Solve Computer Science'

description:
  lang:
    en:                 "A blog about technology, software development, self-hosting, free and open source software"
    it:                 "Un blog che parla di tecnologia, sviluppo software, self-hosting, software libero e open source"
    fr:                 "Un blog sur la technologie, le développement de logiciels, le self-hosting, les logiciels libres et open source"

announcement:
  lang:
    en:                 "Check the [services I offer](/jobs/)"
    it:                 "Guarda i [servizi che offro](/it/jobs/)"
    fr:                 "Consultez les [services que j'offre](/fr/jobs/)"

website:
    html_elements:
      index:
        showing_latest_posts:
          lang:
            en:         'Showing the latest posts'
            it:         'Questi sono gli ultimi post'
            fr:         'Ce sont les derniers articles'
        other_posts_in_nav:
          lang:
            en:         'other posts available in this blog'
            it:         'altri post disponibili in questo blog'
            fr:         'autres articles disponibles sur ce blog'
      navigation:
        main_content:
          lang:
            en:         'Main content'
            it:         'Salta'
            fr:         'Sauter'
        sitemap:
          lang:
            en:         'Sitemap'
            it:         'Mappa del sito'
            fr:         'Plan du site'
        home:
          lang:
            en:         'Home'
            it:         'Home'
            fr:         'Accueil'
        about:
          lang:
            en:         'About'
            it:         'Riguardo a'
            fr:          'À propos'
        jobs:
          lang:
            en:         'Jobs'
            it:         'Lavoro'
            fr:         'Emplois'
        software:
          lang:
            en:         'Software'
            it:         'Software'
            fr:         'Logiciel'
        courses:
          lang:
            en:         'Courses'
            it:         'Corsi'
            fr:         'Cours'
        search:
          lang:
            en:         'Search'
            it:         'Rircerca'
            fr:         'Chercher'
        privacy:
          lang:
            en:         'Privacy'
            it:         'Privacy'
            fr:         'Privacy'
        feed:
          lang:
            en:         'Feed'
            it:         'Feed'
            fr:         'Flux Atom'
        content:
          lang:
            en:         'Content'
            it:         'Contenuto'
            fr:         'Contenu'
        top:
          lang:
            en:         'Top'
            it:         'Inizio'
            fr:         'Début'
        back_to_top:
          lang:
            en:         'Back to top'
            it:         "Torna all'inizio"
            fr:         'Retour au sommet'
        next:
          lang:
            en:         'next'
            it:         'prossimo'
            fr:         'suivant'
        prev:
          lang:
            en:         'previous'
            it:         'precedente'
            fr:         'précédent'
      software:
        table_of_contents:
          lang:
            en:         'Table of contents'
            it:         'Sommario'
            fr:         'Table des matières'
        introduction:
          lang:
            en:         'Introduction'
            it:         'Introduzione'
            fr:         'Introduction'
        authenticity:
          lang:
            en:         'Authenticity'
            it:         'Autenticità'
            fr:         'Authenticité'
        extract:
          lang:
            en:         'Quote'
            it:         'Citazione'
            fr:         'Citation'
        signing_keys:
          lang:
            en:         'Signing keys'
            it:         'Chiavi per la firma'
            fr:         'Clés de signature'
        terminology:
          lang:
            en:         'Terminology'
            it:         'Terminologia'
            fr:         'Terminologie'
        methods:
          lang:
            en:         'Methods'
            it:         'Metodi'
            fr:         'Méthodes'
        upload_what_i_have_to_do:
          lang:
            en:         'Upload (what I have to do)'
            it:         'Upload (cosa devo fare)'
            fr:         'Upload (ce que je dois faire)'
        download_what_you_have_to_do:
          lang:
            en:         'Download (what you have to do)'
            it:         'Download (cosa dovete fare)'
            fr:         'Download (ce que vous devez faire)'
        software:
          lang:
            en:         'Software'
            it:         'Software'
            fr:         'Logiciel'
        repositories:
          lang:
            en:         'Repositories'
            it:         'Repositories'
            fr:         'Dépôts'
        documentation:
          lang:
            en:         'Documentation'
            it:         'Documentazione'
            fr:         'Documentation'
        related_links:
          lang:
            en:         'Related links'
            it:         'Link correlati'
            fr:         'Lines associés'
        releases:
          lang:
            en:         'Releases'
            it:         'Versioni'
            fr:         'Versions'
        version:
          lang:
            en:         'version'
            it:         'versione'
            fr:         'version'
      iso639_selector:
        available_in:
          lang:
            en:         'available in'
            it:         'disponibile in'
            fr:         'disponible en'
      footer:
        content:
          lang:
            en:         'Content'
            it:         'Contenuto'
            fr:         'Contenu'
        updated:
          lang:
            en:         'Updated'
            it:         'Aggiornato'
            fr:         'Mis à jour'

    # This should point to your website source.
    source:             "https://software.franco.net.eu.org/frnmst/blog"
    license:
        url:            "https://creativecommons.org/licenses/by-sa/4.0/"
        name:           "CC-BY-SA 4.0"
    category:           ["technology",
                         "programming",
                         "software",
                         "python",
                        ]
    isso:
        domain:        "isso.franco.net.eu.org"
        script_values:
          - key:       "data-isso-vote"
            value:     "true"
          - key:       "data-isso-reply-notifications-default-enabled"
            value:     "true"

author:
    name:               "Franco Masotti"
    email:              "franco.masotti@tutanota.com"
    buymeacoffee:
        message:        "Buy me a coffee"
        url:            "https://buymeacoff.ee/frnmst"
    social:
      youtube:
        url:          "https://www.youtube.com/channel/UC2rr0LbIuy34JHEoCndmKiA"
        alt:          "Solve Computer Science YouTube channel"
      mastodon:
        url:          "https://social.franco.net.eu.org/@frnmst"

# Post permalink. If you change this you must
# modify all the occurencies manually.
permalink:              "/post/:title.html"
# https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
timezone:               "Europe/Rome"
future:                 true

# Locales.
languages:
  default: 'en'
  supported:
    - 'en'
    - 'it'
    - 'fr'

# Used for OpenGraph only. Map lang values to lang_TERRITORY as described in
# the Opengraph specs.
locales:
  supported:
    - lang: 'en'
      code: 'en_US'
    - lang: 'it'
      code: 'it_IT'
    - lang: 'fr'
      code: 'fr_FR'

navigation:
  post_jump: 5
  post_links_on_index: 16

## Site Build
highlighter:            "rouge"
markdown:               "kramdown"
kramdown:
    input:              "GFM"
sass:
    style:              ":compressed"

excerpts:
    enabled:            true
    start:              "<!--excerpt_start-->"
    end:                "<!--excerpt_end-->"
    words:              10

tag_list:
    score:
        min:            8
    # Set the following to false to avoid limiting the tag links in posts.
    link_limit:         16

meta:
    description_words:         140
    excerpt_words:             140
    site_description_words:    140

anchor_headings:
    enabled:            true
    character:          "#"
    min:                1
    max:                6

    # Puts the anchor heading before the title if set to true.
    before:             false

feeds:
    # Keep last max_release_updates_per_software in RSS/Atom feeds.
    max_release_updates_per_software: 2

    # Keep last max_posts posts in RSS/Atom feeds.
    max_posts: 10

collections:
  pages:
    output:             false
  media:
    output:             true
  assets:
    output:             true
  software:
    output:             true
    sort_by:            "software_name"
  courses:
    output:             true
  extras:
    output:             true

# Link previews.
url_metadata:
  open_timeout: 20 # timeout after 5 second if connection doesn't open
  read_timeout: 10 # timeout after 3 second if there's no data returned
link_preview:
  title_max_characters: 64
  description_max_characters: 128

## Page names
defaults:
  -
    scope:
      path:             ""
    values:
      layout:           "null"
  -
    scope:
      type:             "pages"
    values:
      layout:           "page"
  -
    scope:
      type:             "posts"
    values:
      layout:           "post"
      is_post:          true
  -
    scope:
      path:             "_pages/*/index.md"
    values:
      is_home:          true
  -
    scope:
      path:             "_pages/*/404.md"
    values:
      is_404:           true
  -
    scope:
      path:             "_pages/*/tags.md"
    values:
      is_tags:          true
  -
    scope:
      path:             "_pages/*/sitemap.md"
    values:
      is_sitemap:       true
  -
    scope:
      path:             "_pages/*/about.md"
    values:
      is_about:         true
  -
    scope:
      path:             "_pages/*/software.md"
    values:
      is_software:      true
  -
    scope:
      path:                           "_pages/*/software_instructions.md"
    values:
      is_software_instructions:       true
  -
    scope:
      path:                           "_pages/*/jobs.md"
    values:
      is_jobs:                        true
  -
    scope:
      path:                           "_pages/*/privacy_policy.md"
    values:
      is_privacy_policy:              true
  -
    scope:
      path:                           "_software/*/release.md"
    values:
      is_software_release:            true
  -
    scope:
      path:                           "_software/CHANGELOG-*"
    values:
      is_software_changelog:          true
  -
    scope:
      path:                           "_pages/*/courses.md"
    values:
      is_courses_index:               true
  -
    scope:
      path:                           "_courses/*"
    values:
      is_courses:                     true
  -
    scope:
      path:                           "_extras"
    values:
      is_extras:                      true


## Other Params
include:
  - "_pages"

exclude:
  - "LICENSE"
  - "README.md"
  - "CNAME"
  - "vendor"
  - "Gemfile"
  - "Gemfile.lock"
  - "Makefile"
  - ".venv"
  - ".pre-commit-config.yaml"
  - "requirements.txt"
  - "requirements-dev.txt"
  - "requirements-freeze.txt"
  - ".project.mk"
  - ".vale"
  - ".vale.ini"
  - '.vale_linter.sh'
  - 'dump.txt'
  - 'experiments.html'

  # Svelte.
  - 'components'

  # Git
  - '.gitignore'


plugins:
  - 'jekyll-url-metadata'
