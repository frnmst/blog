---
title: ! 'Reverse a string in C'
tags: [programming, tutorial, c, video, youtube, string]
updated: 2024-01-19 22:59:00
description: ! 'Different ways to reverse a string in C and some caveats'
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/i6mLWjXhcKk?si=9aiQOu77DJdxXI4k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In this video you'll see several algorithms and their implementations in C that
enables you to reverse a string.

There are some important details to note while implementing these algorithms in
C.

## Table of contents

<!--TOC-->

- [Table of contents](#table-of-contents)
- [Instructions](#instructions)
- [Files](#files)

<!--TOC-->

## Instructions

{%- assign url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/test/_extras/solve-computer-science/guided-programming-tutorials/reverse-a-string-in-c/README.md" %}
{% request url %}

## Files

{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/test/_extras/solve-computer-science/guided-programming-tutorials/reverse-a-string-in-c/Makefile" text="Makefile" name="Makefile" is_text=true code_lang="makefile" %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/test/_extras/solve-computer-science/guided-programming-tutorials/reverse-a-string-in-c/solution_1.c" text="solution_1.c" name="solution_1.c" is_text=true code_lang="c" %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/test/_extras/solve-computer-science/guided-programming-tutorials/reverse-a-string-in-c/solution_1_improved.c" text="solution_1_improved.c" name="solution_1_improved.c" is_text=true code_lang="c" %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/test/_extras/solve-computer-science/guided-programming-tutorials/reverse-a-string-in-c/solution_1_improved_no_underflow.c" text="solution_1_improved_no_underflow.c" name="solution_1_improved_no_underflow.c" is_text=true code_lang="c" %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/test/_extras/solve-computer-science/guided-programming-tutorials/reverse-a-string-in-c/solution_2.c" text="solution_2.c" name="solution_2.c" is_text=true code_lang="c" %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/test/_extras/solve-computer-science/guided-programming-tutorials/reverse-a-string-in-c/solution_3.c" text="solution_3.c" name="solution_3.c" is_text=true code_lang="c" %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/test/_extras/solve-computer-science/guided-programming-tutorials/reverse-a-string-in-c/solution_4.c" text="solution_4.c" name="solution_4.c" is_text=true code_lang="c" %}
