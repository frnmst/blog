---
layout: software_release
enable_markdown: true
title: release
excerpt: none
tags: [md-toc, markdown, table-of-contents, python]
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.2.1
software_version_raw: 000008.000002.000001
release_timestamp: 2023-11-07 17:25:33
is_on_pypi: true
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Clean up code:
  - use more list comprehensions
  - use less variables in general
  - remove some typing checks for the arguments passed to functions
    and replace them with improved typing notation in the function
    prototype
- Use stronger typing thanks to the [`typing`](https://docs.python.org/3.11/library/typing.html) module.
- Replace duplicated code in the `md_toc.api.remove_html_tags` function with a
  for loop.
- Use of the SHA1 checksum for the keys of the `header_duplicate_counter`
  dict. This lowers memory usage at the cost of time. The most effective
  application of this is when you have very long headings: previously the keys
  of this dict would take lots of space. By using a checksum, the space used
  is constant. See the `md_toc.api.build_anchor_link` function.
- Update documentation:
  - Add API change notices to the documentation. These changes will take place
    from version 9
  - Update Sphinx version and its theme
- Minimum supported Python version is now `3.8`.

### Added

- Create the `md_toc/types.py` files that holds the structure for some complex
  objects

Note: GitHub Flavored Markdown is still at version 0.29. md-toc does its best
to support both GFM 0.29 and cmark 0.30.
