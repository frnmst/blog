---
title: ! 'Fiverr order cancelled'
tags: [fiverr, gig, programming, script, scraping, money, python, mechanicalsoup, ai]
updated: 2024-08-05 15:46:47
description: ! "A Fiverr customer cancelled the order 30 days after completion. Here's what happened."
lang: 'en'
images:
  - id: 'contract'
    filename: 'contract.png'
    alt: ! 'Fiverr contract (order) between me and the buyer'
    caption: ! "I never put `Unlimited revisions` but the website thinks I did"
    width: '300px'
  - id: 'can_you_do_x'
    filename: 'can_you_do_x.png'
    alt: ! 'Buyer continuously asks for changes'
    caption: ! "See what I mean"
    width: '300px'
  - id: 'trust'
    filename: 'trust.png'
    alt: ! "Buyer saying that doesn't trust the program"
    caption: ! "Mmm..."
    width: '300px'
  - id: 'shit'
    filename: 'shit.png'
    alt: ! "Buyer saying that doesn't understand this *debian shit*"
    caption: ! "*Debian shit*"
    width: '300px'
  - id: 'offer'
    filename: 'offer.png'
    alt: ! "Buyer saying the offer is higher than expected"
    caption: ! "Well... it's not the first time I hear the *moving funds* thing"
    width: '300px'
---

<!--TOC-->

- [Introduction](#introduction)
- [Script purpose and logic](#script-purpose-and-logic)
- [Spreadsheet issue](#spreadsheet-issue)
  - [Alternative solution](#alternative-solution)
- [Email parsing](#email-parsing)
  - [Important note](#important-note)
  - [Logic](#logic)
- [After the two fixes](#after-the-two-fixes)
- [Hint](#hint)
- [Adding the user-agent string to MechanicalSoup](#adding-the-user-agent-string-to-mechanicalsoup)
- [Debian shit](#debian-shit)
- [Blocking comunication: possibile developments if that didn't happen](#blocking-comunication-possibile-developments-if-that-didnt-happen)
- [Fiverr customer service](#fiverr-customer-service)
- [Conclusions](#conclusions)
  - [What to do for the future](#what-to-do-for-the-future)
  - [Suggestions for Fiverr](#suggestions-for-fiverr)
- [The script](#the-script)
- [Can I help you?](#can-i-help-you)

<!--TOC-->

## Introduction

<!--excerpt_start-->Ok so I had a problem with a Fiverr buyer. It was the most
difficult gig I ever had since I joined the website.<!--excerpt_end-->

Besides the fact it was a continuous:

> Can you change x to y. If you can do y it would be better but even if you can
> do only x it would be ok anyway

during the development stage, the real problems came up after the order was
completed.

{% include image.html id="can_you_do_x" %}

I emphasised parts of the discussion in **bold**.

## Script purpose and logic

In short, the purpose of the script (code is at the end) was to scrape data from
[Bandcamp](https://bandcamp.com) starting from subscription emails. The buyer
was subscribed to different bandcamp artist pages and labels. The logic is
the following:

1. connect to the mailbox
2. parse emails one by one, matching the ones with Bandcamp news links
3. scrape each matched page: extract the usual information such as
   1. artist or band name
   2. location of origin
   3. label name
   4. release date
   5. etc...
4. compute extra fields such as:
   1. country of origin (starting from the location)
   2. summary of the release description (this one uses a local A.I. running
      on the CPU)
   3. etc...
5. save the output to a spreadsheet

Now, initially there were some hiccups: I didn't have direct access to his
Google spreadsheet account (I only had the API keys), and I also only had
the Gmail app password.

## Spreadsheet issue

For the first issue this meant I could not see the
final result of the script. I mean, since I was working with
[Pandas dataframes](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html)
I could see the results but I could not see how the spreadsheet was actually
populated. As long as the worksheet is new there are no issues in passing the
data to Google. This is possible using a Python library called
[gspread-dataframe](https://github.com/robin900/gspread-dataframe).

However, there is no native solution to append data to an existing worksheet,
apparently. I tried [this code](https://stackoverflow.com/a/73984886) which
seemed to work initially, but it was adding extra blank rows between runs.

It was very difficult to check if the data was accurate also because I had to
go back and forth with the buyer via the chat.

Buyer:

> Hey - so I just sat down to this now. The good news is all the setup was fine and the venv etc runs ok. Still, there's a couple of questions. I kind of foolishly ran it with only 34 lines in the sheet. I deleted the previous entries to start clean and it only filled lines 30-34 as it had nowhere else to go... so I got like 4/5 entries... It works. Although I did it again and now it doesn't seem to add.

> I'm really confused as to how I select the emails for it to upload (I want to prioritise all unread that have arrived to me in the last week or so first) and then the rest... I also am not sure what the google API limit is so I don't know how many I can expect it to do.... **I am in no way technicall minded** so I'd appreciate any pointers on this!

> [ ... ] The original **code as I say was working fine** [ ... ]

> [ ... ]

> **Basically I just want really simple instructions as to be honest this is holding my processess up.** [ ... ]

> [ ... ]

> **just give me simple instructions to do that**

> [ ... ]

> **I just want something that is going to work easily 100% of the time.**

> [ ... ]

> I was working on the numbers file not the xlsx - **it worked!**

Now, for purpose of testing, I could have set up a
[Google Cloud](https://cloud.google.com/free?hl=en) account myself and use
their free 90 days trial (300$ value). I tried to do it but they want your
credit card info even for a free trial, which is insane. Moreover I could not
find the instructions to set up the Google spreadsheets API anywhere: maybe
they appear after you give them your info? I don't know.

### Alternative solution

Finally I talked to the buyer and opted to output to a plain excel file, which
they agreed. This means the client could syncronize the file on Google Drive,
Dropbox or whatever. Now, at least, I could see the real output.

Appending data to excel files using Pandas is not that simple either. As you'll
see in the code below you have to read and parse the whole excel file and load
it in a DataFrame, then add the new data to it, and finally replace the whole
excel file. This is the only reliable solution I found but it does not work for
*big* amounts of data: I ran out of memory while processing all the emails.
Once the next major version of Pandas comes out, it will have
[native append support for the `ExcelWriter` object](https://pandas.pydata.org/docs/dev/reference/api/pandas.ExcelWriter.html#pandas.ExcelWriter).

*Note: In the code below I omitted the output to the Google worksheets since it was
only partially functional.*

## Email parsing

### Important note

I still haven't mentioned that I had to work with a mailbox *I could not see*
that contained 3484 emails. The buyer wanted all these emails to be
parsed for links to Bandcamp and all the data extracted accurately.

### Logic

By using Python's [imaplib](https://docs.python.org/3/library/imaplib.html)
you can connect to a mailbox and download all the messages. Since I was dealing
with 3484 emails I thought it was prudent to do this job in chunks. It was easy
to implement this because all that is needed is a start and end id for the
emails.

This seemed to work fine.

Now, not all those 3484 emails were Bandcamp-related, so when one sets a start
and end id, let's say `start_id = 10` and `end_id = 20`, some of those 11
emails might have not had bandcamp links in it (it's 11 because:
`start_id <= id <= end_id`).

Now the buyer was trying the script and seemed to be working fine. This
chunking thing was useful because if his computer, his internet or the website
had problems (it happened), the buyer didn't have to restart scraping from
scratch. The data was pulled correctly and written to the excel file. But when
the buyer re-started the script using the same IDs as before, they noticed that
the data in the output was different. After some tests and my assistance they
whine saying:

> [ ... ] I can't trust the program [ ... ]

{% include image.html id="trust" %}

Anyway I offered to download the data myself and in the meantime I implemented
the
[IMAP sorting by oldest date](https://stackoverflow.com/questions/5632713/getting-n-most-recent-emails-using-imap-and-python) which was
the real issue that caused the discrepancy.

## After the two fixes

> Hey! hope you are well. So, a couple of things. 1) in the releases you sent over last week it had a load that were put as the day of the extraction as the release date - but they weren't. Although honestly, I think these  are just from over time. It's a small anomaly so I will keep an eye out for if it repeats but I don't think it will be a big issue.
>
> Although yesterday I just ran this for the first time - I waited to accumulate and inbox full of emails. There were 124 and it had no problems. I got like 80 outputs as the others weren't relevant. BUT, the country came out unknown for all...

My reply:

> Hi. I don't understand what you mean with the first point. Concerning the second point you have to check the debug output and see if there are warnings. I just did a test run and the countries were retrieved

Infact I still have no idea what point `1)` means.

About the second point, I later implemented a retry strategy (just in case) for
[Nominatim](https://nominatim.org/release-docs/develop/),
the API from OpenStreetMap that enables you to do different type of queries
related to location data. The buyer wanted 100% accurate data about the country
starting from the location on Bandcamp. For example starting from
`Los Angeles, California` they wanted `United States` as country. Using
Nominatim covered almost all cases correctly. This REST API, however,
[has limits](https://operations.osmfoundation.org/policies/nominatim/#unacceptable-use):
to do lots of queries you need to change the user agent string after a
certain number of queries in an undetermined amount of time or you get blocked.
Self-hosting this server is out of question since it requires
[128GB of RAM](https://nominatim.org/release-docs/latest/admin/Installation/#hardware)
to work without problems (totally overkill for this project).

After 7 days of silence the buyer writes:

> Hey - I'm having issues with the code. Last week when I sent it with the date issue was the first time I ran it... today the second

Still didn't answer what they meant by that...

> [ ... ]

> man this is really, really letting me down. It's not how I expected this work - I really just wanted soemthing that took the headache out of the manual process and I'm still here no wiser as to how to et the aiutmatic process to work. what is happening?

Judge this for yourself and compare it with the things they
said earlier:

> I can't express how desperate I am for a solution and one I can trust. **This code hasn't worked in any moment. You running it** and sending me things isn't the solution. I need something that works and I can trust in. And I can't spend hour and hours here chatting through to get it to work. I might as well just be uploading manually. I really need working code

My reply:

> The latest solution I provided you always worked when I tested it. In the latest run (attachment) I didn't see any errors while it was running. You can try running the script in a Debian 12 virtual machine so at least we can exclude an environment problem (I can't test the script on MacOS since I don't own apple devices). Given the fact that all fields are empty it's likely that the script was unable to scrape the website for some reason. It's difficult to tell what's going on only with the excel output file. You should check the console for errors: maybe limit the scraping to 2 mails and copy the output to a text editor

## Hint

After this this they sends me a screenshot of the terminal where I notice a
403 HTTP error, so I imagine the script might have been banned by the website.

They say:

> [ ... ]

> That is to say accessing bandcamp from my ip has no problems... At least not if i go there normally

I replied:

> it depends how many times you tried the script earlier (on how many urls). probably they blacklisted the combination of ip and user agent. the easiest way would be to restart your router if you have a dynamic ip and see. if this still does not work maybe trying setting the user agent in the script for the scraping part but that needs to be implemented. since it always worked for thousands of urls in a single session i'm not 100% sure this is the cause

Buyer:

> Like I said the new script i ran twice. Last week and this... But I'll do that this afternoon. I'll let you know

> [ ... ] it's still not working for the block on the ip or whatever it is [ ... ]

Later:

> [ ... ]

> Although i just connected my laptop to my phone data instead of wifi and it had the same http error

## Adding the user-agent string to MechanicalSoup

Still on the same day I wrote this:

> I added the random user agent I mentioned earlier. You just need to do "pip install fake-useragent" in your venv before running

They say it still didn't work:

> cheers. I installed but it looks like it doesn't want to work still :(

I reply:

> I tried running the script through tor just to see if exit nodes are block and indeed i'm getting empty results, but 200 as HTTP status and not 403. I just had an idea to print the page content so we may have a clue. It's just 1 extra line from the previous code

I later discovered that adding the user agent is detrimental: apparently
to scrape Bandcamp using MechanicaSoup, which uses Requests, you don't
need to pass fake user-agent headers. The default ones from Requests are fine.

> dude. **I can't express how much I really appreciate your ongoing support with this**

My reply:

> i just want this to be fixed. i don't understand why you are getting the 403 error since one of the latest times it worked on your pc (except the country which uses a separate api)

> the error is pretty clear. if you are 100% sure you changed your IP address (you can check with "what is my ip" on google) you could try running the script in a debian vm: maybe it's an evironmental factor

They:

> this time i'm back on the same IP as always. because as I mentioned it didn't work even when I did change. Tomorrow - I'm going to go to [ ... ] and try from there to see if it is an ip thing

> I'll also download debian 12

## Debian shit

After trying to set up Debian and me explaining it, we got to this point. Plus,
the buyer never mentioned the new place again and writes all these consecutive
messages:

> dude **this debian shit** has me so lost

> so so lost

{% include image.html id="shit" %}

> can we not just get this running off terminal

> I have no idea what is happening here.

> I came to you for a solution and I told you the tech I'm running

> none of this virtual box, debian stuff makes any senawe

> sense

> as I say. **this code has not run properly one single time**

> **I'm really, at the end of my patience**

> **I just want a solution**

> its taking so much time and we're just back and forth

> **the debian stuff makes zero sense to me** - and quite hoenstly I just wnat a solution

> [ ... ]

> are we just going to accept **this code doesn't work** or is there a solution

> 'caause this has not made my life easier. **it's just stolen so much of time I could have used on other things**

Yes it stole my time as well...

> I'm crushed. my morale is flattened by this

> I need this to run on mac... its what I asked from the start. **this virtual machine, debian stuff is not what we agreed**. mate, I really can't express how desperate I am for a solution

> **if not I'll have to just get you to run the inbox as it is - send the latest output complete and then just give and call it money lost becasue this is absolutely insane downloading and running virtual machines I have no idea about... this isn't the solution I wanted or need**

> **I need a solution or to give up and reclaim my time or pay someone else to sovle this**

> i'm crushed mate

> or you can help find a solution that works on a macbook which was the task all along

> the idea was this would save me time.... it's cost me 10 x more time in the last few weeks. **Not once has the code run successfully extracting everything I need it to.. that's mad**

{% include image.html id="contract" %}

My reply:

> Here's the latest output from the script. Use this version for the future (user agent changes reverted). I told you to try Debian to rule out environmental factors but since you get HTTP 403 it must be the IP (I'd say 99% it is) which has been blacklisted (since it always worked, and still works on my part). The script should work exactly the same also on macos since i'm not using platform-specific python operations. There's not much i can do from here except to tell you to change ip address. I doubt the website bans ip addresses by zone instead of single IPs. The problem is I can't personally test any change I make on my part to address the 403 error since, as I told you, it's working fine on my part, so I'd never know what happens until you try them. I worked lots of hours even at night helping to find a solution for this

Buyer:

> I have an idea. Is debian for windows. [ ... ] Could I run that easily [ ... ] computer? I'll give it a try on there if so. With regards to IP when I messaged the other day (Thursday) it was from another location. E.g. Different IP and nothing. [ ... ] different internet set up in the new place so I'll know for certain **next saturday**. But if Debian is a windows programme I will be able to download and try on [ ... ] computer and let you know

*next saturday* meant exactly 7 days after, WTF!

Me:

> Debian is a linux based operating system. windows and macos are operating systems as well. To avoid changing the other computer you still need to set it up as virtual machine. as an alternative you can install it along side windows (dual boot) or on a totally different hard disk. About the ip we'll see with your new internet. i had problems getting the data when passing the headers to the virtual browser so that's why i sent you the script again: the version i sent you yesterday is the one that certainly works

## Blocking comunication: possibile developments if that didn't happen

At this point 30 days have passed since the order was marked as completed by
the buyer. I was really exhausted from all this conversation that I decided to
block all communication using the available function in the Fiverr chat.

Once I solved the two initial problems (spreadsheet and email), everything else
was out the scope of the gig. I sent the buyer a working script.
I over-delivered to find a solution for the problem the buyer was facing. The
next steps would have
been:

- buyer buying a cheap Linux VPS
- me trying Hackintosh or something similar (to see if it was a macOS specific
  problem but I really think it would have worked fine)
- redo the scraping part of the script using Selenium (out of question: that
  would have costed countless hours of development that they probably would
  have never payed, see below; plus using selenium to scrape that amount of
  data would be much slower when executing the script)

I couldn't give any more free support. Initially, 1 month earlier than my
block, the buyer was not very happy of the price, so I couldn't imagine the
client spending more money on this, lol:

> ok man thanks for the offer. I do have a question, just as to why it is significantly more than your standard premium package? It's a bit more than I expected, but I appreciate the work you have put in too [ ... ]

{% include image.html id="offer" %}

And I replied:

> Yes, it's because of the time spent, around 6 days. Initially I told you 30$ because I expected something very  different.

The other reasons were the continuous demands during the development phase
which involved re-doing bit and pieces of the script: that took a lot of time.

Anyway, the day after the block I got a notification stating that the order was
cancelled by Fiverr customer service. All that time and effort gone in one
click, while the customer could still keep a copy of the working script.

## Fiverr customer service

Obviously I contacted CS the same day the order was cancelled.

This conversation is interesting since you get to know how the website actually
works.

> Hello,
>
> I have a problem with a customer. I delivered the Python script as promised. It was a very complex scraping script, but despite the initial problems, I was able to solve them one by one, so I sent [ ... ] back the script several times until it was completely fixed. After this, the buyer complained that the script still didn't work. [ ... ] sent me the screenshots. I tried to help [ ... ] for free, even at night and gave [ ... ] lots of advises. This went on for a couple of weeks. I have to highlight that fact that the final version of the script always worked for me. I think the problem is an environmental one on his side which is out of the scope of my gig. [ ... ] said the script "hasn't worked in any moment" which is not true. Initially the script pulled the correct data on his environment as well (during the initial phase before the final version of the script). Anyway, I sent the script output several times during this all time frame to help [ ... ]. I think the combination of his operating system, IP address and user agent (or HTTP headers) was banned from the website intended to be scraped. Now, a month has passed since the order was completed. I had to block the buyer yesterday since [ ... ] made me very uncomfortable and I didn't want to waste more time. I was always polite with [ ... ]. Since I worked a lot for this gig and even did lots of extra hours to accomodate his demands I don't think it's fair. Can you please help me with this?
> For reference you can have a look at the entirety of the chat but I'll give you some extracts in the attachments.
>
> Thanks
>
> Kind regards.

Reply:

> Hi there!
>
> ${name} here, and I will be assisting you today!
>
> I can understand your concern about the Order being canceled.
>
> After checking the order ${order} I can indeed see that we canceled the order after the buyer raised their concerns. That said, it seems that **not all of the agreed-on work has been provided within the delivery you've made. They have pointed out that the script you have provided never worked on the machine they use.**
>
> They have stated that they can no longer reach you and **we had to cancel the order**.
>
> In the future, I suggest double-checking the requirements and comparing them to the completed work prior to delivering to ensure the work will be satisfactory for the buyer. **It is essential that you have constant communication with the buyer regarding their requirements and if you are unsure of something, it is best to first contact your buyer before proceeding with the order**. That way, you will have enough information to deliver as per the buyer's expectations and successfully complete the order.
>
> I completely understand that you've put effort and dedication into this order. **While we appreciate your desire to avoid cancelations, they are sometimes unavoidable**. Please know that we do our best to help mitigate issues on every order and that **cancelation is the last resort**. I’m certain that as a seller on Fiverr, you will have many amazing and successful orders, so please don’t let one cancelation discourage you.
>
> I sincerely hope that this clears things up now!
>
> Please let me know if there's anything else you need help with and I'll get back to you as soon as possible.
>
> Kind regards,

My reply:

> Hi ${name},
>
> thank you for the reply. I have a few points and a couple of questions:
>
>> That said, it seems that not all of the agreed-on work has been provided within the delivery you've made.
>
> I did over-deliver also going out the scope of my gig (for the support part). All the things in the order contract have been fulfilled (see attachment)
>
>> They have pointed out that the script you have provided never worked on the machine they use.
>
> The buyer is not accurate. As I said, there have been multiple versions of the script. The initial ones were pulling the data from the website also on his machine. The parts that didn't work were in the final output and email parsing, which I later fixed. After that the script wasn't pulling the correct data only on his machine, but on mine it always worked. I didn't change the data scraping parts so I suspect it's an environmental factor on his setup, a thing which is out of my reach.
>
>> They have stated that they can no longer reach you and we had to cancel the order.
>
> Yes this is true. I blocked [ ... ] after 30 days the order was marked as complete by the buyer. During the 30 days I tried to solve all the problems that arose. The final problem was not solvable (see previous point) without his active collaboration. I saw the conversation with [ ... ] was fruitless and uncomfortable so I cut the conversation.
>
> Now to the questions:
>
> 1. Let's say a buyer contacts customer support to cancel an order 10 years after an order has been completed, and customer support agrees with [ ... ]. Is there no time limit on this?
>
> 2. Since for the moment this specific order has been canceled (thus the contract is nullified), I am free to use my script as I please, and the buyer should delete it.
>
> 3. I always put 0 revisions for custom orders. Why do I see unlimited revisions in the order summary (see attachment)?
>
> Thanks again

Their reply:

> Hi there,
>
> Thank you for reaching out again.
>
> I have reviewed your questions and I will address them as such.
>
> 1. We examine every order that is reported to us carefully in order to mediate a resolution that would benefit both parties. Usually, communicating with your buyer is the best way to resolve an issue. We encourage users to try and resolve issues amicably between themselves prior to requesting our intervention. If for any reason you are unable to successfully resolve this issue with the buyer, please contact us and we will be happy to review the order again to see what we can do. The cancelation is the last option for us and only if we find that the order is eligible for it as per our guidelines.
> 2. The answer to this is yes. **Once the order is canceled all the ownership rights are transferred back to you and you are free to use the script as you would like. The buyer can't use any deliverables from the canceled order.**
> 3. On my end it seems that the revisions were added as an extra in the custom offer that you have provided to the buyer.
>
> Please let me know if there is anything else we can help you out with.
>
> Kind regards,

Practically point 1 is a copy-paste from somewere and does not answer my
question. My request should have been:
*Is there a time limit after which
Fiverr customer service cannot cancel an order anymore?* but I think the
original one was clear nontheless. I suspect the answer is to point 1 is *no*,
as explained [in the ToS](https://www.fiverr.com/legal-portal/legal-terms/payment-terms-of-service)
which I later read:

> In rare circumstances where we find it appropriate, our Customer Support team may cancel a completed Order even after 14 days have passed from its completion.

So is it from `0 <= delivery_completed_days <= Inf`?

Point 2: how do they actually enforce this? It's impossible... But at least
I can publish the code below.

Point 3: are there bugs in the website?

I reply:

> Hi again.
>
> Before closing the ticket a couple more clarifications on my previous questions:
>
> 2. was the buyer informed of this? If not I think [ ... ] should be by the CS.
> 3. I never added extra revisions as part of my orders, although in the gig page I enabled this possibility by using the "Additional revision" option for a certain price. I see that some of my previous orders have the "Unlimited revisions" notice as well. Now I disabled this option in the gig page. I'll get in touch if I see this problem again

Reply:

> Hi there,
>
> Thank you for reaching out again.
>
> **Yes the buyer has been notified of this as the information is available to all users.** If you do believe that any buyer is using the material from a canceled order you may report this to us and we will review it further.
>
> I understand that you will address the issue of the unlimited revisions.
>
> Thank you for the understanding.
>
> Please let me know if there is anything else we can do for you.
>
> Kind regards,

CS closed the ticket after this message.

So I understand that the *ownership rights are transferred back to you* thing
is written in the
[Fiverr's ToS](https://www.fiverr.com/legal-portal/legal-terms/payment-terms-of-service)
but the buyer has not been personally informed by CS. In the ToS, infact,
I read:

> All transfer and assignment of intellectual property to the Buyer shall be subject to full payment and **the delivery may not be used if payment is canceled for any reason.**

## Conclusions

There are also really nice buyers on Fiverr but in this case these are the
results:

- money lost
- HUGE amount of time lost
- anger
- frustration
- *Success score* went from 9 to 8, and then 7 a few days later
- other "hidden" negative metrics, maybe?
- less customers (because of these metrics)

### What to do for the future

- immediately block potential buyers at the first red flag?
- i accept suggestions in the comments

### Suggestions for Fiverr

- find a way to vet the buyers: I know it's difficult, but freelancers, for
  example, need to pass some tests to sell stuff for certain categories
- read all the conversation between the seller and the customer before siding
  with one party: if one of the two parties decides to cancel an order, contact
  the other one for information

## The script

*Note: the script was guaranteed to work at the moment the order was cancelled
(2024-07-28) on a Debian 12 GNU/Linux system. You'll find the accessory files
below.*

{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/fiverr/fiverr-order-cancelled/automate.py" text="automate.py" name="automate.py" is_text=true code_lang="python" %}

{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/fiverr/fiverr-order-cancelled/requirements.txt" text="requirements.txt" name="requirements.txt" is_text=true code_lang="plaintext" %}

{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/fiverr/fiverr-order-cancelled/README.md" text="README.md" name="README.md" is_text=false code_lang="markdown" %}

## Can I help you?

See the [jobs](/jobs/#fiverr-reviews) page.
