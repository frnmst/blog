// SPDX-FileCopyrightText: 2023 Franco Masotti
//
// SPDX-License-Identifier: MIT
#include <stdio.h>
#include <string.h>
#include <stdint.h>

int main(void)
{
    char array[100] = "ab c def g hi";
    char reversed[100];

    // Transform all indices to size_t.
    size_t i = 0, j = 0;
    size_t length = 0;

    int done = 0;

    // Does not include the '\0' character.
    length = strlen(array);

    i = length - 1;
    while (!done)
    {
        reversed[j] = array[i];

        if (i == 0)
            done = 1;

        i--;
        j++;
    }

    reversed[length] = '\0';

    printf("reversed = %s\n", reversed);

    return 0;
}
