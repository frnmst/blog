---
title: ! 'Migrating repositories to GIT LFS'
tags: [git, git-lfs, migration]
updated: 2023-12-08 17:10:00
description: ! "Transforming a normal GIT repository to LFS"
lang: 'en'
images:
  - id: '0'
    filename: 'invalid_git_commit_signature.png'
    alt: 'An invalid GIT commit signature'
---

LFS
[(Large File Support)](https://github.com/git-lfs/git-lfs#git-large-file-storage)
is an extension of GIT useful to save *big* files. These files are saved
outside the repository but are referenced by the repository itself through the
use of pointers.

{% include link_preview.html url="https://git-lfs.com/" %}

Since this is an extension, you have to install `git-lfs` which is not part of
the standard `git` packages.

```shell
apt install git-lfs
```

Setup GIT LFS: this has to be done only once per shell account

```shell
git lfs install
```

All the data of this blog including the images are stored in GIT, so I tried
the `git lfs migrate` tool by following the
[*Migrating existing repository data to LFS*](https://github.com/git-lfs/git-lfs/wiki/Tutorial#migrating-existing-repository-data-to-lfs) tutorial.

```shell
git lfs migrate import --everything --include="*.jpg,*.JPG,*.png,*.PNG,*.tar.gz,*.TAR.GZ,*.gif,*.GIF" --verbose
```

Let's check what has changed.

```
git status

On branch master
Your branch and 'origin/master' have diverged,
and have 467 and 467 different commits each, respectively.
  (use "git pull" to merge the remote branch into yours)
```

Those are __all__ 467 commits of the repository.

```
git push --force
```

This worked: now I had all files on LFS. The GIT history, however, was
re-written to a point that existing GIT commit signatures were now invalid. All
signed commits were marked as suspicious and now had a red color in Gitea. Upom
further inspection, I noticed that GIT commit hashes were now different as
well.

{% include image.html id="0" %}

This defeats the purpose of signing commits!

In the end I decided to archive the original repository and create a new one
with the `.gitattributes` file, like this:

```
rm -rf .git
git init .
git lfs track "*.jpg"
git lfs track "*.JPG"
git lfs track "*.png"
git lfs track "*.PNG"
git lfs track "*.tar.gz"
git lfs track "*.TAR.GZ"
git lfs track "*.gif"
git lfs track "*.GIF"
git add -A
git commit -m "First commit"
git push
```

{% include link_preview.html url="https://software.franco.net.eu.org/frnmst-archives/blog-no-git-lfs" %}

This way I can preserve the original history and commit integrity.

A side effect of using LFS is that I cannot build this website in the same
way I did with plain GIT. The system I described in a
[previous post]({%- include post/post_url_iso639.liquid post_url="2021-02-17-new-hosting" -%}{{ post_url_iso639 }}) doesn't work anymore.

```
bash: line 1: git-lfs-authenticate: command not found: exit status 127
error: failed to push some refs to ...
```

According to this issue there is no easy way to solve this.

{% include link_preview.html url="https://github.com/git-lfs/git-lfs/issues/1044" %}

I opted to move the `post-receive` GIT
hook to a separate script that needs to be manually executed on the server.

```shell
#!/usr/bin/bash -l

TMP_GIT_CLONE="/tmp/blog.franco.net.eu.org"
PUBLIC_WWW='/var/www/blog.franco.net.eu.org'
REPOSITORY='https://software.franco.net.eu.org/frnmst/blog.git'

rm --recursive --force "${TMP_GIT_CLONE}"

# Install Ruby Gems to ~/gems
export GEM_HOME=""${HOME}"/gems"
export PATH="${GEM_HOME}"/bin:"${PATH}"
GEMFILE=""${TMP_GIT_CLONE}"/Gemfile"

# Get the latest commit only.
git clone --depth 1 "${REPOSITORY}" "${TMP_GIT_CLONE}"
BUNDLE_GEMFILE="${GEMFILE}" bundle install
BUNDLE_GEMFILE="${GEMFILE}" bundle exec \
    jekyll build \
        --config="${TMP_GIT_CLONE}"/_config.yml,"${TMP_GIT_CLONE}"/_config_prod.yml \
        --trace \
        --strict_front_matter \
        --verbose \
        --trace \
        --source "${TMP_GIT_CLONE}" \
        --destination "${PUBLIC_WWW}"

rm --recursive --force "${TMP_GIT_CLONE}"
```

Enjoy 🚀
