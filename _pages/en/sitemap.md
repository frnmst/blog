---
layout: page
title: Sitemap
permalink: /sitemap/
description: ! 'Sitemap of blog.franco.net.eu.org'
lang: 'en'
---
## Pages

{% include sitemap/sitemap_pages.html %}

## Posts

Posts are sorted in decreasing date order.

{% include sitemap/sitemap_posts.html %}
