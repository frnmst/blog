---
title: ! 'Scraping essentials: BeautifulSoup - part 1'
tags: [scraping, beautifulsoup, mechanicalsoup, amazon, tutorial, python, amazon-scraper]
updated: 2024-01-11 17:50:00
description: ! 'Scrape product information from Amazon using MechanicalSoup, a frontend of BeautifulSoup'
lang: 'en'
images:
  - id: 'product_title'
    filename: 'product_title.png'
    alt: ! 'Get the product name (a.k.a. title) using the `productTtile` `span` id'
    caption: ! 'The product name/title in the developer console'
    width: '300px'
  - id: 'product_title_better'
    filename: 'product_title_better.png'
    alt: ! 'Better view of the product name/title in the browser developer console'
    caption: ! 'The product name/title highlighted in the developer console'
    width: '300px'
  - id: 'product_price'
    filename: 'product_price.png'
    alt: ! 'The product price in the developer console and on the web page'
    caption: ! 'The product price in the developer console'
    width: '300px'
  - id: 'product_currency_symbol'
    filename: 'product_currency_symbol.png'
    alt: ! "Get the currency symbol of the product's price"
    caption: ! 'The currency symbol'
    width: '300px'
  - id: 'product_availability'
    filename: 'product_availability.png'
    alt: ! "A widget containing the sellers selection options is highlighed in the product page"
    caption: ! 'Get the product availability'
    width: '300px'
  - id: 'product_reviews'
    filename: 'product_reviews.png'
    alt: ! 'Reviews shown with a `span` HTML element'
    caption: ! 'Get the number of total reviews of the product '
    width: '300px'
  - id: 'product_rating'
    filename: 'product_rating.png'
    alt: ! "Average rating is shown near the reviews"
    caption: ! 'Get the average product rating'
    width: '300px'
  - id: 'product_asin'
    filename: 'product_asin.png'
    alt: ! "The ASIN (Amazon Sandard Identification Number) is in an information table near the product specs"
    caption: ! 'Get the product ASIN'
    width: '300px'
  - id: 'before_and_after'
    filename: 'before_and_after.png'
    alt: ! "Original Amazon web page on the left and scraped information on the right"
    caption: ! 'Scraping useful data'
    width: '300px'
---
{% include image.html id="before_and_after" %}

## Table of contents

<!--TOC-->

- [Table of contents](#table-of-contents)
- [Introduction](#introduction)
- [Products](#products)
- [Setting up the agent](#setting-up-the-agent)
- [Fields](#fields)
  - [Product name](#product-name)
  - [Price](#price)
  - [Availability](#availability)
  - [Reviews](#reviews)
  - [Rating](#rating)
  - [ASIN (serial number)](#asin-serial-number)
- [Product value summary](#product-value-summary)
- [Organizing the data](#organizing-the-data)
- [Conclusion](#conclusion)

<!--TOC-->

## Introduction

<!--excerpt_start-->Let's start with this simple scaping exercise. We'll use<!--excerpt_end-->
a Python library called
[MechanicalSoup](https://mechanicalsoup.readthedocs.io/en/stable/ "MechanicalSoup").
This library is based both on
[BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/ "BeautifulSoup")
and
[Requests](https://requests.readthedocs.io/en/latest/ "Requests").

This exercise is divided into multiple posts. You'll find the full Python code
in the final post.

## Products

Something you can do is to scrape some Amazon product information, maybe to
do some comparisons. In these cases you'll typically need this information
for each product:

- the name
- a serial number
- the price
- availability at that moment in time
- rating and reviews

## Setting up the agent

First of all we need to set up some browser options and get the remote web
page. You can do this with a couple of operations. If you do lots of
tests you'll want to rotate the user agent and/or IP often otherwise you'll get
blocked in a short amount of time.

```python
import mechanicalsoup

url = url='https://www.amazon.it/Samsung-Memorie-Tecnologia-Intelligent-Software/dp/B08PC43D78/'
browser_user_agent = 'Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Ubuntu/10.10 Chromium/10.0.648.0 Chrome/10.0.648.0 Safari/534.16'
browser = mechanicalsoup.StatefulBrowser(
        user_agent=browser_user_agent,
)
browser.open(url)
```

The HTML code is now saved in the `browser.page` attribute.

## Fields

### Product name

Let's go to a product page and visually see where the data is.

The first field we'll see is the product name.

{% include image.html id="product_title" %}

Once you expand the `span` HTML tag you can see its content.

{% include image.html id="product_title_better" %}

What we have to do is to filter all the `span` tags that have an
`id="productTitle"` as attribute. In this page there is only one of such
elements, so a simple
[`find`](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#find "BeautifulSoup find method")
operation is sufficient. Using `.text` will get the content inside the tags
and not the whole tag itself.

```python
product_title_raw: str = browser.page.find('span', attrs={"id":'productTitle'}).text.strip()
```

### Price

This same method can be applied to the price field.

{% include image.html id="product_price" %}

On this page there are multiple *prices*, but the specific one I selected has
unique attributes such as the `a-offscreen` class. This one in particular
reports all the price elements in one field. As you can see, the other price
fields right below it represent the price separated into integer
(`a-price-whole`), decimal (`a-price-fraction`), and currency symbol
(`a-price-symbol`) fields.

```python
product_price_raw: str = browser.page.find('span', class_='a-offscreen').text.strip()
```

Ok, so now we have a string `product_price_raw = 56,89€`, which is not really
useful in this format. A common way to normalize money amounts is to use the `decimal`
built-in library. Standard floats should be avoided because operating on them leads to
[inaccurate results](https://docs.python.org/3/tutorial/floatingpoint.html "Floating Point Arithmetic: Issues and Limitations").
**Don't do this**:

```python
product_price: float = float(product_price[:-1].replace(',','.'))
```

Use this function instead, which retains digits and transforms the comma
character to a dot. It is assumed that the originating string does not have
more than one comma or dot character, otherwise transformation to a `Decimal`
will fail.

```python
import decimal
import re

def normalize_price(price_raw: str) -> decimal.Decimal:
    r"""Accepts a stripped string containing numbers, ',' or '.'."""

    price_filtered: list = []
    for c in price_raw:
        if re.match(r'([0-9]|,)', c):
            if c == ',':
                c = '.'
            price_filtered.append(c)

    return decimal.Decimal(''.join(price_filtered))

product_price: decimal.Decimal = normalize_price(product_price_raw)
```

If you are interested in the `float` problem you can also check this out:

```python
>>> p_float = 56.89
>>> p_str = '56.89'
>>> decimal.Decimal(p_float) == decimal.Decimal(p_str)
False
>>> decimal.Decimal(p_float)
Decimal('56.8900000000000005684341886080801486968994140625')
>>> decimal.Decimal(p_str)
Decimal('56.89')
```

Going back to the original matter, we can also extract the currency symbol:

{% include image.html id="product_currency_symbol" %}

Just like before, you can extract this one easily.

```python
product_currency_symbol_raw: str = browser.page.find('span', class_='a-price-symbol').text.strip()
```

### Availability

Another imporant field is the availability. We should be able to filter out
products that are unavailable: you can't buy something that isn't there 😀

This one was trickier: when I compared this page to another one where the
product wasn't available I couldn't find anything in the code different,
pertaining this problem, except the text content where it says: "Product not
available". So I found a *trick*: available products have the seller selection
widget, while unavailable ones do not.

{% include image.html id="product_availability" %}

The first instruction returns the HTML code if it exists or `None` if it does
not. In the end you can condense the answer in a nice boolean.

```python
product_availabile_raw: str = browser.page.find('div', class_='olp-link-widget')
if product_availabile_raw is not None:
    product_availabile_raw = product_availabile_raw.text.strip()
product_availabile: bool = bool(product_availabile_raw)
```

### Reviews

{% include image.html id="product_reviews" %}

The total number of product reviews is just another HTML element.

```python
product_reviews_raw: str = browser.page.find('span', attrs={'id': 'acrCustomerReviewText'}).text.strip()
```

In this case we need to normalize the value, similarly to what we did with
the price:

```python
def normalize_int(int_raw: str) -> int:
    int_filtered: list = []

    for c in int_raw:
        if re.match(r'[0-9]', c):
            int_filtered.append(c)

    return int(''.join(int_filtered))

product_reviews: int = normalize_int(product_reviews_raw)
```

### Rating

{% include image.html id="product_rating" %}

When dealing with the product rating we must start *higher* in the HTML tree.
There is a `div` element callled `averageCustomerReviews` which contains
several other elements including the reviews. This is going to be our starting
point for searching the average reviews. The CSS classes called `a-size-base`
and `a-color-base` are used more than once in the product page so we can't
select them directly.

```python
product_rating_raw: str = browser.page.find('div', attrs={'id': 'averageCustomerReviews'}).find('span', class_='a-size-base a-color-base').text.strip()
```

The `find` operation with `class_='a-size-base a-color-base'`, i.e.:
space-separated classes, will match a tag with exactly those classes, not more,
not less.

Again, we can normalize the rating value to a float, this is practically
identical to the `normalize_price` function:

```python
def normalize_float(float_raw: str) -> float:
    r"""Accepts a stripped string containing numbers, ',' or '.'."""

    float_filtered: list = []
    for c in float_raw:
        if re.match(r'([0-9]|,)', c):
            if c == ',':
                c = '.'
            float_filtered.append(c)

    return float(''.join(float_filtered))

product_rating: float = normalize_float(product_rating_raw)
```

### ASIN (serial number)

I left the ASIN (Amazon Standard Identification Number) as last because I added
this later in the script. There are two ways to get this number:

- get the url of the page, split by `/`, and get the 4th element
- get it from the HTML code

For the sake of this exercise we'll use the second method.

{% include image.html id="product_asin" %}

Just like the rating, we can't get the ASIN directly. We can see that it's in
a specific table row. On the left we have the table header (`th`) with the word
`ASIN` and on the right (`td`) we have the actual value. Once we loop through
all `th`s and find the one with the `ASIN` string we can get its *sibling*
using the
[`find_next_sibling`](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#find-next-siblings-and-find-next-sibling "find_next_sibling - BeautifulSoup")
method.

```python
table_headers = browser.page.select('th.a-color-secondary.a-size-base.prodDetSectionEntry')
asin_raw: str = '<undefined>'
for th in table_headers:
    if th.text.strip() == 'ASIN':
        # ASIN value is in the column (td) of the ASIN table header (th).
        asin_raw = th.find_next_sibling().text.strip()
```

## Product value summary

Ok, so now we have all the data:

- `asin: str`
- `product_title: str`
- `product_price: decimal.Decimal`
- `product_currency_symbol: str`
- `product_availabile: bool`
- `product_reviews: int`
- `product_rating: float`

Each one of these variables has a raw version (`str` data type) as well which
is a mostly-unsanitized version of the HTML text: the only applied operation
for these is [`strip()`](https://docs.python.org/3/library/stdtypes.html#str.strip "strip - Python 3 documentation").

## Organizing the data

As I did in a [previous post]({%- include post/post_url_iso639.liquid post_url="2023-12-06-simpler-yaml-io-in-python" -%}{{ post_url_iso639 }}),
I am going to organize this new data in a neat dataclass. It is very simple:

```python
from dataclasses import dataclass
import pprint

@dataclass
class Product:
    url: str
    asin_raw: str
    asin: str
    title_raw: str
    title: str
    price_raw: str
    price: decimal.Decimal
    currency_symbol_raw: str
    currency_symbol: str
    availabile_raw: str
    availabile: bool
    rating_raw: str
    rating: float
    reviews_raw: str
    reviews: int

# <all the previous code goes here>

p = Product(
    url=url,
    asin_raw=asin_raw,
    asin=asin,
    title_raw=product_title_raw,
    title=product_title,
    price_raw=product_price_raw,
    price=product_price,
    currency_symbol_raw=product_currency_symbol_raw,
    currency_symbol=product_currency_symbol,
    availabile_raw=product_availabile_raw,
    availabile=product_availabile,
    reviews_raw=product_reviews_raw,
    reviews=product_reviews,
    rating_raw=product_rating_raw,
    rating=product_rating,
)

pprint.pprint(p)
```

which results in

```python
Product(url='https://www.amazon.it/Samsung-Memorie-Tecnologia-Intelligent-Software/dp/B08PC43D78/',
        asin_raw='B08PC43D78',
        asin='B08PC43D78',
        title_raw='Samsung 870 EVO SSD, Fattore di forma 2,5", Intelligent '
                  'Turbo Write, software Magician 6, Nero, 500 GB',
        title='Samsung 870 EVO SSD, Fattore di forma 2,5", Intelligent Turbo '
              'Write, software Magician 6, Nero, 500 GB',
        price_raw='€47,89',
        price=Decimal('47.89'),
        currency_symbol_raw='€',
        currency_symbol='€',
        availabile_raw='Nuovo & Usato (143) da55,72€55,72€ \xa0Spedizione '
                       'GRATUITA.',
        availabile=True,
        rating_raw='4,8',
        rating=4.8,
        reviews_raw='34.533 voti',
        reviews=34533)
```

*Note: price discrepancies might happen because of the user agent string, I'm not
really sure...*

Of course, to get single fields you can use the dot notation, such as

```python
>>> print(p.asin)
B08PC43D78
```

And you can also transform the dataclass to a dict

```python
>>> pprint.pprint(vars(p))
{'asin': 'B08PC43D78',
 'asin_raw': 'B08PC43D78',
 'availabile': True,
 'availabile_raw': 'Nuovo & Usato (150) da55,72€55,72€ \xa0Spedizione '
                   'GRATUITA.',
 'currency_symbol': '€',
 'currency_symbol_raw': '€',
 'price': Decimal('47.89'),
 'price_raw': '€47,89',
 'rating': 4.8,
 'rating_raw': '4,8',
 'reviews': 34557,
 'reviews_raw': '34.557 voti',
 'title': 'Samsung 870 EVO SSD, Fattore di forma 2,5", Intelligent Turbo '
          'Write, software Magician 6, Nero, 500 GB',
 'title_raw': 'Samsung 870 EVO SSD, Fattore di forma 2,5", Intelligent Turbo '
              'Write, software Magician 6, Nero, 500 GB',
 'url': 'https://www.amazon.it/Samsung-Memorie-Tecnologia-Intelligent-Software/dp/B08PC43D78/'}
```

## Conclusion

In this post I have shown you how to scrape the most important information from
an Amazon product. In the next blog post we'll see how to perform a search
operation and extract multiple products from a search result page.

🚀
