#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2023 Franco Masotti
#
# SPDX-License-Identifier: MIT

import random
import sys
import time


def zombie(w: int) -> int:
    # Initial value.
    zombie: float = 100.0
    done: bool = False
    hit_wall: int = 0

    start = time.time()
    while not done:
        if zombie > w and time.time() - start >= 1.0:
            # Previous zombie hit the wall.
            zombie = random.random()
            hit_wall += 1
            start = time.time()

        if zombie <= w:
            done = True

    return hit_wall


if __name__ == '__main__':
    iterations: int = int(sys.argv[1])
    w: float = float(sys.argv[2])
    i: int = 0

    assert w <= 1.0

    # Clock
    o: int = 0
    # Arrival time.
    ta: int = 0
    # Service time.
    ts: int = 0
    # Queue length.
    c: int = 1

    queue_empty: bool = False
    while i < iterations:
        print('iteration: ' + str(i + 1))

        # First case with special case coverage.
        if ta < ts or queue_empty:

            if queue_empty:
                o += ta
                c += 1
                queue_empty = False
            else:
                ts -= ta

            ta = zombie(w)

        # Second case.
        else:
            ta -= ts
            o += ts
            c -= 1
            ts = zombie(w)

        if c == 0:
            queue_empty = True

        print('c: ' + str(c))

        i += 1
