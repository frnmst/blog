---
layout: page
title: ! 'À propos'
permalink: /fr/about/
description: ! 'À propos et contacts de Franco Masotti'
lang: 'fr'
---

Ce blog est maintenu par Franco Masotti. Vous trouverez des articles sur
l'informatique, les logiciels, le matériel, les hacks, la technologie, etc...

J'espère que ces articles s'avéreront utiles.

## Contacts

| Dépôts |
|:-------|
| [Canonical repositories](https://software.franco.net.eu.org/frnmst) |
| [Codeberg](https://codeberg.org/frnmst) |
| [Framagit](https://framagit.org/frnmst) |
| [GitHub](https://github.com/frnmst) (voir [cette articles]({%- include post/post_url_iso639.liquid post_url="2021-09-02-quitting-github" -%}{{ post_url_iso639 }})) |

| Courrier électronique |
|:----------------------|
| [my first name].[my last name]@tutanota.com |
| [my first name].[my last name].1@protonmail.com |

| Autre |
|:------|
| [Solve Computer Science YouTube channel](https://www.youtube.com/channel/UC2rr0LbIuy34JHEoCndmKiA) |
| [anonymous share via Nextcloud](https://cloud.franco.net.eu.org/s/QyqgfCPHdemjCcp) |
| Nextcloud (Open Cloud Mesh protocol) à [frnmst@cloud.franco.net.eu.org](https://cloud.franco.net.eu.org)
| [Keyoxide ID](https://keyoxide.org/396da54dc8019e4f4522cbd5a3fa3c2b4230215a) |

## Éducation

- Baccalauréat en informatique @ Université of Ferrara: [*Integrazione di una applicazione web con strumenti per la statistica*](https://software.franco.net.eu.org/frnmst-archives/thesis)
- [ORCID iD](https://orcid.org/0000-0002-1736-3858)
- [CS198.1x: Bitcoin and Cryptocurrencies](https://courses.edx.org/certificates/2849569d6b2e422e8652b5ff93f8ea09)

## Mes clés publiques PGP

- [Clé publique pour signer les commits Git](/pubkeys/pgp_pubkey_since_2019.txt)
- [Clé publique de Keyoxide (ID)](/pubkeys/pgp_pubkey_frnmst_keyoxide.txt)
