---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fattura-elettronica-reader
software_name_python_module: fattura_elettronica_reader
software_version: 3.0.0
software_version_raw: 000003.000000.000000
release_timestamp: 2022-01-17 21:33:52
is_on_pypi: true
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- All variables are in [snake case](https://en.wikipedia.org/wiki/Snake_case)
  including the ones used in the configuration file. If you have an existing
  configuration file you have to update it manually or delete it.
- Changed an asset file.
- Updated file checksum variables.
- Updated copyright headers.
- Automatically create the destination directory.
- Changed API of these functions:
  - `fattura_elettronica_reader.api.create_appdirs`

### Fixed

- Fixed relative paths.
