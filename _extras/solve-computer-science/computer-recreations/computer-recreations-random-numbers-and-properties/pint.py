#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2023 Franco Masotti
#
# SPDX-License-Identifier: MIT

import math
import multiprocessing
import random
import sys


def compute_vector_length(x: float, y: float) -> float:
    """
    This method is equivalent.

    return math.sqrt((x ** 2) + (y ** 2))
    """
    return math.hypot(x, y)


def fire(iterations: int) -> int:
    i: int = 0
    hit: int = 0

    circle_radius: float = 1.0

    while i < iterations:
        x = random.random()
        y = random.random()

        if compute_vector_length(x, y) <= circle_radius:
            hit += 1

        i += 1

    return hit


if __name__ == '__main__':
    iterations: int = int(sys.argv[1])

    # Repeate the same problem `processes` time.
    processes: int = int(sys.argv[2])

    args = [iterations for x in range(0, processes)]

    concurrent_workers: int = multiprocessing.cpu_count()
    with multiprocessing.Pool(processes=concurrent_workers) as pool:
        try:
            res = pool.imap_unordered(fire, args)
        except (KeyboardInterrupt, InterruptedError):
            pool.terminate()
            pool.join()
        else:
            pool.close()
            pool.join()

    hits: int = sum(res)
    # Attempts.
    total_points: int = sum(args)
    pi_approx: float = (hits / total_points) * 4.0

    print('computed pi = {:.32f}'.format(pi_approx))
    print('percent shift from math.pi = ' +
          str(abs(100.0 - ((pi_approx / math.pi) * 100.0))))
