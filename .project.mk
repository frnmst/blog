PROJECT_NAME := blog
PYTHON_MODULE_NAME := blog

MAKEFILE_SOURCE := https://software.franco.net.eu.org/frnmst/python-makefile/raw/branch/master/Makefile.example
bootstrap:
	curl -o Makefile $(MAKEFILE_SOURCE)

PORT := 3050

# Add extra targets here.

ruby:
	rm -f Gemfile.lock
	bundle install

build:
	cd components/svelte/search && npm run build
	bundle exec jekyll build --config=_config.yml,_config_dev.yml --trace --strict_front_matter --verbose --future

build-components:
	cd components/svelte/search && npm install && npm run build

serve: build-components
	bundle exec jekyll serve --config=_config.yml,_config_dev.yml --trace --future

serve-global: build-components
	bundle exec jekyll serve --config=_config.yml,_config_dev.yml --trace --host=0.0.0.0 --port=$(PORT) --future

serve-global-verbose: build-components
	bundle exec jekyll serve --config=_config.yml,_config_dev.yml --trace --verbose --host=0.0.0.0 --port=$(PORT) --future

# Override targets in the Makefile.
OVERRIDE_CLEAN := true
clean::
	bundle exec jekyll clean
	rm -rf _site .jekyll_cache .jekyll-cache
