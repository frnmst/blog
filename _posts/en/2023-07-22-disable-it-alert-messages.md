---
title: Disable IT-alert notifications
tags: [it-alert, cell-broadcast, android, video, youtube]
updated: 2023-07-22 16:20:00
description: How to disable reception of IT-alert messages through Android menus and ADB commands
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/ib_a1oLWnR4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The IT-alert public warning system is uses the cell broadcasr technology to send notifications to mobile phones and it's currently (2023) being tested in Italy.

There are two ways to disable IT-altert cell broadcast messages on an Android phone. I tested them both at the same time on my smartphone and I didn't receive any message the day of the test.

Sottotitoli in Italiano.
