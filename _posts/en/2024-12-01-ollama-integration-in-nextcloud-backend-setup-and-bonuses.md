---
title: ! 'Ollama integration in Nextcloud (backend setup and bonuses)'
tags: [ai, chatbot, nextcloud, ollama, tutorial, youtube, video]
related_links: ['https://www.youtube.com/watch?v=iPXSsgrEuCY']
updated: 2024-12-01 00:00:00
description: ! "Second part of the tutorial on how to set up the Nextcloud OpenAI integration with Ollama"
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/iPXSsgrEuCY?si=xUEr_qV3Z46i97tp" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<!--TOC-->

- [Summary](#summary)

<!--TOC-->

## Summary

<!--excerpt_start-->In the second part of my Nextcloud A.I. integration video, I continue from where<!--excerpt_end--> we left off with the Ollama setup. I explain how to use the A.I. system from any device that supports the OpenAI API, even though the full implementation isn't complete yet. We will be testing an Android app that allows access to the Ollama instance from anywhere, which adds a layer of convenience for users on the go.

I provide a detailed overview of the basic setup, including creating a new Docker-compose file and a Systemd service to run the Ollama container automatically. I highlight some important environment variables that are not well-documented but are crucial for optimizing the Ollama performance. For instance, I discuss how to keep an AI model loaded in RAM indefinitely and the importance of the DEBUG variable for troubleshooting any issues that arise during operation.

As I delve deeper into the setup, I emphasize the need to manage CPU performance for inference tasks. I share warnings about potential conflicts with existing tools and the importance of setting the CPU governor to performance mode. I also touch on the implications of disabling CPU mitigations, which can lead to significant performance gains but may expose the system to security risks. Throughout this process, I ensure that everything is functioning smoothly, although I note that the generated results can sometimes be a bit off (A.I. hallucination).

Finally, I discuss how to securely expose the Ollama instance on the internet using an HTTPS backend with authentication. I demonstrate how to set up an Apache server to manage access through a token, ensuring that only authorized users can interact with the A.I. system. I introduce a new app called GPTMobile, available on F-Droid, which supports various A.I. systems, including Ollama. Share, like, and subscribe for more content.
