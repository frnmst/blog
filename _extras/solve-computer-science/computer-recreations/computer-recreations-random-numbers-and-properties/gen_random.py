#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2023 Franco Masotti
#
# SPDX-License-Identifier: MIT

import sys

# For example
# ./gen_random.py $(date +%N) 100
if __name__ == '__main__':
    seed: int = int(sys.argv[1])
    iterations: int = int(sys.argv[2])
    n = seed
    m = 325
    k = 1913
    p = 18541

    for i in range(0, iterations):
        n = ((n * m) + k) % p

    print(n)
