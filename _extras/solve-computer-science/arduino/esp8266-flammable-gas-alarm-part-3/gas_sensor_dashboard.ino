#include <Adafruit_SSD1306.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <DHT.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <WiFiClientSecure.h>


// 1 to enable serial print debug, 0 to disable.
#define DEBUG 0

// WiFi configuration.
const char* ssid = "<You WiFi network name>";
const char* password = "<You WiFi password>";

// Static IP configuration.
IPAddress local_IP(192, 168, 0, 32);
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress dns(192, 168, 0, 100);

#define IGNORE_MISSING_WIFI  true
#define SENSOR_READING_TIMEOUT_MS 10000

// This name must be unique between all boards using this program.
#define BOARD_ID               "boiler room"

#define HTTPS_CLIENT_HOST      "iot.my.domain"
#define HTTPS_CLIENT_PORT      443
#define HTTPS_CLIENT_ENDPOINT  "/sensors/data"

// See the wsgi.py file for information on how to generate this token.
#define HTTPS_CLIENT_AUTHORIZATION_BEARER_TOKEN     "e9b1b5d9-b7a0-409a-b21c-b84eb6f5a3f0"

#define HTTPS_CLIENT_SEND_DATA_FREQUENCY_MS         60000

// Uncomment this line to see the response from the server on serial.
// #define HTTPS_CLIENT_DEBUG

// NTP.
#define NTP_SERVER            "ntp1.inrim.it"

// Sensors and outputs.
#define MQ2_DIGITAL_PIN                   14
#define MQ2_ANALOG_PIN                    A0
#define BUZZER_PIN                        0
#define BOARD_LED                         2
#define DHT_PIN                           12
#define DHT_TYPE                          DHT11
#define WIRE                              Wire

// Fine tune calibration.
#define TEMPERATURE_CELSIUS_OFFSET                -3.10
#define HUMIDITY_PERCENT_OFFSET                   0.0

/* 0 = 0°
   1 = 90°
   2 = 180°
   3 = 270°
*/
#define DISPLAY_ROTATION 2

// MQ_2 is connected to the 5 Volts rail.
#define MQ_2_SENSOR_VOLTS                 5

// Higer value means less sensitive
#define GAS_ALARM_VOLTS_THRESHOLD         1.25

// Raise the alarm if the raw value increases after GAS_ALARM_TENDENCY_ITERATIONS.
#define GAS_ALARM_TENDENCY_ITERATIONS     512
// If set to true, do not raise an alarm if the gas tendency passes the GAS_ALARM_TENDENCY_ITERATIONS.
#define IGNORE_GAS_TENDENCY               true

// Rolling readings for measuring average
#define TOTAL_SENSOR_READINGS             64

#define BUZZER_SENSOR_ERROR_ITERATIONS    10
#define BUZZER_SENSOR_ERROR_DELAY_MS      500
#define MQ_2_WARM_UP_MS                   120000

// Devices.
Adafruit_SSD1306 display = Adafruit_SSD1306(128, 64, &WIRE);
DHT dht(DHT_PIN, DHT_TYPE);

// Data structures.
float gas_sensor_readings[TOTAL_SENSOR_READINGS], temperature_sensor_readings[TOTAL_SENSOR_READINGS], humidity_sensor_readings[TOTAL_SENSOR_READINGS];
float average_gas_reading, average_temperature_reading, average_humidity_reading, gas_total = 0.0, temperature_total = 0.0, humidity_total = 0.0;
float average_gas_reading_volts;
char average_gas_reading_normalized_char[32];
static int gas_index = 0, temperature_index = 0, humidity_index = 0;
int gas_state, gas_tendency = 0;
bool inverted = false, gas_detected = false;
unsigned long previous_millis = 0, previous_millis_https_request;
WiFiClientSecure https_client;
// Create a UDP instance
WiFiUDP udp;
// Create an NTPClient instance: always work in UTC.
NTPClient time_client(udp, NTP_SERVER, 0);
int check_wifi = 1;

String iso_8601_timestamp = "";

#if DEBUG
    #define prt(x) Serial.print(x)
    #define prtn(x) Serial.println(x)
#else
    #define prt(x)
    #define prtn(x)
#endif


String epoch_to_iso_8601(unsigned long epoch)
{
    // Create a time structure.
    struct tm timeinfo;

    // Cast.
    time_t epoch_time = (time_t) epoch;

     // Convert epoch to UTC time.
    gmtime_r(&epoch_time, &timeinfo);

    // Format the time as ISO 8601.
    char buffer[30];
    strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S %z", &timeinfo);

    return String(buffer);
}

String get_json()
{
    JsonDocument doc;
    String output;

    JsonObject board = doc["board"].to<JsonObject>();

    board["id"] = BOARD_ID;

    JsonObject sensors = doc["sensors"].to<JsonObject>();

    // Transform the vchar array to float.
    sensors["gas_concentration_volts"] = atof(average_gas_reading_normalized_char);

    sensors["temperature_celsius"] = average_temperature_reading;
    sensors["humidity_percent"] = average_humidity_reading;
    if (gas_detected)
        sensors["gas_state_bool"] = true;
    else
        sensors["gas_state_bool"] = false;

    JsonObject computations = doc["computations"].to<JsonObject>();
    computations["gas_tendency"] = gas_tendency;
    computations["timestamp_utc"] = iso_8601_timestamp;

    serializeJson(doc, output);

    return output;
}

void clear_display()
{
    display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(SSD1306_WHITE);
    display.setCursor(0,0);
}

// Function to read sensor value and update index and total
void update_sensor_data(float sensor_value, float* sensor_readings, int& index, float& total, int& tendency)
{
    float prev_total = total;
    total = total - sensor_readings[index];
    sensor_readings[index] = sensor_value;
    total = total + sensor_readings[index];

    // Move to the next position in the array.
    index = (index + 1) % TOTAL_SENSOR_READINGS;

    // Specific to the gas readings only.
    if (prev_total > total)
        tendency += 1;
    else if (prev_total == total)
        // Flatten if the reading does not change.
        if (tendency < 0)
            tendency = 0;
        else
            // Adjust to -10% the alarm value.
            tendency -= (int) (GAS_ALARM_TENDENCY_ITERATIONS * 0.10);
        else if (prev_total < total)
            tendency -= 1;
}

bool connect_wifi()
{
    // Connect to Wi-Fi with static IP.
    WiFi.config(local_IP, gateway, subnet, dns);
    WiFi.begin(ssid, password);

    int attempts = 0;

    // Wait for connection
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(1000);
        prtn("Connecting to WiFi...");
        if (IGNORE_MISSING_WIFI && attempts >= 10)
            return false;
        attempts++;
    }
    return true;
}

void setup()
{
    Serial.begin(9600);

    if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
        prtn(F("SSD1306 allocation failed"));
        for(;;);
    }
    display.display();

    bool wifi = connect_wifi();

    delay(1000);
    clear_display();
    display.display();
    display.setRotation(DISPLAY_ROTATION);

    if (!wifi)
    {
        clear_display();
        display.println("NO WiFi, continuing boot...");
        display.display();
        delay(5000);
    }

    dht.begin();

    pinMode(MQ2_DIGITAL_PIN, INPUT);
    pinMode(BUZZER_PIN, OUTPUT);

    prtn("Warming up MQ2 sensor");

    for (int i = 0; i < MQ_2_WARM_UP_MS / 1000; i++)
    {
        clear_display();
        display.println("Warming");
        display.println(i);
        display.println("of");
        display.println(MQ_2_WARM_UP_MS / 1000);
        display.display();
        delay(1000);
    }

    clear_display();
    display.display();
    display.println("sampling");

    for (int i = 0; i < TOTAL_SENSOR_READINGS; i++)
    {
        clear_display();
        display.println("sample ");
        display.println(i + 1);
        display.println("of ");
        display.println(TOTAL_SENSOR_READINGS);
        display.display();

        prt("sampling ");
        prt(i + 1);
        prt(" of ");
        prtn(TOTAL_SENSOR_READINGS);

        gas_sensor_readings[i] = analogRead(MQ2_ANALOG_PIN);
        temperature_sensor_readings[i] = dht.readTemperature() + TEMPERATURE_CELSIUS_OFFSET;
        humidity_sensor_readings[i] = dht.readHumidity() + HUMIDITY_PERCENT_OFFSET;
        gas_total += gas_sensor_readings[i];
        temperature_total += temperature_sensor_readings[i];
        humidity_total += humidity_sensor_readings[i];
        delay(SENSOR_READING_TIMEOUT_MS);
    }

    average_gas_reading = gas_total / TOTAL_SENSOR_READINGS;
    average_temperature_reading = temperature_total / TOTAL_SENSOR_READINGS;
    average_humidity_reading = humidity_total / TOTAL_SENSOR_READINGS;

    time_client.begin();
    time_client.update();

    // Avoid checking certificate auth.
    https_client.setInsecure();
}

void invert_display_colors()
{
    inverted = !inverted;
    if (inverted)
        // Invert colors.
        display.invertDisplay(true);
    else
        display.invertDisplay(false);
}

void loop()
{
    int dummy = 0;
    unsigned long current_millis = millis();

    // Use non-blocking code.
    if (current_millis - previous_millis >= SENSOR_READING_TIMEOUT_MS)
    {
        previous_millis = current_millis;

        display.clearDisplay();
        display.display();
        delay(10);

        gas_state = digitalRead(MQ2_DIGITAL_PIN);
        float gas_concentration = (float) analogRead(MQ2_ANALOG_PIN);
        float temperature_celsius = dht.readTemperature() + TEMPERATURE_CELSIUS_OFFSET;
        float humidity_percent = dht.readHumidity() + HUMIDITY_PERCENT_OFFSET;

        // Check if the DHT11 sensor has problems.
        if (isnan(temperature_celsius) || isnan(humidity_percent))
        {
            // If there is a reading problem get the latest available value
            // which is presumably not NaN.
            prtn("Warning DHT11 is NaN");
            temperature_celsius = temperature_sensor_readings[temperature_index];
            humidity_percent = humidity_sensor_readings[humidity_index];

            for (int i = 0; i < BUZZER_SENSOR_ERROR_ITERATIONS; i++)
            {
                digitalWrite(BUZZER_PIN, HIGH);
                delay(BUZZER_SENSOR_ERROR_DELAY_MS);
                digitalWrite(BUZZER_PIN, LOW);
                delay(BUZZER_SENSOR_ERROR_DELAY_MS);
            }
        }

        update_sensor_data(gas_concentration, gas_sensor_readings, gas_index, gas_total, gas_tendency);
        average_gas_reading = gas_total / TOTAL_SENSOR_READINGS;
        update_sensor_data(temperature_celsius, temperature_sensor_readings, temperature_index, temperature_total, dummy);
        average_temperature_reading = temperature_total / TOTAL_SENSOR_READINGS;
        update_sensor_data(humidity_percent, humidity_sensor_readings, humidity_index, humidity_total, dummy);
        average_humidity_reading = humidity_total / TOTAL_SENSOR_READINGS;

        average_gas_reading_volts = (average_gas_reading / 1023) * MQ_2_SENSOR_VOLTS;
        sprintf(average_gas_reading_normalized_char, "%.6f", average_gas_reading_volts);

        iso_8601_timestamp = epoch_to_iso_8601(time_client.getEpochTime());

        clear_display();

        if (gas_state == HIGH)
            prtn("The gas is NOT present");
        else
        {
            prtn("The gas is present");
            display.println("GAS!");
        }

        prt("MQ2 sensor avg V: ");
        prtn(average_gas_reading_volts);
        prtn(average_gas_reading_normalized_char);
        prtn(average_temperature_reading);
        prtn(average_humidity_reading);
        prtn(gas_tendency);
        prtn(iso_8601_timestamp);

        display.print(average_gas_reading_normalized_char);
        display.println(" V");
        display.print("Gt: ");
        display.println(gas_tendency);
        display.print(average_temperature_reading);
        display.println(" oC");
        display.print(average_humidity_reading);
        display.println(" %");

        display.display();

        if (gas_state == LOW
            || average_gas_reading_volts >= GAS_ALARM_VOLTS_THRESHOLD
            || (!IGNORE_GAS_TENDENCY && gas_tendency >= GAS_ALARM_TENDENCY_ITERATIONS)
        )
            gas_detected = true;
        else
            gas_detected = false;

        if (gas_detected)
        {
            for (int i = 0; i < BUZZER_SENSOR_ERROR_ITERATIONS; i++)
            {
                digitalWrite(BUZZER_PIN, HIGH);
                delay(BUZZER_SENSOR_ERROR_DELAY_MS);
                digitalWrite(BUZZER_PIN, LOW);
                delay(BUZZER_SENSOR_ERROR_DELAY_MS);
                invert_display_colors();
            }
        }
    }

    if (current_millis - previous_millis_https_request >= HTTPS_CLIENT_SEND_DATA_FREQUENCY_MS)
    {
        invert_display_colors();
        previous_millis_https_request = current_millis;


        // Avoid busy waiting if not connected.
        if (WiFi.status() == WL_CONNECTED)
        {
            // Update the time.
            time_client.update();

            prt("connecting to ");
            prtn(HTTPS_CLIENT_HOST);
            if (!https_client.connect(HTTPS_CLIENT_HOST, HTTPS_CLIENT_PORT))
                prtn("connection failed");

            // Create the PUT request.
            String json_data = get_json();
            String request = String("PUT ") + HTTPS_CLIENT_ENDPOINT + " HTTP/1.1\r\n" +
                             "Host: " + HTTPS_CLIENT_HOST + "\r\n" +
                             "User-Agent: ESP-8266\r\n" +
                             "Accept: */*\r\n" +
                             "Content-Type: application/json\r\n" +
                             "Content-Length: " + json_data.length() + "\r\n" +
                             "Authorization: Bearer " + HTTPS_CLIENT_AUTHORIZATION_BEARER_TOKEN + "\r\n"
                             "Connection: close\r\n\r\n" +
                             json_data;

            // Send the request.
            https_client.print(request);

            #ifdef HTTPS_CLIENT_DEBUG
                prtn(request);
                // Read the response.
                while (https_client.connected() || https_client.available())
                {
                    if (https_client.available())
                    {
                        String line = https_client.readStringUntil('\n');
                        prtn(line);
                    }
                }
            #endif
        }
        else
        {
            check_wifi++;
        }

        Serial.println("running");
    }

    if (check_wifi % 10 == 0 && WiFi.status() != WL_CONNECTED)
    {
        prtn("WiFi is disconnected");
        invert_display_colors();
        display.println("NO WiFi");
        display.println("retrying...");
        display.display();
        delay(1000);
        WiFi.reconnect();
        clear_display();
        check_wifi = 1;
    }
}
