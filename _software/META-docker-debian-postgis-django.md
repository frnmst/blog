---
meta: true
software_name: docker-debian-postgis-django
archived: false
repositories:
  - label: 'canonical'
    url: 'https://software.franco.net.eu.org/frnmst/docker-debian-postgis-django'
  - label: 'codeberg'
    url: 'https://codeberg.org/frnmst/docker-debian-postgis-django'
  - label: 'framagit'
    url: 'https://framagit.org/frnmst/docker-debian-postgis-django'
documentation:
  - label: 'software.franco.net.eu.org/frnmst/docker-debian-postgis-django#docker-debian-postgis-django'
    url: 'https://software.franco.net.eu.org/frnmst/docker-debian-postgis-django#docker-debian-postgis-django'
  - label: 'changelog'
    url:
---
