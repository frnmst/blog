#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2023 Franco Masotti
#
# SPDX-License-Identifier: MIT

import random
import sys


def view(matrix: list, rows: int, cols: int):
    print()
    for i in range(0, rows):
        for j in range(0, cols):
            if matrix[i][j] == 1:
                print('*', end='')
            else:
                print('.', end='')
        print()


def result(matrix: list, rows: int, cols: int) -> tuple:
    b: int = 0
    w: int = 0

    for i in range(0, rows):
        for j in range(0, cols):
            if matrix[i][j] == 1:
                b += 1
            else:
                w += 1

    return b, w


if __name__ == '__main__':
    rows: int = int(sys.argv[1])
    cols: int = int(sys.argv[2])
    # Number of opinions.
    n: int = int(sys.argv[3])
    # Reverse vote.
    reverse: bool = bool(int(sys.argv[4]))
    total_elections: int = int(sys.argv[5])
    enable_view: int = int(sys.argv[6])

    white_wins: int = 0
    black_wins: int = 0
    unanimous_white_wins: int = 0
    unanimous_black_wins: int = 0
    even: int = 0

    i: int = 0
    for i in range(0, total_elections):
        # rows x cols matrix.
        voters = [[random.randint(0, 1) for k in range(0, cols)]
                  for l in range(0, rows)]

        j: int = 0
        while j < n:
            # Row.
            r: int = random.randint(0, rows - 1)
            # Col.
            c: int = random.randint(0, cols - 1)

            # Neigh.
            neigh_r: int = r
            neigh_c: int = c

            # This is very inefficient but it works.
            while neigh_r == r and neigh_c == c:
                neigh_r = random.randint(r - 1, r + 1) % rows
                neigh_c = random.randint(c - 1, c + 1) % cols

            if reverse:
                neigh_vote = int(not voters[neigh_r][neigh_c])
            else:
                neigh_vote = voters[neigh_r][neigh_c]

            voters[r][c] = neigh_vote
            if enable_view == 1:
                view(voters, rows, cols)

            j += 1

        b, w = result(voters, rows, cols)

        if w == rows * cols:
            unanimous_white_wins += 1
        elif b == rows * cols:
            unanimous_black_wins += 1
        elif w > b:
            white_wins += 1
        elif b > w:
            black_wins += 1
        else:
            even += 1

    people: int = rows * cols
    print(
        'people, opinions_changes, total_elections, unanimous_white_wins, unanimous_black_wins, white_wins, black_wins, even'
    )
    print(people, n, total_elections, unanimous_white_wins,
          unanimous_black_wins, white_wins, black_wins, even)
