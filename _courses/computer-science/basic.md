---
layout: course
title: ! 'BASIC courses'
permalink: /courses/computer-science/basic/
description: ! 'BASIC courses'
enable_markdown: true
lang: 'en'
courses:
  comment: ! 'Everything about BASIC.'
  course:
  - subject: ! 'BASIC programming'
    comment: ! 'Generic videos about BASIC'
    data:
      - id: 0
        type: video
        title: ! 'Corso BASIC RAI - Episodio 1 (1986): Introduzione alla Programmazione'
        comment: ! 'An interesting document by RAI about BASIC programming.'
        embed_html: |
          <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/e3d-RwD3_Qw?si=k6VKXILi4Jf3wvOp" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

        backup_url: ! 'https://assets.franco.net.eu.org/video/blog.franco.net.eu.org/Corso%20BASIC%20RAI%20-%20Episodio%201%20(1986)%ef%bc%9a%20Introduzione%20alla%20Programmazione%20%5be3d-RwD3_Qw%5d.webm'

---
