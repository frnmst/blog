---
title: ! 'ESP8266 flammable gas alarm'
tags: [circuit, esp8266, mq-2, gas detector, gas alarm, tutorial, youtube, video]
related_links: ['https://www.youtube.com/watch?v=7dPdnuSxaQA']
updated: 2024-08-30 17:05:00
description: ! "In this video we'll see how to create a simple gas detector with alarm using a microcontroller and a sensor."
lang: 'en'
images:
  - id: 'gas_detector'
    filename: 'gas_detector.png'
    alt: ! 'Gas detector circuit schema'
    caption: ! "The circuit schema of the ESP8266 gas detector"
    width: '300px'
---

<iframe width="280px" height="157px" src="https://www.youtube.com/embed/7dPdnuSxaQA?si=Cpy393u4MnBwyfz9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<!--TOC-->

- [Summary](#summary)
- [Arduino sketch](#arduino-sketch)
- [KiCad schematics](#kicad-schematics)

<!--TOC-->

## Summary

<!--excerpt_start-->In this video, I introduce a project focused on creating a simple gas detector using a breadboard. The device<!--excerpt_end--> is designed to trigger an alarm when it detects specific gas levels. After powering up the microcontroller, an ESP8266 D1 mini, the system must wait for the MQ-2 gas detector to heat up, during which the initial readings are not reliable.

I set a 60-second timer to ensure the sensor is ready before taking multiple samples, including readings from a DHT11 temperature and humidity sensor. The data is displayed on a classical OLED screen, where I show various sensor readings, including moving averages to avoid sudden spikes. I explain the analog and digital outputs of the MQ-2 sensor, noting that while they are affordable, they are not the best choice for accurate gas concentration measurements.

As I delve into the sensor's datasheet, I discuss how factors like gas type, temperature, and humidity influence the readings. I clarify that my primary goal is to trigger an alarm when flammable gas is detected, rather than focusing on precise PPM values. I also describe how the gas tendency is calculated and how it can be used to trigger alarms while minimizing false positives.

Finally, I touch on the importance of mitigating OLED display burn-in through color inversion and display clearing. Although I haven't implemented a Wi-Fi dashboard yet, I've made the project more stable by moving it to a mini breadboard, which you'll see in the next video.

{% include image.html id="gas_detector" %}

## Arduino sketch

{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/solve-computer-science/arduino/esp8266-flammable-gas-alarm/sketch.ino" text="schetch.ino" name="schetch.ino" is_text=true code_lang="cpp" %}

## KiCad schematics

*Note: the sketch shows an MQ-6 sensor because the MQ-2 was not available. I
consider A1 the analog, and B1 the digital outputs respectively.*

{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/solve-computer-science/arduino/esp8266-flammable-gas-alarm/gas_sensor.kicad_sch" text="gas_sensor.kicad_sch" name="gas_sensor.kicad_sch" is_text=true code_lang="plaintext" %}
