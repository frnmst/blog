---
title: ! '3 tips to reduce computer-induced eye strain'
tags: [eye-strain, computer-eye-strain, firefox, terminal, kde, youtube, video]
related_links: ['https://www.youtube.com/watch?v=v5qLzV4FcbM']
updated: 2024-03-04 21:47:00
description: ! "In this video we'll see 3 tips to improve eye strain reduction while working at the computer."
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/v5qLzV4FcbM?si=QMLnWJ7tE1eSBFYk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

If you work a lot at the computer you might suffer from eye strain.

This video presents you three simple tips that might improve the situation if
you are a GNU/Linux user.
