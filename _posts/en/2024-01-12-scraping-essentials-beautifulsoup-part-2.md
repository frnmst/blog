---
title: ! 'Scraping essentials: BeautifulSoup - part 2'
tags: [scraping, beautifulsoup, amazon, mechanicalsoup, python, tutorial, amazon-scraper]
updated: 2024-01-13 18:52:00
description: ! 'Scrape product information from Amazon using MechanicalSoup, a frontend of BeautifulSoup'
lang: 'en'
images:
  - id: 'scraping_result'
    filename: 'scraping_result.png'
    alt: ! 'A comparison between a search result page and its scraped data on a terminal'
    caption: ! 'Scraping multiple product information into a single data structure'
    width: '300px'
  - id: 'query_result'
    filename: 'query_result.png'
    alt: ! 'A search result page of amazon.it is displayed. `ssd` is the search term'
    caption: ! 'A normal search on Amazon using `ssd` as keyword'
    width: '300px'
  - id: 'filtering_single_elements'
    filename: 'filtering_single_elements.png'
    alt: ! 'Developer console opened to show the div element corresponding to the first displayed product'
    caption: ! 'Identification of the single products'
    width: '300px'
  - id: 'another_element'
    filename: 'another_element.png'
    alt: ! 'Developer console opened to show the div element corresponding to the second displayed product'
    caption: ! 'Identifying the second product'
    width: '300px'
  - id: 'getting_the_product_url'
    filename: 'getting_the_product_url.png'
    alt: ! 'Developer console opened to show the `a` tag of the first product'
    caption: ! 'Looking at the `a` tag of the product'
    width: '300px'
  - id: 'invalid_link'
    filename: 'invalid_link.png'
    alt: ! 'Developer console opened to show an `a` tag triggered by an event and not belonging to a product'
    caption: ! 'A non-product link'
    width: '300px'
---
{% include image.html id="scraping_result" %}

<!--excerpt_start-->We'll start from what we
[learned in the previous post]({%- include post/post_url_iso639.liquid post_url="2024-01-11-scraping-essentials-beautifulsoup" -%}{{ post_url_iso639 }} "{{ post_title_iso639 }}")<!--excerpt_end-->. You can conveniently wrap all that
code as a single function that takes a URL and the browser agent string as
input.

## Table of contents

<!--TOC-->

- [Table of contents](#table-of-contents)
- [Product search results page](#product-search-results-page)
- [Identifying the products](#identifying-the-products)
  - [Getting the product URLs](#getting-the-product-urls)
- [Dataclass](#dataclass)
- [Considerations](#considerations)
  - [Sleeping?](#sleeping)
  - [User agent rotation](#user-agent-rotation)
  - [IP rotation](#ip-rotation)
  - [All data is in the search page](#all-data-is-in-the-search-page)
- [Conclusion](#conclusion)

<!--TOC-->

## Product search results page

Now we can extend the single product information extraction. The aim here is to
get all the products' URLs from a search page so we can loop them and extract
the single product data.

Let's make a query in the search bar and see how each product is displayed.
We'll try with `ssd` as keyword.

{% include image.html id="query_result" %}

Just like before, we need to set up the virtual browser: the URL of the
search page is a copy-paste from a query I did manually with Firefox.

```python
url: str = 'https://www.amazon.it/s?k=ssd&__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1P5V65HO3OOBJ&sprefix=ssd%2Caps%2C88&ref=nb_sb_noss_1'
browser_user_agent: str = 'Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Ubuntu/10.10 Chromium/10.0.648.0 Chrome/10.0.648.0 Safari/534.16'

browser = mechanicalsoup.StatefulBrowser(
        user_agent=browser_user_agent,
)
browser.open(url)
```

Ok, so now we need an empty list to store the products.

```python
all_products: list[Product] = []
```

## Identifying the products

We'll use the *image* `div`s to identify the different products.

{% include image.html id="filtering_single_elements" %}

{% include image.html id="another_element" %}

In this case the [`select`](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#css-selectors-through-the-css-property "select method in BeautifulSoup")
method in BeautifulSoup is [useful](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#searching-by-css-class "Searching by CSS class") to...
> [ ... ] match two or more CSS classes [ ... ]

```python
image_divs = browser.page.select('div.s-product-image-container.puis-image-overlay-grey')
```

Notice that in this case CSS classes are separated by a `.` character, not
a whitespace.
[The two things are not interchangable](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#css-selectors-through-the-css-property "CSS selectors through the .css property").

What's more: Using
[`find_all()`](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#find-all "find_all() - BeautifulSoup")
vs `select()` is a matter of preference and use case. `find_all()` is more
Pythonic than `select()` but requires more planning. To get the same result,
in-fact, you could have used this instead:

```python
image_divs = browser.page.find_all('div', class_=['s-product-image-container', 'puis-image-overlay-grey'])
```

*Notice the list vs the string in the previous one.*

### Getting the product URLs

{% include image.html id="getting_the_product_url" %}

Inside these `div`s we need to extract the URLs of the product pages: we can do
that by looping through them and getting the content of the
`<a href="...">` tags.

```python
import time
from urllib.parse import urljoin

for divs in image_divs:
    link = divs.find('a', class_='a-link-normal s-no-outline')
    if link:
        product_url: str = link.get('href')
        # Products don't start with https:// and /sspa/
        if not (product_url.startswith('https://') or product_url.startswith('/sspa/')):
            full_url: str = urljoin(url, product_url)
            all_products.append(get_product(full_url, browser_user_agent))
            time.sleep(5)
```

What's happening here?

After selecting the correct `a` elements, using the
two classes `a-link-normal` and `s-no-outline`, we get the content of the tag
and check if it's really a product. By looking at the HTML code, you can notice
that non-product links start with `https://` or `/sspa/`, so, apparently, there
are only three categories of links below the *image* `div`s.

{% include image.html id="invalid_link" %}

`get_product` is a function that calls the code in the
[previous post]({%- include post/post_url_iso639.liquid post_url="2024-01-11-scraping-essentials-beautifulsoup" -%}{{ post_url_iso639 }} "{{ post_title_iso639 }}").
Every scraped product is saved into an instance of the `Product` dataclass and
appended to a list.

To avoid hitting browsing limits set by Amazon I have used the `time.sleep`
function. This does not guarantee not to be blocked, but it is the first basic
mitigation strategy one can implement.

## Dataclass

If we need a list of products we can create a new dataclass like this
(we'll see the `DataClassJSONMixin` in the next post)

```python
from mashumaro.mixins.json import DataClassJSONMixin

@dataclass
class Products:
    products: list[Product]

@dataclass
class ProductSearch(DataClassJSONMixin):
    products: Products
    url: str

# <previous code here>

p = ProductSearch(products=Products(products=all_products), url=url)
pprint.pprint(p)
```

We get a long list of products. I'll truncate it to the last two:

```
# [ ... ]
   Product(url='https://www.amazon.it/devices-robust-responsiveness-exceptional-performance/dp/B09H1CKTFP/ref=sr_1_58?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3LBF29P0UKQSV&keywords=ssd&qid=1705077011&sprefix=ssd%2Caps%2C81&sr=8-58',
           asin_raw='B09H1CKTFP',
           asin='B09H1CKTFP',
           title_raw='WD Red SN700 1TB NVMe SSD for NAS '
                     'devices, with robust system '
                     'responsiveness and exceptional I/O '
                     'performance',
           title='WD Red SN700 1TB NVMe SSD for NAS devices, '
                 'with robust system responsiveness and '
                 'exceptional I/O performance',
           price_raw='99,49€',
           price=Decimal('99.49'),
           currency_symbol_raw='€',
           currency_symbol='€',
           availabile_raw='Nuovo & Usato (79) '
                          'da85,62€85,62€ \xa0Spedizione '
                          'GRATUITA.',
           availabile=True,
           rating_raw='4,7',
           rating=4.7,
           reviews_raw='338 voti',
           reviews=338),
   Product(url='https://www.amazon.it/Samsung-Interno-Solid-State-MZ-77E1T0/dp/B08T1SMTF9/ref=sr_1_59?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3LBF29P0UKQSV&keywords=ssd&qid=1705077011&sprefix=ssd%2Caps%2C81&sr=8-59',
           asin_raw='B08T1SMTF9',
           asin='B08T1SMTF9',
           title_raw='Samsung 870 EVO Unità SSD interna SATA '
                     'da 2,5" (MZ-77E1T0) 1 TB',
           title='Samsung 870 EVO Unità SSD interna SATA da '
                 '2,5" (MZ-77E1T0) 1 TB',
           price_raw='€70,00',
           price=Decimal('70.00'),
           currency_symbol_raw='€',
           currency_symbol='€',
           availabile_raw='',
           availabile=False,
           rating_raw='4,7',
           rating=4.7,
           reviews_raw='964 voti',
           reviews=964)])
```

## Considerations

### Sleeping?

Using the `time.sleep(5)` instruction is, of course, detrimental to the
overall speed. Given that there are a total of 48 products extracted for this
example, it takes at least `48 * 5 = 240` seconds to perform the script,
excluding the overheads of opening and processing the web pages themselves.

### User agent rotation

Another solution must be found. For example one could put rotating user agents
in this call.

```python
import random

BROWSER_USER_AGENTS = ['one', 'two', ..., 'n']

browser_user_agent = random.choice(BROWSER_USER_AGENTS)
all_products.append(get_product(full_url, browser_user_agent))
```

### IP rotation

About the IPs, a simple and free solution is to use TOR:

```
torsocks --isolate python -m amzn_scrape
```

There are probably better suited services and APIs to do this.

### All data is in the search page

All data we've seen as part of the `Product` dataclass is already present is
the search result page. If we used the search page only we could save 240
seconds and 48 queries!!! In-fact for each product you have the title, rating,
price, reviews, ASIN (through the URL).

I started with a bottom up approach, but it's not always the best answer.

Using this different method, however, requires a full redesign of the previous
algorithms and you won't be able to get extra details from the single product
pages.

You could combine both methods to get information depending on the fields you
need. If you need basic fields, don't scrape the products, otherwise if you
need the text of users' reviews, for example, you must scrape those pages as
well.

## Conclusion

In this post we saw how to scrape and group information about multiple products
in a single data structure. In the next post we'll see how to perform a query
using the search bar (which is a form) and how to export all data to JSON.

🚀
