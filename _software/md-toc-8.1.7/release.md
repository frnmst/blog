---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.1.7
software_version_raw: 000008.000001.000007
release_timestamp: 2022-12-29 16:57:02
is_on_pypi: true
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---

### Added

- Add comparison table for the various tools similar to md-toc. This closes
  [issue #36](https://github.com/frnmst/md-toc/issues/36) although there is
  still room for improvement.

### Fixed

- Use of list comprehensions instead of for loops to possibly improve speed.
- Pinned YAPF and flake8 versions through the pre-commit update command
  in the Makefile.
- Improved benchmark script
  - Improved random string generation speed using `ctypes`.
  - Bug fixes.
- Continuing to fix [issue #25](https://github.com/frnmst/md-toc/issues/25).
- Fixed pre-commit hooks.

### Changed

- Implement use of `setup.cfg` and `pyproject.toml`.
  `setup.py` is now a dummy file.
- Moved some pre-commit options to `setup.cfg`.
- Replaced Pipenv environment with `venv` and `pip` commands.
- Replaced distribution commands (`setup.py sdist`, `setup.py bdist_wheel`)
  with the [`build`](https://pypa-build.readthedocs.io/en/stable/) module.
- Pinned [fpyutils](https://blog.franco.net.eu.org/software/#fpyutils)
  dependency at least to version 3.0.0.

Note: GitHub Flavored Markdown is still at version 0.29. md-toc does its best
to support both GFM 0.29 and cmark 0.30.
