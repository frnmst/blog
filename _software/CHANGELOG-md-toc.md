---
layout: changelog
enable_markdown: true
software_name: md-toc
title: md-toc changelog
excerpt: none
lang: 'en'
---

## Plan for 10.0.0

### Added

- [#24: *Feature Request: Automatically skip lines to first or (N) TOC*](https://github.com/frnmst/md-toc/issues/24)
- [#37: *Is it possible to ignore certain headers?*](https://github.com/frnmst/md-toc/issues/37)

## Plan for 9.0.1

### Fixed

- [__✓__ 5c7ddab](https://software.franco.net.eu.org/frnmst/md-toc/commit/5c7ddab)

  Add missing binary file detections in `md_toc.api.build_toc`
- [__✓__ 89e2d60](https://software.franco.net.eu.org/frnmst/md-toc/commit/89e2d60)

  Fix references to `md_toc.api.build_toc` from `md_toc.build_toc`
- [__✓__ 5c7ddab](https://software.franco.net.eu.org/frnmst/md-toc/commit/5c7ddab)

  Improve test coverage
- [__✓__ 5c7ddab](https://software.franco.net.eu.org/frnmst/md-toc/commit/5c7ddab)

  Reduce some code for function input validation
- [Issue #25: *Links not working when some styling is used in the header on GitHub*](https://github.com/frnmst/md-toc/issues/25):
  must be completed before this version.
- Update existing cmark code from version 0.30.0 to version
  [0.31.2](https://spec.commonmark.org/0.31.2/changes.html).
- An `md_toc.exceptions.TocDoesNotRenderAsCoherentList` exception is raised for
  big inputs (>= 50M characters) when using the benchmark script.
  There is a jump in the detected header level, 1 then 3, not 2. This always
  happens with the last line of the dummy input file.
- Update existing redcarpet code from version v3.5.0 to version v3.6.0.
  See [issue #28: *Redcarpet v3.5.1 is out*](https://github.com/frnmst/md-toc/issues/28).

### Added

- [__✓__ 276acd6](https://software.franco.net.eu.org/frnmst/md-toc/commit/276acd6)

  Add GitHub unit test action
- Add code fence detection before writing TOC in place.
  Until version 9.0.0, given for example a file like this

  ````markdown
  ```

  <!--TOC_MARKER-->

  ```

  # ONE

  ## TWO

  # ONE

  ## TWO

  ### THREE
  ````

  running

  ```shell
  md_toc --toc-marker '<!--TOC_MARKER-->' -p github file.md
  ```

  results in

  ````markdown
  ```

  <!--TOC_MARKER-->

  - [ONE](#one)
    - [TWO](#two)
  - [ONE](#one-1)
    - [TWO](#two-1)
      - [THREE](#three)

  <!--TOC_MARKER-->

  ```

  # ONE

  ## TWO

  # ONE

  ## TWO

  ### THREE
  ````

  instead of

  ````markdown
  ```

  <!--TOC_MARKER-->

  ```

  # ONE

  ## TWO

  # ONE

  ## TWO

  ### THREE
  ````

  This means that if a TOC marker is detected within fenced code block it must
  be ignored. A similar problem happens if there are two TOC markers, one of
  them being inside a fenced code block:

  ````markdown
  ```

  <!--TOC_MARKER-->

  ```

  <!--TOC_MARKER-->

  # ONE

  ## TWO

  # ONE

  ## TWO

  ### THREE
  ````

  using the previous command results in

  ````markdown
  ```

  <!--TOC_MARKER-->

  - [ONE](#one)
    - [TWO](#two)
  - [ONE](#one-1)
    - [TWO](#two-1)
      - [THREE](#three)

  <!--TOC_MARKER-->
  <!--TOC_MARKER-->

  ```

  <!--TOC_MARKER-->

  # ONE

  ## TWO

  # ONE

  ## TWO

  ### THREE
  ````
