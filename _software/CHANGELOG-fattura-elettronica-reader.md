---
layout: changelog
enable_markdown: true
software_name: fattura-elettronica-reader
title: fattura-elettronica-reader changelog
excerpt: none
lang: 'en'
---

## Plan for 4.0.1

### Fixed

- Use of list comprehensions instead of for loops to possibly improve speed.
- Use Mashumaro to R/W YAML.

## Plan for 4.0.0

### Fixed

- [__✓__ 8b06430](https://software.franco.net.eu.org/frnmst/fattura-elettronica-reader/commit/8b06430)

  Import Makefile from another project
- [__✓__ 8b06430](https://software.franco.net.eu.org/frnmst/fattura-elettronica-reader/commit/8b06430)

  Update pre-commit hooks.
- [__✓__ 24051a0](https://software.franco.net.eu.org/frnmst/fattura-elettronica-reader/commit/24051a0)

  Fix constants file to have a cleaner dictionary structure.
- [__✓__ 8b06430](https://software.franco.net.eu.org/frnmst/fattura-elettronica-reader/commit/8b06430)

  Update configuration for the documentation.
- [__✓__ 8b06430](https://software.franco.net.eu.org/frnmst/fattura-elettronica-reader/commit/8b06430)

  Update Python dependencies.

### Changed

- [__✓__ 8b06430](https://software.franco.net.eu.org/frnmst/fattura-elettronica-reader/commit/8b06430)

  Rename functions:
  - `fattura_elettronica_reader.api.create_appdirs` -> `fattura_elettronica_reader.api.create_platformdirs`
  - `fattura_elettronica_reader.api.define_appdirs_user_config_dir_file_path` -> `fattura_elettronica_reader.api.define_platformdirs_user_config_dir_file_path`
  - `fattura_elettronica_reader.api.define_appdirs_user_data_dir_file_path` -> `fattura_elettronica_reader.api.define_platformdirs_user_data_dir_file_path`

- [__✓__ 64822b9](https://software.franco.net.eu.org/frnmst/fattura-elettronica-reader/commit/64822b9)

  Remove `fattura_elettronica_reader.api.get_invoice_filename` function.

- [__✓__ 8b06430](https://software.franco.net.eu.org/frnmst/fattura-elettronica-reader/commit/8b06430)

  Use [platformdirs](https://github.com/platformdirs/platformdirs)
  instead of [appdirs](https://github.com/ActiveState/appdirs). The latter
  library seems unmaintained.
