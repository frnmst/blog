---
layout: software_release
enable_markdown: true
title: release
excerpt: none
tags: [fpyutils, utilities, python]
software_name: fpyutils
software_name_python_module: fpyutils
software_version: 4.0.1
software_version_raw: 000004.000000.000001
release_timestamp: 2023-11-08 22:01:16
is_on_pypi: true
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---

### Fixed

- Support older Python versions >= 3.7, using `__future__` imports. This fixes
  [issue #6](https://github.com/frnmst/fpyutils/issues/6). See also
  [PR #7](https://github.com/frnmst/fpyutils/pull/7)
- Fix regression. See
  [md-toc changelog](https://blog.franco.net.eu.org/software/CHANGELOG-md-toc.html#plan-for-822)

### Changed

- Add unit test support for multiple Python versions using
  [tox](https://tox.wiki) and [asdf](https://asdf-vm.com/)
