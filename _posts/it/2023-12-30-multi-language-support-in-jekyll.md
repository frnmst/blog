---
title: ! 'Supporto multi linguaggio per Jekyll'
tags: [i18n, traduzione, jekyll, html, tutorial]
updated: 2023-12-30 17:25:00
description: ! "Creiamo un sistema per gestire i post del blog e le pagine scritte in diversi linguaggi su Jekyll"
lang: 'it'
permalink: '/it/post/multi-language-support-in-jekyll.html'
images:
  - id: 'liquid_nvim'
    filename: 'liquid_syntax_highlighting_nvim.png'
    alt: 'Evidenziazione della sitassi liquid con Neovim'
    caption: ! 'Evidenziazione (libera e gratuita) della sitassi liquid con Neovim'
    width: '300px'
---

{% include image.html id="liquid_nvim" %}

<!--excerpt_start-->
Di recente ho aggiunto il supporto multi linguaggio su questo blog. Ci sono
diversi plugin per arrivare a questo<!--excerpt_end-->, ma ho preferito fare
tutto manualmente perché ci sono già troppi elementi personalizzati. Sono
richiesti diversi passaggi. Diamo un'occhiata a quelli per gestire i post.

Prima di tutto bisogna decidere un linguaggio predefinito: si può usare
l'inglese (`en`), per esempio.

Si inizia organizzando quindi i post per linguaggio.

1. il linguaggio predefinito o quello esistente devono coincidere. I post in
   questo linguaggio  devono mantenere i permalink esistenti per motivi di SEO.
   Se usate lo schema `/post/${post_name}` come me, lasciatelo così com'è
2. i nuovi linguaggi possono seguire lo schema `/${lang}/post/${post_name}`.
   Questo viene ottenuto aggiungendo la variabile `permalink` al front matter
   dello YAML
3. bisogna spostare tutti i post esistenti in una sub-directory. Se il
   linguaggio predefinito è `en` bisogna spostare `_posts/*.md` in
   `_posts/en/*.md`
4. aggiungete la variabile `lang: 'en'` al front matter dello YAML per ogni
   post in inglese. I post in altre lingue devono seguire il
   [proprio codice](https://en.wikipedia.org/wiki/IETF_language_tag#List_of_common_primary_language_subtags).
   Questo semplificherà gli algoritmi per filtrare i post per linguaggio

Riguardo il punto 2, quello che consiglio di fare è di copiare il post
della lingua predefinita **dopo** che questo viene completato. Purtroppo non
ci sono sitemi come [gettext](https://en.wikipedia.org/wiki/Gettext) o simili
quindi dovrete tradurre tutto il codice markdown e non solo le stringhe di
testo. Una vola che avete sia la versione nella lingua predefinita, sia la
traduzione, bisognerà tenerle aggiornate entrambi.

Seguono un paio di esempi per i punti 2 e 4

```yaml
---
title: ! 'JSON-LD in Jekyll'
tags: [json, json-ld, jekyll, tutorial]
updated: 2023-12-16 18:00:00
description: ! "Generate JSON-LD for Jekyll blog posts to improve SEO"
lang: 'en'
---
```

Da notare il permalink qui:

```yaml
---
title: ! 'JSON-LD con Jekyll'
tags: [json, json-ld, jekyll, tutorial]
updated: 2023-12-27 16:53:00
description: ! "Generate JSON-LD for Jekyll blog posts to improve SEO"
lang: 'it'
permalink: '/it/post/json-ld-in-jekyll.html'
---
```

La prossima cosa da fare è mettere in ordine la pagina home. Nel mio caso
contiente la lista di tutti i post. Quello che voglio fare è di mettere in
lista tutti i post nel linguaggio specifico se esistono, altrimenti usare
quelli nel linguaggio predefinito. Questo si può ottenere attraverso una serie
di filtri in linguaggio liquid. Per esempio, per ottenere tutti i post nel
linguaggio corrente si può usare questa instruzione:

```liquid
{% raw %}{%- assign site_posts_iso639 = site.posts | where: 'lang', page.lang -%}{% endraw %}
```

Per avere i post nel linguaggio predefinito è sufficiente cambiare la variabile
di filtro:

```liquid
{% raw %}{%- assign site_posts_default_iso639 = site.posts | where: 'lang', site.languages.default -%}{% endraw %}
```

È qui che la variabile `lang` diventa utile.

Incominciando con questi due array potete poi usare dei cicli for e delle
condizioni per fare quello spiegato sopra poiché non ci sono operazioni su
insiemi nel liguaggio Jekyll liquid.

Per generare la lista di post dovete passare le due variabili in questo modo:

```liquid
{% raw %}{%- assign site_posts_iso639 = site.posts | where: 'lang', page.lang -%}
{%- assign site_posts_default_iso639 = site.posts | where: 'lang', site.languages.default -%}
{%- include core/get_elements_by_iso639.liquid elements_iso639=site_posts_iso639 elements_default_iso639=site_posts_default_iso639 -%}
{%- assign available_posts = available_elements -%}{% endraw %}
```

Per comodità ho rinominato la variabile alla fine del codice.

Il risultato è questo file chiamato
`_includes/core/get_elements_by_iso639.liquid`.

```liquid
{% raw %}{%- comment -%}
Generate a list of elements giving priority to the localized versions.

If a transalted elements exists use that one, otherwise use the one in the default
language which must always exist.

input:
    elements_default_iso639: array
        A list of files in the default language
    site_element_iso639: array
        A list of files in a non-default language

return:
    available_elements: array
{%- endcomment -%}
{%- assign available_elements = '' | split: ',' -%}
{%- for i in include.elements_default_iso639 -%}
    {%- assign translated_element_exists = false -%}
    {%- for j in include.elements_iso639 -%}
        {%- comment -%}Get the last path component (stem) of the two files in exam.{%- endcomment -%}
        {%- assign j_stem = j.path | split: '/' | last -%}
        {%- assign i_stem = i.path | split: '/' | last -%}
        {%- if j_stem == i_stem -%}
        {%- comment -%}Translated file found, add it to the final list.{%- endcomment -%}
            {%- assign translated_element_exists = true -%}
            {%- assign available_elements = available_elements | push: j -%}
            {%- break -%}
        {%- endif -%}
    {%- endfor -%}
    {%- if translated_element_exists == false -%}
        {%- comment -%}Translated file not found, add the default file to the final list.{%- endcomment -%}
        {%- assign available_elements = available_elements | push: i -%}
    {%- endif -%}
{%- endfor -%}{% endraw %}
```

Utilizzo questo *file-funzione* (in basso è spiegata la definizione) anche per
generare la lista di pagine. Le pagione come riguardo a, mappa del sito,
privacy policy, ecc... funzionano allo stesso modo dei post.

Purtroppo, come molte altre operazioni fatte in questo modo, la complessità
è di O(n<sup>2</sup>). Questo si riflette sul tempo di build del sito che è
cresciuto molto dopo tutti questi cambiamenti. 💥

Comunque, ora dobbiamo aggiungere alcune variabili nel file `_config.yml`

1. informazioni del sito come il titolo, descrizione, ecc... Per esempio,
   per quanto riguarda il titolo:

   ```yaml
   title:
      lang:
        en:                 "Franco Masotti software developer blog"
        it:                 "Franco Masotti sviluppatore software"
   ```

2. per i linguaggi supportati:

   ```yaml
   # Locales.
   languages:
     default: 'en'
     supported:
       - 'en'
       - 'it'
   ```

Per migliorare la struttura del codice potete trattare la vostra directory
`_includes` come una sorgente di *file-funzione*. Ogni file può avere uno
specifico input e output. Questo file chiamato `_includes/head/hreflang.liquid`
, per esempio, genera i link che devono essere usati dai motori di ricerca
per
[trovare le stesse pagine in altre lingue](https://developers.google.com/search/docs/specialty/international/localized-versions?hl=it#html)

```liquid
{% raw %}{%- include seo/iso639_alternate.liquid -%}
{%- assign iso639_alt = iso639_alt | split: ',' -%}
{%- for lang in iso639_alt -%}
    {%- assign hreflang = lang | split: '|' -%}
<link rel="alternate" hreflang="{{ hreflang[0] }}" href="{{ site.rooturl }}{{ hreflang[1] }}" />
{%- endfor -%}{% endraw %}
```

Una chiamata di include al file `seo/iso639_alternate.liquid` ritorna un array
con i link alla pagina corrente in diverse lingue ed i suoi codici lingua.

Ci sono molti altri di questi file ed esempi come i selettori del linguaggio,
i tag, ecc... ma ho spiegato i punti più importanti. Il resto si trova nel
[codice sorgente](https://software.franco.net.eu.org/frnmst/blog) 🚀
