---
title: ! 'Che cosa è successo al CSS?'
tags: [css, sito-web, blog]
updated: 2023-12-30 22:04:00
description: ! 'Motivi per la rimozione del CSS da questo sito web'
permalink: '/it/post/what-happened-to-the-css.html'
lang: 'it'
---

<!--excerpt_start-->
Si, praticamente ho rimosso tutto il file CSS da<!--excerpt_start--> questo
sito!

Questo mi da alcuni vantaggi:

- posso concentrarmi di più sulla struttura e sul contenuto
- non uso il tempo per cercare soluzioni per fare x, y o z per il CSS
- il sito si carica più velocemente

~~Se avete problemi per l'assenza di un tema scuro, non lamentatevi: installate [Redshift](http://jonls.dk/redshift/).~~

Aggiornamento 2022-07-21: ora uso i CSS del [tema Jekyll primer di GitHub](https://github.com/pages-themes/primer/)
perché sono più responsive ed il risultato è più elegante.

Aggiornamento 2022-10-08: ho implementato un tema scuro con < 10 righe CSS.
Dovete impostare il vostro dispositivo in *modalità scura*, oppure usare
l'interruttore nella console di sviluppo del browser.

```css
/* set the background color to black when client requests dark mode and invert the colors
 * from post by user lewisl9029: https://news.ycombinator.com/item?id=26472246
 * See also https://github.com/pages-themes/primer/issues/64
 */
@media (prefers-color-scheme: dark) {
    body {
        background-color: black;
        filter:  brightness(.9) contrast(1.2) hue-rotate(180deg) invert(80%);
    }
  img {
    filter: brightness(.6) contrast(1.2) invert(-80%) grayscale(50%);
  }
}
```

Aggiornamento 2023-12-30: è da un po' che ho rimosso il trucco del tema scuro
perché causava problemi di prestazione e di visualizzazione. Se lo si usa
si vede che il browser lagga, specialmente su dispositivi meno potenti.

~

Saluti!
