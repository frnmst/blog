import json
import os
import fcntl
from wsgiref.simple_server import make_server

# This path must be readable and writable by your web server.
DATA_FILE: str = '/var/www/iot.my.domain/sensors/data.json'

# Development port. Not used in production.
PORT: int = 3050

# python3 -c 'import uuid; print(str(uuid.uuid4()))'
TOKENS: list[str] = [
    'e9b1b5d9-b7a0-409a-b21c-b84eb6f5a3f0'
]


class Data:
    def __init__(self, data_file: str):
        self.data_file = data_file
        self.data: list = []

    def append_json(self, data: dict):
        with open(self.data_file, 'a') as f:
            # Create a waiting lock on the data file.
            fcntl.flock(f, fcntl.LOCK_EX)
            try:
                json.dump(data, f)
                f.write(os.linesep)
            finally:
                # Always release a lock whatever happens.
                fcntl.flock(f, fcntl.LOCK_UN)

    def read_json(self):
        # Return the data.
        with open(self.data_file) as f:
            self.data = [json.loads(line) for line in f if line.strip()]

    def get_data(self, latest: bool=True) -> list[dict]:
        r"""Get the existing data.

        :param: latest: If ``True``, the default, return the last reading for
            each board.
        """
        self.read_json()

        filtered_result: list[dict] = self.data
        if latest:
            rs = []
            filtered_result = []
            for r in self.data[::-1]:
                key = r['board']['id']
                if key not in rs:
                    rs.append(key)
                    filtered_result.append(r)

        return filtered_result

    def get_gas_events(self, latest: bool=True) -> list[dict]:
        r"""Get the existing data.

        :param: latest: If ``True``, the default, return the last reading for
            each board containing gas events.
        """

        result: list[dict] = []
        for d in self.data:
            if d['sensors']['gas_state_bool']:
                result.append({d['board']['id']: d})

        filtered_result: list[dict] = result
        if latest:
            rs = []
            filtered_result = []
            for r in result[::-1]:
                key = list(r.keys())[0]
                if key not in rs:
                    rs.append(key)
                    filtered_result.append(r[key])

        return filtered_result


class Request:
    def __init__(self, data: dict = {}):
        self.data = data
        self.valid = self.is_valid_data()

    def is_valid_data(self) -> bool:
        if ('board' in self.data
           and isinstance(self.data['board'], dict)
           and 'sensors' in self.data
           and isinstance(self.data['sensors'], dict)
           and 'computations' in self.data
           and isinstance(self.data['computations'], dict)
           and 'id' in self.data['board']
           and isinstance(self.data['board']['id'], str)
           and 'gas_concentration_volts' in self.data['sensors']
           and isinstance(self.data['sensors']['gas_concentration_volts'], float | int)
           and 'temperature_celsius' in self.data['sensors']
           and isinstance(self.data['sensors']['temperature_celsius'], float | int)
           and 'humidity_percent' in self.data['sensors']
           and isinstance(self.data['sensors']['humidity_percent'], float | int)
           and 'gas_state_bool' in self.data['sensors']
           and isinstance(self.data['sensors']['gas_state_bool'], bool)
           and 'gas_tendency' in self.data['computations']
           and isinstance(self.data['computations']['gas_tendency'], int)
           and 'timestamp_utc' in self.data['computations']
           and isinstance(self.data['computations']['timestamp_utc'], str)):
            return True
        else:
            return False


class Response:
    def __init__(self, start_response):
        self.start_response = start_response

    def send_http(self, r_type: str, payload: str | dict, status_string: str):
        if r_type == 'json':
            response_body = json.dumps(payload).encode('utf-8')
            headers = [('Content-Type', 'application/json')]
        elif r_type == 'html':
            response_body = payload.encode('utf-8')
            headers = [('Content-Type', 'text/html')]
        self.start_response(status_string, headers)
        return [response_body]


class HTMLResponse(Response):
    def send_http(self, payload: str, status_string: str):
        return super().send_http('html', payload,  status_string)

    def send_http_200(self, text: str = 'OK'):
        return self.send_http(text, '200 OK')

    def generate_payload(self, data: list[dict], latest_gas_events: list[dict]) -> str:
        html: str = '<!DOCTYPE html><html lang="en"><head><meta http-equiv="refresh" content="5"><title>ESP8266 gas detector</title></head>'
        html += '<body><h1>Sensors data</h1><hr>'
        html += '<style>'
        html += 'body { font-family: Arial, sans-serif; background-color: #f4f4f4; margin: 0; padding: 20px; }'
        html += 'h1 { color: #333; }'
        html += 'p { font-size: 18px; color: #555; }'
        html += '.sensor-data { justify-content: space-between; display: flex; background: #fff; border-radius: 5px; padding: 15px; box-shadow: 0 2px 5px rgba(0,0,0,0.1); margin-bottom: 10px; }'
        html += '.sensor-value { display: flex; flex-direction: column; align-items: flex-end; }'
        html += '@keyframes blink { 0% { opacity: 1; } 50% { opacity: 0; } 100% { opacity: 1; } }'
        html += '.blink { animation: blink 1s infinite; }'
        html += '</style>'

        for d in data:
            html += f'<p><b>{d["board"]["id"]} on {d["computations"]["timestamp_utc"]}</b></p>'
            html += '<div class="sensor-data">'
            html += f'<p><span><b>Gas (V)</b></span><span class="sensor-value">{d["sensors"]["gas_concentration_volts"]:.2f}</span></p>'
            html += f'<p><span><b>Gas trend</b></span><span class="sensor-value">{d["computations"]["gas_tendency"]}</span></p>'
            html += "<p><span><b>Gas detect</b></span><span class='sensor-value'>"
            if d['sensors']['gas_state_bool']:
                html += '<b class="blink">True</b>'
            else:
                html += 'False'
            html += '</span></p>'
            html += f'<p><span><b>Temp (oC)</b></span><span class="sensor-value">{d["sensors"]["temperature_celsius"]:.2f}</span></p>'
            html += f'<p><span><b>Hum (%)</b></span><span class="sensor-value">{d["sensors"]["humidity_percent"]:.2f}</span></p>'
            html += '</div><hr>'

        if latest_gas_events:
            html += '<h2>Latest gas events</h2>'
        for e in latest_gas_events:
            html += f'<p>{e["board"]["id"]}: {e["computations"]["timestamp_utc"]}</p>'

        html += '</body></html>'

        return html


class JSONResponse(Response):
    def send_http(self, payload: dict, status_string: str):
        return super().send_http('json', payload,  status_string)

    def send_http_400(self):
        return self.send_http({'error': 'Invalid JSON'}, '400 Bad Request')

    def send_http_403(self):
        return self.send_http({'error': 'Forbidden: invalid token'}, '403 Forbidden')

    def send_http_404(self):
        return self.send_http({'error': 'Not found'}, '404 Not Found')

    def send_http_200(self):
        return self.send_http({'message': 'OK'}, '200 OK')

    def send_http_415(self):
        return self.send_http({'error': 'Content-Type must be application/json'}, '415 Unsupported Media Type')


class JSONRequest(Request):
    pass


def app(environ, start_response):
    path = environ['PATH_INFO']
    method = environ['REQUEST_METHOD']
    auth_token = environ.get('HTTP_AUTHORIZATION', '').replace('Bearer ', '')

    json_response: JSONResponse = JSONResponse(start_response)
    html_response: HTMLResponse = HTMLResponse(start_response)
    data: Data = Data(DATA_FILE)

    if path == '/data' and method == 'PUT':
        # Create a new data entry.

        # Check for a valid token.
        if auth_token not in TOKENS:
            return json_response.send_http_403()

        # Handle JSON input.
        content_type = environ.get('CONTENT_TYPE', '')
        if content_type == 'application/json':
            try:
                request_body = environ['wsgi.input'].read(int(environ.get('CONTENT_LENGTH')))
                data_payload = json.loads(request_body)

                json_request: JSONRequest = JSONRequest(data_payload)

                if not json_request.valid:
                    return json_response.send_http_400()

                data.append_json(data_payload)

                return json_response.send_http_200()
            except (ValueError, KeyError, json.JSONDecodeError):
                return json_response.send_http_400()
        else:
            return json_response.send_http_415()

    elif path == '/data' and method == 'GET':
        # Get all historic data.
        return html_response.send_http_200('not implemented')

    elif path == '/data/latest' and method == 'GET':
        # Serve the dashboard.
        html_content: str

        try:
            basic_data: list[dict] = data.get_data()
            latest_gas_events: list[dict] = data.get_gas_events()
            html_content = html_response.generate_payload(basic_data, latest_gas_events)
        except FileNotFoundError:
            # Just print 'no data' and refresh the page as usual.
            html_content = '<!DOCTYPE html><html lang="en"><head><meta http-equiv="refresh" content="5"><title>ESP8266 gas detector</title></head><body><p>no data, will refresh every 5 seconds</p></body></html>'

        return html_response.send_http_200(html_content)

    else:
        return json_response.send_http_404()


# Production.
application = app

if __name__ == '__main__':
    # Development.
    # Create a WSGI server.
    httpd = make_server('', PORT, app)
    print('Serving on port ' + str(PORT) + '...')

    # Serve until process is killed-
    httpd.serve_forever()
