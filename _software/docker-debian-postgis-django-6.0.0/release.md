---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: docker-debian-postgis-django
software_name_python_module: docker_debian_postgis_django
software_version: 6.0.0
software_version_raw: 000006.000000.000000
release_timestamp: 2021-08-30 20:35:37
is_on_pypi: false
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---

### Fixed

- Most commands in the Dockerfile have been composed in a single
  shell command.
- Fixed some `Makefile.dist` targets.
- Updated email.

### Changed

- Updated Dockerfile image and pinned packages from Debian 10 to 11.

### Added

- New git hooks have been added in the pre-commit file.

### Removed

- The `ci.sh` has been replaced by Jenkins files on repositories that use this
  project, such as [django-futils](https://software.franco.net.eu.org/frnmst/django-futils).
- Checksums and examples in the readme file are no longer needed.
