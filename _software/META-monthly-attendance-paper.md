---
meta: true
software_name: monthly-attendance-paper
archived: true
repositories:
  - label: 'canonical'
    url: 'https://software.franco.net.eu.org/frnmst-archives/monthly-attendance-paper'
  - label: 'codeberg'
    url: 'https://codeberg.org/frnmst-archives/monthly-attendance-paper'
  - label: 'framagit'
    url: 'https://framagit.org/frnmst-archives/monthly-attendance-paper'
documentation:
  - label: 'software.franco.net.eu.org/frnmst-archives/monthly-attendance-paper#monthly-attendance-paper'
    url: 'https://software.franco.net.eu.org/frnmst-archives/monthly-attendance-paper#monthly-attendance-paper'
---
