---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: docker-debian-postgis-django
software_name_python_module: docker_debian_postgis_django
software_version: 0.0.1
software_version_raw: 000000.000000.000001
release_timestamp: 2020-11-09 22:29:01
is_on_pypi: false
has_changelog: false
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---
