---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: automated-tasks
software_name_python_module: automated_tasks
software_version: 12.0.0
software_version_raw: 000012.000000.000000
release_timestamp: 2021-11-17 22:57:46
is_on_pypi: false
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Simplified configuration for `build_python_packages.py`. Breaking change.

### Fixed

- Improved documentation for the `qvm.py` script.

### Added

- Added network device option for the `qvm.py` script. Breaking change.
- Added pre and post build commands for `build_python_packages.py`. Breaking change.
- Use of arbitrary block lists for `hblock_unbound.py`. Breaking change.
