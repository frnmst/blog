---
title: ! 'Computer recreations: random numbers and properties'
tags: [computer-recreations, pi, monte-carlo-method, galton-board, tutorial, video, youtube]
updated: 2024-01-22 22:58:00
description: ! 'A new video for the "Computer recreations" playlist concentrating on practical examples of random numbers'
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/DgHp3FRw0Qk?si=_0PmxSDxaJoor5Lg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Table of contents

<!--TOC-->

- [Table of contents](#table-of-contents)
- [Intro](#intro)
- [Files](#files)

<!--TOC-->

## Intro

This new video of the "Computer recreations" playlist talks about random numbers and their properties. We'll see 3 of 5 examples explained in the article. All source code is written in Python.

The article is called "Five easy pieces for a do loop and random-number generator" by A. K. Dewdney published on the "Scientific American" magazine from April 1985.

## Files

{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/solve-computer-science/computer-recreations/computer-recreations-random-numbers-and-properties/galton.py" text="galton.py" name="galton.py" is_text=true code_lang="python" %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/solve-computer-science/computer-recreations/computer-recreations-random-numbers-and-properties/gen_random.py" text="gen_random.py" name="galton.py" is_text=true code_lang="python" %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/solve-computer-science/computer-recreations/computer-recreations-random-numbers-and-properties/pint.py" text="pint.py" name="pint.py" is_text=true code_lang="python" %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/solve-computer-science/computer-recreations/computer-recreations-random-numbers-and-properties/qwing.py" text="qwing.py" name="qwing.py" is_text=true code_lang="python" %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/solve-computer-science/computer-recreations/computer-recreations-random-numbers-and-properties/voters.py" text="voters.py" name="voters.py" is_text=true code_lang="python" %}
{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/solve-computer-science/computer-recreations/computer-recreations-random-numbers-and-properties/zombie.py" text="zombie.py" name="zombie.py" is_text=true code_lang="python" %}
