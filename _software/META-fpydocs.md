---
meta: true
software_name: fpydocs
archived: true
repositories:
  - label: 'canonical'
    url: 'https://software.franco.net.eu.org/frnmst-archives/fpydocs'
  - label: 'codeberg'
    url: 'https://codeberg.org/frnmst-archives/fpydocs'
  - label: 'framagit'
    url: 'https://framagit.org/frnmst-archives/fpydocs'
documentation:
  - label: 'docs.franco.net.eu.org/ftutorials'
    url: 'https://docs.franco.net.eu.org/ftutorials'
  - label: 'changelog'
    url:
---
