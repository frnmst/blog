// SPDX-FileCopyrightText: 2023 Franco Masotti
//
// SPDX-License-Identifier: MIT
#include <stdio.h>
#include <string.h>
#include <stdint.h>

int main(void)
{
    char array[100] = "ab c def g hi";
    char reversed[100];

    // Transform all indices to size_t.
    size_t i = 0, j = 0;
    size_t length = 0;

    // Does not include the '\0' character.
    length = strlen(array);

    for (i = length - 1; i != SIZE_MAX; i--, j++)
        reversed[j] = array[i];

    reversed[length] = '\0';

    // Underflow check.
    i = 0;
    printf("virtual 0 = %zu\n", i - 1);
    printf("SIZE_MAX = %zu\n", SIZE_MAX);

    printf("reversed = %s\n", reversed);

    return 0;
}
