---
title: ! 'ESP8266 flammable gas alarm - part 3'
tags: [circuit, esp8266, mq-2, gas detector, gas alarm, tutorial, youtube, video]
related_links: ['https://www.youtube.com/watch?v=Datphk70QXk']
updated: 2024-09-23 19:11:00
description: ! "Third part of the gas detector-alarm ESP8266 video"
lang: 'en'
---
<iframe width="280px" height="157px" src="https://www.youtube.com/embed/Datphk70QXk?si=dI-gEkRPy-BvqLJO" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<!--TOC-->

- [Summary](#summary)
- [Arduino sketch](#arduino-sketch)
- [WSGI server](#wsgi-server)
- [Apache configuration](#apache-configuration)

<!--TOC-->

## Summary

<!--excerpt_start-->This video continues our ESP8266 project, focusing on improving the dashboard's functionality<!--excerpt_end-->. I highlight the security issues with the current setup, which operates on plain HTTP, and the limitations of the board in handling multiple clients simultaneously. To address these problems, I propose a straightforward solution: transforming the board into an HTTPS client that sends sensor data to a more capable server.

I explain the changes made to the board's code, including the removal of the HTTP server components and the introduction of new constants for configuration. I emphasize the importance of assigning a unique name to the board and defining the server's host, port, and endpoint. Additionally, I discuss the necessity of an authorization token for accessing the server's data endpoint, ensuring that only valid requests could add data to the dashboard.

As I delve deeper into the code, I outline how the board would now send data at defined intervals, utilizing a full date-time format close to ISO 8601. I also describe the manual construction of HTTP requests, opting for a `PUT` verb since we were appending data to a JSON file rather than creating a new resource. I highlight the importance of debugging and the option to disable certificate validation for easier and local setups, while also recommending proper validation when sending data via Internet.

Finally, I introduce the backend server, built using a simple WSGI application in Python. I detailed the endpoints created for data submission and retrieval, along with the setup process for Apache to run the WSGI application. I demonstrated how to manually make requests using curl and showed the real-time updates of the JSON data file. The script does not manage the size if the data file so it has the potential of growing over time.

## Arduino sketch

{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/solve-computer-science/arduino/esp8266-flammable-gas-alarm-part-3/gas_sensor_dashboard.ino" text="schetch.ino" name="schetch.ino" is_text=true code_lang="cpp" %}

## WSGI server

{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/solve-computer-science/arduino/esp8266-flammable-gas-alarm-part-3/wsgi.py" text="wsgi.py" name="schetch.ino" is_text=true code_lang="python" %}

## Apache configuration

{% include show_code/show_code.liquid url="https://software.franco.net.eu.org/frnmst/blog/raw/branch/master/_extras/solve-computer-science/arduino/esp8266-flammable-gas-alarm-part-3/virtualhost_apache.conf" text="virtualhost_apache.conf" name="schetch.ino" is_text=true code_lang="apache" %}
