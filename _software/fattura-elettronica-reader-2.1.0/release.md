---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fattura-elettronica-reader
software_name_python_module: fattura_elettronica_reader
software_version: 2.1.0
software_version_raw: 000002.000001.000000
release_timestamp: 2021-12-21 21:36:20
is_on_pypi: true
has_changelog: true
lang: 'en'
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Updated file checksum variables.
- Updated Python dependencies.
- Updated copyright headers.

### Added

- Added a new option called `destination directory` where all the extracted
  file and attachments will be placed in.

### Removed

- Removed obsolete tables in documentation.
