---
title: ! "Recuperare un Patriot Burst con l'errore SATAFIRM S11"
tags: [satafirm-s11, ssd, ssd-controller, errore-firmware, firmware-danneggiato]
updated: 2024-01-05 23:00:00
description: ! "Sono riuscito a recuperare un SSD Patriot Burst con l'errore SATAFIRM S11 (firmware danneggiato) al costo di perdere i  dati contenuti"
lang: 'it'
permalink: '/it/post/patriot-burst-satafirm-s11-recovery.html'
images:
  - id: 'post_firmware_flash'
    filename: 'post_firmware_flash.png'
    alt: 'Risultato dopo aver flashato il firmware su un Patriot Burst'
    caption: ! 'Re-flash del firmware di un Partiot Burst SSD'
    width: '300px'
  - id: 'bios_disk_mode'
    filename: 'bios_disk_mode.jpg'
    alt: 'Schermata che mostra le opzioni dei dischi dal BIOS'
    caption: ! 'Ricordatevi di impostare la modalità giusta!'
    width: '300px'
  - id: 'translated_readme'
    filename: 'translated_readme.png'
    alt: 'Sezione tradotta dal russo di uno dei file leggimi'
    caption: ! 'Parte della traduzione del file leggimi'
    width: '300px'
---
{% include image.html id="post_firmware_flash" %}

## Sommario

<!--TOC-->

- [Sommario](#sommario)
- [Introduzione](#introduzione)
- [Cosa è successo](#cosa-è-successo)
- [Istruzioni per recuperare l'SSD ad uno stato utilizzabile](#istruzioni-per-recuperare-lssd-ad-uno-stato-utilizzabile)
  - [Strumenti](#strumenti)
  - [Modalità dei dischi](#modalità-dei-dischi)
  - [Ottenere il file firmware corretto](#ottenere-il-file-firmware-corretto)
  - [Fare il build del flasher](#fare-il-build-del-flasher)
  - [Esecuzione](#esecuzione)
- [Conclusione](#conclusione)

<!--TOC-->

## Introduzione

<!--excerpt_start-->Sono riuscito a recuperare un SSD
[Patriot Burst](https://www.patriotmemory.com/products/burst-sata-iii-retired-2-5-ssd-box-package)
da 240 GB il cui firmware si è danneggiato.<!--excerpt_end--> **Se provate il
seguente metodo perderete tutti i dati ma almeno potrete riutilizzare il disco.**
Sfortunatamente anche tutti i dati S.M.A.R.T. si resettano, come se il disco
fosse appena stato aperto dalla scatola:

```
SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAG     VALUE WORST THRESH TYPE      UPDATED  WHEN_FAILED RAW_VALUE
1 Raw_Read_Error_Rate       0x000b   100   100   050    Pre-fail  Always       -       0
9 Power_On_Hours            0x0012   100   100   000    Old_age   Always       -       0
12 Power_Cycle_Count        0x0012   100   100   000    Old_age   Always       -       1
168 SATA_Phy_Error_Count    0x0012   100   100   000    Old_age   Always       -       0
170 Bad_Blk_Ct_Erl/Lat      0x0003   100   100   010    Pre-fail  Always       -       0/301
173 MaxAvgErase_Ct          0x0012   100   100   000    Old_age   Always       -       1
192 Unsafe_Shutdown_Count   0x0012   100   100   000    Old_age   Always       -       0
194 Temperature_Celsius     0x0023   067   067   000    Pre-fail  Always       -       33 (Min/Max 33/33)
218 CRC_Error_Count         0x000b   100   100   050    Pre-fail  Always       -       0
231 SSD_Life_Left           0x0013   100   100   000    Pre-fail  Always       -       100
241 Lifetime_Writes_GiB     0x0012   100   100   000    Old_age   Always       -       0
```

Leggendo diversi post online la causa della corruzione del firmware non mi è
chiara: è un problema di usura o la qualità del controller (PS3111-S11 o
PS3111-S11T) è bassa? C'è stato un problema elettrico. Non ne ho idea.

{% include link_preview.html url="https://forum.hddguru.com/viewtopic.php?f=10&t=39161&mobile=desktop" %}

Ho trovato
[questo foglio di calcolo](https://docs.google.com/spreadsheets/d/1B27_j9NDPU3cNlj2HKcrfpJKHkOf-Oi1DbuuQva2gT4/edit#gid=0)
che contiene una lista di diversi SSD. In questo modo si possono ordinare per
controller e quindi vedere quali sono i modelli che potrebbero presentare il
problema.

## Cosa è successo

Comunque quello che è successo è che un disco è sparito improvvisamente dalle
periferiche del sistema mentre il computer era acceso e funzionante
normalmente. Questo ed altri errori sono comparsi in continuazione eseguendo
`dmesg`:

```
BTRFS warning (device sdb2): lost page write due to IO error on /dev/sde2
```

Il disco era parte di un array Btrfs RAID 1 che contenente i repository Git
ospitati da Gitea. Avevo comunque i dati sull'altro disco gemello e i backup,
quindi non ero preoccupato.

Quello che ho fatto dopo è stato spegnere il server e provare a controllare
se il disco potesse funzionare: in quel momento non sapevo cosa era successo
veramente. Pensavo che il cavo di alimentazione o quello dei dati SATA si
fosse allentato. Il disco era ora visibile ma vedevo alcuni errori. Ho notato
che `smartctl` vedeva il disco come SATAFIRM S11 e non come Patriot Burst

```
Device Model:     SATAFIRM   S11
```

Quello che ho fatto poi è stato di *riparare* il filesystem usando alcuni
strumenti forniti da Btrfs. L'unico che ha funzionato è `btrfs scrub`. Tutti
gli errori erano ora risolti. infatti, quando ho provato a fare rsync prima
dello scrub risultavano molti errori, ma nessuno dopo.

Per evitare altri problemi ho poi creato un nuovo array RAUD 1 Btrfs usando
il secondo disco e un'altro di riserva. Così facendo ho lasciato il disco
*rotto* fuori dal server.

## Istruzioni per recuperare l'SSD ad uno stato utilizzabile

### Strumenti

Recuperare l'SSD non è stato così difficile una volta che si ottengono i
[tre strumenti](https://drive.google.com/drive/folders/1xuHNNRUeO-ls19I2bQ96SfpodolndMVL):

- il programma per identificare il tipo di flash
- il programma che flasha il firmware
- i file binari (i firmware)

Potete anche scaricare due di questi da [qui](http://vlo.name:3000/ssdtool/).
Andate alla sezione *Phison utility* e scaricate
[Phison flash id (for S5/S8/S9/S10/S11)](http://vlo.name:3000/tmph/phison_flash_id.rar)
e
[Phison S11 firmware flasher v2](http://vlo.name:3000/tmph/s11-flasher.rar).
I file firmware si scaricano da questo link:
[Phison PS3111 Firmware [SBFM]](https://www.usbdev.ru/?wpfb_dl=6583).
La spiegazione si trova su
[questa pagina](https://www.usbdev.ru/files/phison/ps3111fw/).

Tutti questi sono file RAR quindi potete usare il comando `unrar` per
estrarli da GNU/Linux.

### Modalità dei dischi

Sfortunatamente è necessario Windows (sigh) per fare il recupero. Windows 10
ha funzionato bene, ricordandosi però di collegare il disco via SATA e
impostare la modalità in AHCI. Inizialmente, infatti, non ha funzionato sul
mio portatile. L'ho collegato via USB: non riconosciuto. Ho usato lo slot SATA
interno: not recognized. Nel BIOS però era impostata la modalità RAID.

{% include image.html id="bios_disk_mode" %}

Se aprite uno dei file leggimi, che sono principalmente in russo, leggerete
questo (traduzione):

> L'unità deve essere collegata a un controller SATA (la connessione tramite firewire non è supportata); supporta i driver IDE e AHCI standard di Microsoft e Intel RST. Il funzionamento con altri driver non è stato testato. Probabilmente non funzionerà con i driver del controller della sezione "Controller SCSI e RAID" (eccetto le versioni RST a partire dalla 11.7).
>
> È anche possibile che funzioni con alcuni bridge USB-Sata, ad esempio asmedia asm105x/115x, jmicron jm[s]20329/539/578, initio inic1608 e alcuni altri.

Ciò significa che è meglio collegare l'SSD tramite SATA e impostare la modalità
AHCI nel BIOS.

{% include image.html id="translated_readme" %}

### Ottenere il file firmware corretto

Dovete identificare il firmware specifico di cui avete bisogno tramite il
programma `phison_flash_id.exe`. Il valore della linea `S11fw` identifica il
firmware da usare. Da me è risultato `SBFM61.3, 2019Jul26`. Prendete il
firmware corretto dalla directory `Firmware PS3111` (il file binario menzionato
prima). Nel mio caso c'era più di un file per lo stesso firmware quindi ho
preso il più recente. La data del file è nel formato `ddmmyyyy`, e si trova
subito doppo il carattere di trattino basso. Questo è lo schema usato:

```
${FIRMWARE_ID}_$(date +%d)$(date +%m)$(date +%Y).BIN
```

Nel mio caso ho scelto `SBFM61.3_11032021.BIN`.

Dovete poi rinominare `SBFM61.3_11032021.BIN` in `fw.BIN` e copuiarlo nella
directory del firmware flasher.

### Fare il build del flasher

Ora potete fare il build del programma che fa il flashing. Eseguite lo scipt
`s11-flasher2-toshiba.cmd` per l'SSD Patriot Burst. Si creerà un nuovo
eseguibile chiamato `fw.exe` nella directory.

### Esecuzione

Dopo l'avvio cliccate sul bottone `Upgrade Firmware` e aspettate qualche
minuto. Finito.

## Conclusione

Ci sono diversi video su YouTube che spiegano questi passaggi. In più, quando
estraete gli strumenti troverete i file leggimi.

{% include link_preview.html url="https://www.youtube.com/watch?v=OoDZlJjTc_I" %}
{% include link_preview.html url="https://www.youtube.com/watch?v=BQ-QRkO9VOI" %}

Lezione imparata: evitare questi tipi di SDD e verificare se il controller è
affidabile secondo le specifiche. Continuerò ad usare questo SSD in produzione?
Non credo: sarà una riserva.

🚀
